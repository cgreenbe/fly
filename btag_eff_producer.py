import os
import ROOT
from array import array
import argparse
from pathlib import Path
from ROOT import *

ROOT.TH2D.AddDirectory(False)


# Set up the argument parser
parser = argparse.ArgumentParser(description='Process some root files.')
parser.add_argument('--input_dir', '-I', type=str, help='Input directory containing .root files')

# Parse the arguments
args = parser.parse_args()

# Create a Path object for the input directory
input_path = Path(args.input_dir)

# Check if the input directory exists
if not input_path.is_dir():
    raise ValueError(f"The input directory {input_path} does not exist.")

pt_bins = [0., 40., 60., 80., 100., 150., 200., 300., 1000.]
eta_bins = [0., 0.6, 1.2, 2.4]



h_MedbtagEff_bcflav = ROOT.TH2D('h_MedbtagEff_bcflav', 'BTag Efficiency MedID BCFlav', 50, 0, 1000, 4, 0 ,2.4)
h_MedbtagEff_lflav = ROOT.TH2D('h_MedbtagEff_lflav', 'BTag Efficiency MedID LFlav', 50, 0, 1000, 4, 0, 2.4)

h_TightbtagEff_bcflav = ROOT.TH2D('h_TightbtagEff_bcflav', 'BTag Efficiency TightID BCFlav', 50, 0, 1000, 4, 0 ,2.4)
h_TightbtagEff_lflav = ROOT.TH2D('h_TightbtagEff_lflav', 'BTag Efficiency TightID LFlav', 50, 0, 1000, 4, 0, 2.4)




h_TightbtagEff_bcflav = ROOT.TH2D('h_TightbtagEff_bcflav', 'BTag Efficiency BCFlav', 50, 0, 1000, 4, 0 ,2.4)
h_TightbtagEff_lflav = ROOT.TH2D('h_TightbtagEff_lflav', 'BTag Efficiency LFlav', 50, 0, 1000, 4, 0, 2.4)



n=0
# Iterate over all .root files in the directory
for root_file in input_path.glob('*.root'):
    if "DATA" in root_file.name: continue
    if "Run" in root_file.name: continue
    print(f"Processing file: {root_file}")
    file = ROOT.TFile.Open(str(root_file))

    h_all_lflav = file.Get("Pt_Eta_all_lFlav_jets_cut000")
    h_all_bcflav = file.Get("Pt_Eta_all_bcFlav_jets_cut000")

    h_medID_lflav = file.Get("Pt_Eta_medID_bTagPass_lFlav_jets_cut000")
    h_medID_bcflav = file.Get("Pt_Eta_medID_bTagPass_bcFlav_jets_cut000")

    h_tightID_bcflav = file.Get("Pt_Eta_tightID_bTagPass_bcFlav_jets_cut000")
    h_tightID_lflav = file.Get("Pt_Eta_tightID_bTagPass_lFlav_jets_cut000")

    print(f'Integral = {h_all_lflav.Integral()}')

    if n==0:

        all_h_all_lflav = ROOT.TH2D("","", 50, 0, 1000, 4, 0, 2.4)
        all_h_all_bcflav = ROOT.TH2D("","", 50, 0, 1000, 4, 0, 2.4)

        all_h_medID_lflav = ROOT.TH2D("","", 50, 0, 1000, 4, 0, 2.4)
        all_h_medID_bcflav = ROOT.TH2D("","", 50, 0, 1000, 4, 0, 2.4)

        all_h_tightID_bcflav = ROOT.TH2D("","", 50, 0, 1000, 4, 0, 2.4) 
        all_h_tightID_lflav = ROOT.TH2D("","", 50, 0, 1000, 4, 0, 2.4)
        n=1

    

    all_h_all_lflav.Add(h_all_lflav)
    all_h_all_bcflav.Add(h_all_bcflav)

    all_h_medID_lflav.Add(h_medID_lflav)
    all_h_medID_bcflav.Add(h_medID_bcflav)

    all_h_tightID_bcflav.Add(h_tightID_bcflav)
    all_h_tightID_lflav .Add(h_tightID_lflav)

    file.Close()



for i in range(all_h_medID_bcflav.GetNbinsX()):
    for j in range(all_h_medID_bcflav.GetNbinsY()):
        numerator_bcflav = all_h_medID_bcflav.GetBinContent(i + 1, j + 1)
        denominator_bcflav = all_h_all_bcflav.GetBinContent(i + 1, j + 1)
        bcflav_efficiency = numerator_bcflav / denominator_bcflav if denominator_bcflav != 0 else 0
        
        numerator_lflav = all_h_medID_lflav.GetBinContent(i + 1, j + 1)
        denominator_lflav = all_h_all_lflav.GetBinContent(i + 1, j + 1)
        lflav_efficiency = numerator_lflav / denominator_lflav if denominator_lflav != 0 else 0

        h_MedbtagEff_bcflav.SetBinContent(i + 1, j + 1, bcflav_efficiency)
        h_MedbtagEff_lflav.SetBinContent(i + 1, j + 1, lflav_efficiency)
        

for i in range(all_h_tightID_bcflav.GetNbinsX()):
    for j in range(all_h_tightID_bcflav.GetNbinsY()):
        numerator_bcflav = all_h_tightID_bcflav.GetBinContent(i + 1, j + 1)
        denominator_bcflav = all_h_all_bcflav.GetBinContent(i + 1, j + 1)
        bcflav_efficiency = numerator_bcflav / denominator_bcflav if denominator_bcflav != 0 else 0
        
        numerator_lflav = all_h_tightID_lflav.GetBinContent(i + 1, j + 1)
        denominator_lflav = all_h_all_lflav.GetBinContent(i + 1, j + 1)
        lflav_efficiency = numerator_lflav / denominator_lflav if denominator_lflav != 0 else 0

        h_TightbtagEff_bcflav.SetBinContent(i + 1, j + 1, bcflav_efficiency)
        h_TightbtagEff_lflav.SetBinContent(i + 1, j + 1, lflav_efficiency)
# Create the output file path
output_file_path = 'btag_eff_tightID_2017.root'

# Save the histograms to the output file
output_file = ROOT.TFile.Open(output_file_path, 'RECREATE')
h_MedbtagEff_bcflav.Write()
h_MedbtagEff_lflav.Write()
h_TightbtagEff_bcflav.Write()
h_TightbtagEff_lflav.Write()
output_file.Close()
