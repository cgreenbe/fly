import ROOT
from ROOT import *

# Open the input files and get the histograms
wjets_file = ROOT.TFile("PROC_WJetsToLNu.root")
wjets_mu = wjets_file.Get("Wboson_transversMass_cut100")
wjets_el = wjets_file.Get("Wboson_transversMass_cut000")

data_file_mu = ROOT.TFile("DATA_SingleMuon.root")
data_mu = data_file_mu.Get("Wboson_transversMass_cut100")

data_file_el = ROOT.TFile("DATA_SingleElectron.root")
data_el = data_file_el.Get("Wboson_transversMass_cut000")

# Check that both histograms have the same number of bins
if wjets_mu.GetNbinsX() != data_mu.GetNbinsX() or wjets_el.GetNbinsX() != data_el.GetNbinsX():
    print("Error: histograms have different binning!")
    exit(1)

# Create the output histogram with the same binning as the input histograms
QCDdataDriven_Muon = ROOT.TH1D("Wboson_transversMass_cut101", "QCD data-driven estimation for muon", wjets_mu.GetNbinsX(), wjets_mu.GetXaxis().GetXmin(), wjets_mu.GetXaxis().GetXmax())
QCDdataDriven_Electron = ROOT.TH1D("Wboson_transversMass_cut101", "QCD data-driven estimation for electron", wjets_el.GetNbinsX(), wjets_el.GetXaxis().GetXmin(), wjets_el.GetXaxis().GetXmax())

# Loop over all bins and subtract qcd from wjets bin per bin
for i in range(1, wjets_el.GetNbinsX() + 1):

    bin_content_mu = data_mu.GetBinContent(i) - wjets_mu.GetBinContent(i)
    QCDdataDriven_Muon.SetBinContent(i, bin_content_mu)

    bin_content_el = data_el.GetBinContent(i) - wjets_el.GetBinContent(i)
    QCDdataDriven_Electron.SetBinContent(i, bin_content_el)


# Write the output histogram to a ROOT file
output_mu = ROOT.TFile("PROC_QCD_dataDriven_Muon.root", "RECREATE")
QCDdataDriven_Muon.Write()
output_mu.Close()

output_el = ROOT.TFile("PROC_QCD_dataDriven_Elec.root", "RECREATE")
QCDdataDriven_Electron.Write()
output_el.Close()