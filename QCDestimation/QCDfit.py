import os
import sys
import ROOT
from ROOT import *
import argparse
import ctypes


sys.path.append("..") # Add parent directory to system path
import hists_names as histName # Import module from parent directory
import printColors as color


parser = argparse.ArgumentParser()
parser.add_argument("--inFiles_nonQCD", help="the directory containing the ROOT files")
parser.add_argument("--inFiles_QCD", help="the directory containing the ROOT files")
args = parser.parse_args()

lumi = 59.74

# Lists of root files proceesses to use
nonQCD_MCfiles = [

    ['PROC_ST_t-channel_top', 136.02],
    ['PROC_ST_t-channel_antitop', 80.95],
    ['PROC_ST_s-channel', 3.68],
    ['PROC_ST_tW_top', 35.85],
    ['PROC_ST_tW_antitop', 35.85],

    ['PROC_TTToSemiLeptonic', 364.351],
    ['PROC_TTTo2L2Nu', 87.31],

    ['PROC_TTWJetsToLNu', 0.2045],
    ['PROC_TTWJetsToQQ', 0.4062],
    ['PROC_TTZToLLNuNu', 0.2529],
    ['PROC_TTZToQQ', 0.5297],

    ['PROC_WJetsToLNu', 61334.9],

    ['PROC_WW', 118.7],
    ['PROC_WZ', 47.13],
    ['PROC_ZZ', 16.523],

    ['PROC_DYJetsToLL_M-10to50', 18610],
    ['PROC_DYJetsToLL_M-50', 6025],

]

data_files = [

    ['DATA_SingleMuon', 1],
    # ['DATA_SingleElectron', 1],
]

dataDrivenQCD_files = [

    ['PROC_QCD_dataDriven_Muon', 1],
    ['PROC_QCD_dataDriven_Elec', 1],

]

# Function to fetch histograms from TFile files
def get_TH1D(file, hist_name, xsec, isMC):
    hist = file.Get(hist_name)

    if hist is None:
        print(f"Error: histogram {hist_name} not found in file {file}")
        return None

    if 'dataDriven' not in os.path.basename(file.GetName()) and isMC==True:
        sum_weights = file.Get(histName.genweight_sumOf+'_nocut')
        if sum_weights:
            scale_factor = xsec * 10**3 * lumi / sum_weights.GetBinContent(1)
            hist.Scale(scale_factor)

    return hist


# Histogram to fetch
Wboson_transversMass_histoName = histName.Wboson_transversMass + '_cut101'
print(f'{color.HRED}hist to make: {Wboson_transversMass_histoName}{color.RESET}')

# Define histograms for fit
all_mc = ROOT.TH1D('all_mc','',40,0,200)
data = ROOT.TH1D('data','',40,0,200)
QCD_dataDriven = ROOT.TH1D('QCD_dataDriven','',40,0,200)

# Fetching all histograms from the root files with the function defined above 'get_TH1D()'
for process, xsec in nonQCD_MCfiles:
    # print(f'process = {process} with xsec = {xsec}')
    mcfiles_path = args.inFiles_nonQCD + process + '.root'
    mcfile = ROOT.TFile(mcfiles_path, "READ")
    histo = get_TH1D(mcfile, Wboson_transversMass_histoName, xsec, isMC=True)
    all_mc.Add(histo)
print(f'entries in all_mc: {all_mc.GetEntries()}')


dataFiles_path = args.inFiles_nonQCD + data_files[0][0] + '.root'
data_file = ROOT.TFile(dataFiles_path, "READ")
histo = get_TH1D(data_file, Wboson_transversMass_histoName, xsec, isMC=False)
data.Add(histo)
print(f'entries in data: {data.GetEntries()}')



QCD_dataDriven_path = args.inFiles_QCD + dataDrivenQCD_files[0][0] + '.root'
QCD_dataDriven_file = ROOT.TFile(QCD_dataDriven_path, "READ")
histo = get_TH1D(QCD_dataDriven_file, Wboson_transversMass_histoName, xsec, isMC=True)
QCD_dataDriven.Add(histo)
print(f'entries in QCD_dataDriven: {QCD_dataDriven.GetEntries()}')


# Getting the values of the integrals
all_mc_integral = all_mc.Integral(8,40)
print(f'all_mc_integral = {format(all_mc_integral, ".2e")}')

data_integral = data.Integral(8,40)
print(f'data_integral = {format(data_integral, ".2e")}')

QCD_dataDriven_integral = QCD_dataDriven.Integral(8,40)
print(f'QCD_dataDriven_integral = {format(QCD_dataDriven_integral, ".2e")}')

constrain = all_mc_integral/data_integral

mc = ROOT.TObjArray(4)
mc.Add(all_mc)
mc.Add(QCD_dataDriven)

# Create a TFractionFitter object with the data and model histograms
fit = ROOT.TFractionFitter(data, mc)

# Set constraints on the fit parameters
fit.Constrain(0, constrain - 0.01, constrain + 0.01)
fit.Constrain(1, 0.0, 0.5)

fit.SetRangeX(8,40)
status = fit.Fit()
print(f'{color.BYELLOW}status: {status}{color.RESET}')

# Define C-style double precision variables
param0 = ctypes.c_double(0.0)
param1 = ctypes.c_double(0.0)
param0_error = ctypes.c_double(0.0)
param1_error = ctypes.c_double(0.0)

# Call GetResult() with the address of the double precision variables
fit.GetResult(0, param0, param0_error)
fit.GetResult(1, param1, param1_error)

# Extract the double precision values from the ctypes variables
param0 = param0.value
param1 = param1.value
param0_error = param0_error.value
param1_error = param1_error.value

newscale0 = data_integral * param0 / all_mc_integral
newscale1 = data_integral * param1 / QCD_dataDriven_integral

print("newscale0=", newscale0)
print("newscale1=", newscale1)


# c = ROOT.TCanvas()
# all_mc.Draw()
# data.Draw()
# c.Draw()
# ROOT.gApplication.Run()
