#!/bin/bash
#
#SBATCH --spread-job
# mail-type=BEGIN, END, FAIL, REQUEUE, ALL, STAGE_OUT, TIME_LIMIT_90
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=c.greenberg@ip2i.in2p3.fr
#SBATCH --time=4:00:00

export X509_USER_PROXY=$HOME/x509up_u2395
export EOS_MGM_URL=root://lyoeos.in2p3.fr

inputFile=$(cat ${1})

root='.root'

COUNTER=1

killer=0

for line in $inputFile
do
    outputFile=$(echo $line | tr "/" "\n")
    for place in $outputFile
    do

        if [[ "$place" == *"$root"* && $killer == *0* ]]; then
            srun -n1 --exclusive ./processonefile.py "$line" ${2}$place ${4} ${5} > ${2}$(echo $place | tr ".root" "\n").out &
            # srun -n1 ./processonefile.py "$line" ${2}$place ${4} > ${2}$(echo $place | tr ".root" "\n").out &
            # ./processonefile.py "$line" ${2}$place ${4} > ${2}$(echo $place | tr ".root" "\n").out &
            echo $COUNTER $SLURM_JOB_ID.$SLURM_PROCID
            let COUNTER++
            if [ $COUNTER -gt ${3} ]; then
                let killer++
            fi
        fi

    done
done
wait