
### 2023-10-30 Muon Channel / 2j0t1m REG Region / All Datasets
Analysis pwd: "results/17/2023-10-30/muChannel_2j0t1m_REGregion_nbTasksall/Run2017D_SingleMuon/"
**Year:** 17
**HLTs used:**
"HLT_IsoMu27",
**Region:** 2j0t1m
**Channel:** mu
**is QCD?:** No
**Number of tasks:** all
**Corrections:**  All and Medium BJets
**Comments:** 
**Merged?:** 


### 2023-10-30 Muon Channel / 2j0t1m QCD Region / Only Data
Analysis pwd: "results/17/2023-10-30/muChannel_2j0t1m_QCDregion_nbTasksall/Run2017D_SingleMuon/"
**Year:** 17
**HLTs used:**
"HLT_Mu27",
"HLT_Mu20",
**Region:** 2j0t1m
**Channel:** mu
**is QCD?:** Yes
**Number of tasks:** all
**Corrections:** Medium BJets
**Comments:** 
**Merged?:** 


### 2023-10-31 Muon Channel / 3j2t Rgion / All files
Analysis pwd: "results/17/2023-10-31/muChannel_3j2t_nbTasksall/Run2017D_SingleMuon/"
**Year:** 17
**HLTs used:**
"HLT_IsoMu27",
**Region:** 3j2t
**Channel:** mu
**is QCD?:**
**Number of tasks:** all
**Corrections:** Tight BJets
**Comments:** 
**Merged?:** 



### 2023-11-15 Muon / Btag REG / Produced BTag eff file: btag_eff_2017.root
Analysis pwd: "results/17/2023-11-15/muChannel_btagREG_tightANDmedBtagEff_nbTasksall/PROC_QCD_Pt-1000_MuEnriched/"
**Year:** 17
**HLTs used:**
"HLT_Mu20", // Use Only this one for the next QCD region analysis cause we don't know the relative prescaling between this two non iso HLT paths
**Region:** 
**Channel:** mu
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 


### 2023-11-15 Muon / 2j0t1m REG Region / All Files _trying new btag efficiency file computed before_
Analysis pwd: "results/17/2023-11-15/muChannel_2j0t1m_nbTasksall/Run2017D_SingleMuon/"
**Year:** 17
**HLTs used:**
"HLT_IsoMu27",
**Region:** 
**Channel:** mu
**is QCD?:** 
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 


### 2023-11-15 Muon / 2j0t1m QCD Region / All Files _trying new btag efficiency file computed before_ 

**Copied the Following folder into: /gridgroup/cms/greenberg/v2_analyzer/CMSSW_12_3_0/src/v3_FLY/v3_fly/results/17/2023-11-15/muChannel_2j0t1m_nbTasksall/QCDregion**

Analysis pwd: "results/17/2023-11-15/muChannel_2j0t1mQCD_nbTasksall/Run2017D_SingleMuon/"
**Year:** 17
**HLTs used:**
"HLT_Mu20", // Use Only this one for the next QCD region analysis cause we don't know the relative prescaling between this two non iso HLT paths
**Region:** 
**Channel:** mu
**is QCD?:** YES
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 

