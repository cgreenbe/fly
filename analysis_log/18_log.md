
### 2023-10-30 Muon Channel / 2j0t1m REG Region / All Datasets
Analysis pwd: "results/18/2023-10-30/muChannel_2j0t1m_REGregion_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:**
"HLT_IsoMu24",
**Region:** 
**Channel:** mu
**is QCD?:** NO
**Number of tasks:** all
**Corrections:** Medium BJets
**Comments:** 
**Merged?:** 

### 2023-10-30 Muon Channel / 2j0t1m QCD Region / Only Data
Analysis pwd: "results/18/2023-10-30/muChannel_2j0t1m_QCDregion_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:**
"HLT_Mu27",
"HLT_Mu20",
**Region:** 2j0t1m
**Channel:** mu
**is QCD?:** Yes
**Number of tasks:** all
**Corrections:** Medium BJets
**Comments:** 
**Merged?:** 

### 2023-10-31 Muon Channel / 3j2t Region / All Files
Analysis pwd: "results/18/2023-10-31/muChannel_3j2t_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:**
"HLT_IsoMu24",
**Region:** 3j2t
**Channel:** mu
**is QCD?:** no
**Number of tasks:** all
**Corrections:** Tight BJets
**Comments:** 
**Merged?:** 



### 2023-11-03 Muon Channel / 2j0t1m QCD Region / Only MC QCD
Analysis pwd: "results/18/2023-11-03/muChannel_2j0t1m_QCDregion_onlyQCDmc_nbTasksall/PROC_QCD_Pt-1000_MuEnriched/"
**Year:** 18
**HLTs used:**
"HLT_Mu27",
"HLT_Mu20",
**Region:** 2j0t1m
**Channel:** mu
**is QCD?:** Yes
**Number of tasks:** all
**Corrections:** Medium b tagged / Old fashion = 1step btag SF process
**Comments:** 
**Merged?:** 


### 2023-11-09 Muon channel / BTag REG / All Files _TTBarToSemiLeptonic FAILED Running it on the next analysis_
Analysis pwd: "results/18/2023-11-09/muChannel_muchannel_BTagREG_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:**
"HLT_Mu20",
**Region:** BTag Region
**Channel:** mu
**is QCD?:** No
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 

### 2023-11-10 Muon channel / BTag REG / Only TTBarToSemiLep _TTBBar to use on previous run_
Analysis pwd: "results/18/2023-11-10/muChannel_muChannle_btagREG_onlyTTBar_nbTasksall/PROC_TTToSemiLeptonic/"
**Year:** 18
**HLTs used:**
"HLT_Mu20",
**Region:** BTag Region
**Channel:** mu
**is QCD?:** No
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 


### 2023-11-10 Muon Channel / 2j0t1m QCD Region / Only MC _with new btagSF_ It is only MC because Btag SF are not applyed to DATA. Take Data from: results/18/2023-10-30/muChannel_2j0t1m_REGregion_nbTasksall/ And QCD DataDriven from the same place
Analysis pwd: "results/18/2023-11-10/muChannel_muchannel_2j0t1mREG_onlyMC_nbTasksall/PROC_QCD_Pt-1000_MuEnriched/"
**Year:** 18
**HLTs used:**
"HLT_IsoMu24",
**Region:** 
**Channel:** mu
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 

### 2023-11-13 Muon Channel / BTag REgion / Only MC / For Tight BTag Eff Computations
Analysis pwd: "results/18/2023-11-13/muChannel_btag_tightEff_nbTasksall/PROC_QCD_Pt-1000_MuEnriched/"
**Year:** 18
**HLTs used:**
"HLT_Mu20",
**Region:** 
**Channel:** mu
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 


### 2023-11-13 Muon channel / Signal Reg/ Only MC / Implemented new tight ID EFF corrections
Analysis pwd: "results/18/2023-11-13/muChannel_2j1t_nbTasksall/PROC_QCD_Pt-1000_MuEnriched/"
**Year:** 18
**HLTs used:**
"HLT_IsoMu24",
**Region:** 
**Channel:** mu
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 


### 2023-11-14 Muon / 2j0t1m / Rel Iso Study with ISO HLT
Analysis pwd: "results/18/2023-11-14/muChannel_2j0t1m_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:**
"HLT_IsoMu24",
**Region:** 
**Channel:** mu
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** Issue with selecting muonChannel==1 cause I miss events at certain iso ranges
**Merged?:** 


### 2023-11-14 Muon / 2j0t1m / Rel Iso Study with nonISO HLT
Analysis pwd: "results/18/2023-11-14/muChannel_2j0t1m_nonIsoMuonSelection_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:**
"HLT_IsoMu24",
**Region:** 
**Channel:** mu
**is QCD?:**
**Number of tasks:** all
**Corrections:** Same issue as before but a lot more MC cause of the usage of non Iso HLT
**Comments:** 
**Merged?:** 


### 2023-11-14 Muon / 3j2t / Usage of BTagg corrections for the first time
Analysis pwd: "results/18/2023-11-14/muChannel_test3j2t_nbTasks2/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:**
"HLT_IsoMu24",
**Region:** 
**Channel:** mu
**is QCD?:**
**Number of tasks:** 2
**Corrections:** 
**Comments:** 
**Merged?:** 


### 2023-11-15 Muon / SIGNAL REG / Usage of BTagg Eff Corrections for the first time
Analysis pwd: "results/18/2023-11-15/muChannel_2j1t_nbTasksall/PROC_QCD_Pt-1000_MuEnriched/"
**Year:** 18
**HLTs used:**
"HLT_IsoMu24",
**Region:** 
**Channel:** mu
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 

