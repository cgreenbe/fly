import ROOT
from ROOT import TFile, TCanvas, gSystem, TLegend
import argparse
from pathlib import Path

def subtract_histograms(hist_data, hist_wjets):
    # Check if the histograms have the same number of bins
    if hist_data.GetNbinsX() != hist_wjets.GetNbinsX():
        raise ValueError("Histograms have different number of bins!")

    # Create a new histogram for the subtraction
    hist_data_minus_wjets = hist_data.Clone("data_minus_wjets")
    hist_data_minus_wjets.Add(hist_wjets, -1)  # Subtract the contents of Wjets from data

    # Normalize the subtracted histogram to a total integral of 1
    hist_data_minus_wjets.Scale(1.0 / hist_data_minus_wjets.Integral())

    return hist_data_minus_wjets

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Reads .root files from source folders and prints histograms in target folder")

    # Add the arguments
    parser.add_argument("--qcd_source_folder", help="Path to the source folder")
    parser.add_argument("--wjets_source_folder", help="Path to the source folder")
    parser.add_argument("--target_folder", help="Path to the target folder")

    # Parse the arguments
    args = parser.parse_args()
    qcd_source_folder = args.qcd_source_folder
    wjets_source_folder = args.wjets_source_folder
    target_folder = args.target_folder

    sourcePathQCD = Path(qcd_source_folder)
    sourcePathWJets = Path(wjets_source_folder)

    qcd_files_QCDregion = list(sourcePathQCD.glob("*QCD*Pt*.root"))
    qcd_files_WJetsregion = list(sourcePathWJets.glob("*QCD*Pt*.root"))


    qcd_in_QCDregion_histo = ROOT.TH1D()  # Create an empty TH1D object

    for qcd_file in qcd_files_QCDregion:
        print(f'qcd_file: {qcd_file}')
        file_qcd = TFile(str(qcd_file), 'READ')
        hist_qcd = file_qcd.Get('Wboson_transversMass_cut000')
        if hist_qcd.Integral() != 0:
            # hist_qcd.Scale(1.0 / hist_qcd.Integral())
            qcd_in_QCDregion_histo.Add(hist_qcd)  # Add the histogram to the TH1D object
        else:
            print(f"Warning: Empty histogram found in file {qcd_file}")

    qcd_in_QCDregion_histo.Scale(1/qcd_in_QCDregion_histo.Integral())
    
    qcd_in_WJetsregion_histo = ROOT.TH1D()  # Create an empty TH1D object

    for qcd_file in qcd_files_WJetsregion:
        print(f'qcd_file: {qcd_file}')
        file_qcd = TFile(str(qcd_file), 'READ')
        hist_qcd = file_qcd.Get('Wboson_transversMass_cut000')
        if hist_qcd.Integral() != 0:
            # hist_qcd.Scale(1.0 / hist_qcd.Integral())
            qcd_in_WJetsregion_histo.Add(hist_qcd)  # Add the histogram to the TH1D object
        else:
            print(f"Warning: Empty histogram found in file {qcd_file}")

    qcd_in_WJetsregion_histo.Scale(1/qcd_in_WJetsregion_histo.Integral())


    file_data = TFile(qcd_source_folder + 'DATA_SingleMuon.root', 'READ')
    hist_data = file_data.Get('Wboson_transversMass_cut000')
    if not hist_data:
        raise ValueError("Data histogram is empty or not found!")

    file_wjets = TFile(qcd_source_folder + 'PROC_WJetsToLNu.root', 'READ')
    hist_wjets = file_wjets.Get('Wboson_transversMass_cut000')
    if not hist_wjets:
        raise ValueError("Wjets histogram is empty or not found!")

    # Call the subtract_histograms function
    hist_data_minus_wjets = subtract_histograms(hist_data, hist_wjets)

    c1 = TCanvas('c1', 'c1',1200,1200)

    hist_data_minus_wjets.SetLineColor(ROOT.kRed)  # Set line color for the subtracted histogram
    qcd_in_QCDregion_histo.SetLineColor(ROOT.kBlue)
    qcd_in_WJetsregion_histo.SetLineColor(ROOT.kGreen)

    hist_data_minus_wjets.Draw()
    qcd_in_QCDregion_histo.Draw("SAME")
    qcd_in_WJetsregion_histo.Draw("SAME")


    # Create a legend
    legend = TLegend(0.7, 0.7, 0.9, 0.9)
    legend.AddEntry(hist_data_minus_wjets, "Data - Wjets in QCD region", "l")
    legend.AddEntry(qcd_in_QCDregion_histo, "QCD in QCD region", "l")  # Add the legend entry for the combined histograms
    legend.AddEntry(qcd_in_WJetsregion_histo, "QCD in WJets region", "l")  # Add the legend entry for the combined histograms

    legend.Draw()

    # Save the canvas as a PNG file
    c1.SaveAs(target_folder + 'hist3.png')
