from pathlib import Path
import argparse
import ROOT
import pandas as pd
import ctypes
import sys
# sys.path.append("../") # Add parent directory to system path
import printColors as color


ROOT.TH1D.AddDirectory(False)

def get_Xsec_values():
    pd_csv = pd.read_csv("/gridgroup/cms/greenberg/v2_analyzer/CMSSW_12_3_0/src/v3_FLY/v3_fly/dataSet_info.csv")
    return pd_csv.values.tolist()


def makeFit(nonQCD_mc_regRegion, data_filepwd, QCD_mc_regRegion, data_qcd, histogram, lumi):

    # Set the text color to bright red at the beginning
    print("\033[91;1mStarting QCD Fit")
    print(f'lumi value: {lumi} | histogram: {histogram}')
    
    sumOfWeights_hist_name = 'genweight_sumOf_nocut'

    print(f"Data file: {data_filepwd}")
    data_file = ROOT.TFile.Open(str(data_filepwd),"READ")
    data_hist = data_file.Get(histogram)

    print(f"QCD Data Driven file: {data_qcd}")
    data_qcd_file = ROOT.TFile.Open(str(data_qcd),"READ")
    data_qcd_hist = data_qcd_file.Get(histogram)

    nbins = data_hist.GetNbinsX()
    xmin = data_hist.GetXaxis().GetXmin()
    xmax = data_hist.GetXaxis().GetXmax()

    print(f'nbins = {nbins}; xmin = {xmin}; xmax = {xmax}')

    all_mc_noneWjets = ROOT.TH1D('all_mc','all_mc',nbins,xmin,xmax)
    wjets = ROOT.TH1D('wjets','wjets',nbins,xmin,xmax)
    data = ROOT.TH1D('data','data',nbins,xmin,xmax)
    QCD_dataDriven = ROOT.TH1D('QCD_dataDriven','QCD_dataDriven',nbins,xmin,xmax)

    data.Add(data_hist)
    QCD_dataDriven.Add(data_qcd_hist)

    proc_Xsec = get_Xsec_values()

    print('non QCD and non Data files to be processed in regular region (non QCDregion):')
    for rootFile in nonQCD_mc_regRegion:
        # print(f'Processing file: {rootFile}')
        if rootFile.name == 'PROC_WJetsToLNu.root': continue
        file = ROOT.TFile.Open(str(rootFile),"READ")
        hist = file.Get(histogram)
        sumOfWeights_hist = file.Get(sumOfWeights_hist_name)
        sumOfWeights = sumOfWeights_hist.GetMaximum()
        for process, Xsec in proc_Xsec:
            if str(process) in str(rootFile.name): 
                totalWeight = Xsec*lumi*10**3/sumOfWeights
                # print(f"Xsec for sample {rootFile.name} = {Xsec}")
                break
        if totalWeight == 0:
            print(f"Error computing weights for file {rootFile.name}")
            exit(1)
        hist.Scale(totalWeight)
        all_mc_noneWjets.Add(hist)
        if 'WJetsToLNu' in rootFile.name and 'TT' not in rootFile.name:
            print(f'removing {rootFile.name} from all_mc template') 
            wjets.Add(hist)
    all_mc_noneWjets.Add(wjets,-1)


    print('QCD and non Data files to be processed in regular region (non QCDregion):')
    QCDmc_integral = 0.0
    for rootFile in QCD_mc_regRegion:
        # print(f'Processing file: {rootFile}')
        file = ROOT.TFile.Open(str(rootFile),"READ")
        hist = file.Get(histogram)
        # print(f'hist integral before weights: {hist.Integral()}')
        sumOfWeights_hist = file.Get(sumOfWeights_hist_name)
        sumOfWeights = sumOfWeights_hist.GetMaximum()
        # print(f'sumOfWeights={sumOfWeights}')
        for process, Xsec in proc_Xsec:
            if str(process) in str(rootFile.name): 
                totalWeight = Xsec*lumi*10**3/sumOfWeights
                # print(f"totalWeight for sample {rootFile.name} = {totalWeight}")
                break
        if totalWeight == 0:
            print("Error computing weights")
            exit(1)
        # print(f'hist integral before scale: {hist.Integral()}')
        hist.Scale(totalWeight)
        # print(f'hist integral after scale: {hist.Integral()}\n')
        QCDmc_integral += hist.Integral()
    
    print(f'QCDmc_integral = {QCDmc_integral:e}')    

    mc = ROOT.TObjArray()

    # onlyWJets = True
    # if onlyWJets:
    #     print('Fitting only WJets template')
    #     all_mc.Add(wjets,-1)
    #     data.Add(all_mc,-1)
    #     mc.Add(wjets)
    # else:
    #     mc.Add(all_mc)

    

    
    all_mc_nonWjets_integral = all_mc_noneWjets.Integral()
    print(f'all_mc_integral = {format(all_mc_nonWjets_integral, ".2e")}')

    data_integral = data.Integral()
    print(f'data_integral = {format(data_integral, ".2e")}')

    QCD_dataDriven_integral = QCD_dataDriven.Integral()
    print(f'QCD_dataDriven_integral BEFORE SCALING IT TO QCD MC = {format(QCD_dataDriven_integral, ".2e")}')

    wjets_integral = wjets.Integral()
    print(f'wjets_integral = {format(wjets_integral, ".2e")}')

    QCDdataDriven_scaling_to_QCDmc = QCDmc_integral/QCD_dataDriven_integral

    QCD_dataDriven.Scale(QCDdataDriven_scaling_to_QCDmc)
    print(f'dataDriven_SF = {QCDdataDriven_scaling_to_QCDmc}')
    print(f'QCD_dataDriven_integral AFTER SCALING TO MC QCD = {format(QCD_dataDriven.Integral(), ".2e")}')


    mc.Add(all_mc_noneWjets)
    mc.Add(wjets)
    mc.Add(QCD_dataDriven)


    ratio_all_mc = all_mc_nonWjets_integral/data_integral
    ratio_wjets = wjets_integral/data_integral
    ratio_QCDdataDriven = QCDmc_integral/data_integral
    print(f'ratio_all_mc = {ratio_all_mc}')
    print(f'ratio_wjets = {ratio_wjets}')
    print(f'ratio_QCDdataDriven = {ratio_QCDdataDriven}')

    # Create a TFractionFitter object with the data and model histograms
    fit = ROOT.TFractionFitter(data, mc)

    # Set constraints
    fit.Constrain(0, ratio_all_mc - ratio_all_mc*0.5, ratio_all_mc + ratio_all_mc*0.5)
    fit.Constrain(1, ratio_wjets - ratio_wjets*0.5, ratio_wjets + ratio_wjets*0.5)
    fit.Constrain(2, 0, ratio_QCDdataDriven*2)

    fit.SetRangeX(int(xmin),int(xmax))
    status = fit.Fit()
    print(f'{color.BYELLOW}status: {status}{color.RESET}')

    # Define C-style double precision variables
    param0 = ctypes.c_double(0.0)
    param1 = ctypes.c_double(0.0)
    param2 = ctypes.c_double(0.0)
    param0_error = ctypes.c_double(0.0)
    param1_error = ctypes.c_double(0.0)
    param2_error = ctypes.c_double(0.0)

    # Call GetResult() with the address of the double precision variables
    fit.GetResult(0, param0, param0_error)
    fit.GetResult(1, param1, param1_error)
    fit.GetResult(2, param2, param2_error)

    # Extract the double precision values from the ctypes variables
    param0 = param0.value
    param1 = param1.value
    param2 = param2.value
    param0_error = param0_error.value
    param1_error = param1_error.value
    param2_error = param2_error.value

    # print(f'param0={param0}')
    # print(f'param1={param1}')

    all_mc_SF = data_integral * param0 / all_mc_nonWjets_integral
    wjets_SF = data_integral * param1 / wjets_integral
    QCD_dataDriven_SF = data_integral * param2 / QCDmc_integral


    print("all_mc_sf =", f'{all_mc_SF:.2f}')
    print("wjets_sf =", f'{wjets_SF:.2f}')
    print("QCD_dataDriven_sf =", f'{QCD_dataDriven_SF:.2f}')

    # Set the text color back to normal at the end
    print("\033[0m")

    return all_mc_SF, wjets_SF, QCD_dataDriven_SF, QCDdataDriven_scaling_to_QCDmc


def hist_add(rootFiles, histogram, normalized, channel, region, lumi):

    print("\033[91mInside hist_add function\033[0m")


    ROOT.TH1D.AddDirectory(False)

    normalized_flag = f'_{channel}Channel_{region}'
    if normalized:
        normalized_flag = f'_normalized_{channel}Channel_{region}'

    # Create a canvas
    canvas_title = f'Sum of {histogram}'
    canvas_hist_add = ROOT.TCanvas("canvas", canvas_title, 800, 600)

    # Variable to keep track of the maximum y-value
    max_y = 0

    # print(f'file to plot: {str(rootFiles[0])}')
    flag1 = 0
    for rootFile in rootFiles:
        file = ROOT.TFile.Open(str(rootFile), "READ")
        hist = file.Get(histogram)
        if hist.Integral() != 0:
            first_file = ROOT.TFile.Open(str(rootFiles[flag1]),"READ")
            break
        flag1 += 1        


    first_hist = first_file.Get(histogram)
    # print(f'first_file: {type(first_file)}')
    sum_hist = ROOT.TH1D(f"Sum {histogram}", "", first_hist.GetNbinsX(), first_hist.GetXaxis().GetXmin(), first_hist.GetXaxis().GetXmax())

    # print(f'binning for sum of hists = {sum_hist.GetNbinsX()}')

    proc_Xsec = get_Xsec_values()
    # print(Xsec[0])

    # Loop over the ROOT files
    for rootFile in rootFiles:
        # Open the file and get the histogram
        # print(f'Openning file: {rootFile.name}')
        file = ROOT.TFile.Open(str(rootFile), "READ")
        hist = file.Get(histogram)

        # Get the sum of weights of the current file
        sumOfWeights_hist = file.Get("genweight_sumOf_nocut")
        sumOfWeights = sumOfWeights_hist.GetMaximum()

        # Skip the file is it is empty
        if hist.Integral() == 0:
            print(f'No events in file {rootFile.name}')
            continue
        
        # Skip the file if the sumOfWeights is emty or the histo does not exists
        if not hist or not sumOfWeights_hist:
            raise ValueError(f"Histogram '{histogram}' not found in file {rootFile}")
        
        # print(f'sum of weights for {rootFile.name}: {sumOfWeights}')
        totalWeight = 0  # Initialize totalWeight with a default value

        # Compute the weight of the process
        for process, Xsec in proc_Xsec:
            # print(f'process: {process}')
            # print(f'Xsec: {Xsec}')
            if str(process) in str(rootFile.name.replace('.root','')): 
                totalWeight = Xsec*lumi*10**3/sumOfWeights
                # print(f"Xsec for sample {rootFile.name} = {Xsec}")
                break
        
        # Check if the total weight is 0
        if totalWeight == 0:
            print("Error computing weights")
            exit(1)

        # print(f'process: {rootFile.name} has a totalWweight = {totalWeight}')
        hist.Scale(totalWeight)


        if hist.GetMaximum() > max_y:
            max_y = hist.GetMaximum()

        # Check if the histograms have the same number of bins
        if hist.GetNbinsX() != first_hist.GetNbinsX():
            raise ValueError(f"Histograms in {first_file} and {rootFile} have different number of bins")

        # Add the histogram to the non-QCD histogram
        sum_hist.Add(hist)

    # Adjust the y-axis maximum

    # sum_hist.GetYaxis().SetRangeUser(0, max_y * 4)

    if normalized:
        sum_hist.Scale(1/sum_hist.Integral())
        # sum_hist.GetYaxis().SetRangeUser(0, 0.1)
    

    # sum_hist.Draw()
    # canvas_hist_add.SaveAs(f"{histogram}{normalized_flag}.png")
    # print(f'type of sum_hist: {type(sum_hist)}')
    return sum_hist


def getData_hist(rootFile, histogram, normalized, channel, region):

    print("\033[91mInside getData_hist function\033[0m")


    normalized_flag = f'_{channel}_{region}'
    if normalized:
        normalized_flag = f'_normalized_{channel}_{region}'
    canva_getData = ROOT.TCanvas("","", 800,600)
    file = ROOT.TFile.Open(str(rootFile), "READ")
    hist = file.Get(histogram)
    if normalized:
        hist.Scale(1/hist.Integral())

    # hist.Draw()
    # canva_getData.SaveAs(f"{histogram}_in_{rootFile.name.replace('.root','')}_{normalized_flag}.png")
    return hist


def make_plot(histograms_info, histname, outFile):
    print("\033[91mInside make_plot function\033[0m")

    canva = ROOT.TCanvas("", "", 1200, 900)
    legend = ROOT.TLegend(0.6, 0.7, 0.9, 0.9)  # Adjust legend position
    max_y = 0.0  # Variable to store the maximum y-height

    for i, info in enumerate(histograms_info):
        histogram, color, title = info
        histogram.SetStats(ROOT.kFALSE)
        histogram.SetLineColor(color)
        histogram.SetLineWidth(2)  # Increase line width for better visibility
        histogram.SetMarkerStyle(20)  # Add markers
        histogram.SetMarkerColor(color)
        histogram.SetMarkerSize(1.0)
        if i == 0:
            histogram.Draw()
        else:
            histogram.Draw("SAME")

        # Update max_y if the histogram's maximum value is greater
        if histogram.GetMaximum() > max_y:
            max_y = histogram.GetMaximum()

        legend.AddEntry(histogram, title, "l")
        if 'cut' in histname:
            histname.replace('_cut000', '')

    histograms_info[0][0].GetXaxis().SetTitle(histname)
    histograms_info[0][0].GetXaxis().SetTitleSize(0.04)  # Adjust axis label size
    histograms_info[0][0].GetXaxis().SetTitleOffset(1.2)  # Adjust axis label position

    histograms_info[0][0].GetYaxis().SetTitle("Normalized")  # Add a label to the y-axis
    histograms_info[0][0].GetYaxis().SetTitleSize(0.04)  # Adjust axis label size
    histograms_info[0][0].GetYaxis().SetTitleOffset(1.4)  # Adjust axis label position

    # Set the maximum y-axis value 1.4 times above max_y
    max_y *= 1.4
    histograms_info[0][0].GetYaxis().SetRangeUser(0, max_y)

    legend.Draw()
    canva.SaveAs(outFile)


def main(year, histname, normalized, channel, qcdRegion, regRegion):
    
    lumi=0.0
    if '18' in year: lumi = 59.74
    if '17' in year: lumi = 41.48

    QCDregion_files_path = Path(qcdRegion)
    regRegion_files_path = Path(regRegion)

    # Get all .root files in the directory
    all_files_QCDregion = list(QCDregion_files_path.glob('*.root'))
    all_files_regRegion = list(regRegion_files_path.glob('*.root'))

    if "el" in channel:
        dataFile_QCDregion = QCDregion_files_path / "DATA_SingleElectron.root"
        QCDfile_QCDregion = [file for file in all_files_QCDregion if "EMEnriched" in file.name and "QCD" in file.name]

    if "mu" in channel:
        dataFile_QCDregion = regRegion_files_path / "PROC_QCD_dataDriven_Muon.root" # Data in QCD Region
        dataFile_regRegion = regRegion_files_path / "DATA_SingleMuon.root" # Data in Reg Region
        QCDfile_QCDregion = [file for file in all_files_QCDregion if "MuEnriched" in file.name and "QCD" in file.name] # MC QCD files in QCD region
        QCDfile_regRegion = [file for file in all_files_regRegion if "MuEnriched" in file.name and "QCD" in file.name] # MC QCD files in Reg region
        nonQCD_mc_regRegion = [file for file in all_files_regRegion if "MuEnriched" not in file.name and "QCD" not in file.name and "Run" not in file.name and "DATA" not in file.name] # MC nonQCD files in Reg region
        nonQCD_mc_QCDregion = [file for file in all_files_QCDregion if "MuEnriched" not in file.name and "QCD" not in file.name and "Run" not in file.name and "DATA" not in file.name] # MC nonQCD files in QCD region

    # print("MC QCD files in QCD region")
    # for file in QCDfile_QCDregion:
    #     print(file)

    print("MC QCD files in Reg region")
    for file in QCDfile_regRegion:
        print(file)

    # print("MC nonQCD files in Reg region")
    # for file in nonQCD_mc_regRegion:
    #     print(file)

    # print("MC nonQCD files in QCD region")
    # for file in nonQCD_mc_QCDregion:
    #     print(file)


    # Load histograms
    # sum_MC_QCD_hist_QCDregion = hist_add(QCDfile_QCDregion, histname, normalized, channel, f"QCDin_QCDregion_{year}").Clone()
    sum_MC_QCD_hist_WJetsregion = hist_add(QCDfile_regRegion, histname, normalized, channel, f"QCDin_WJetsregion_{year}").Clone()
    # data_hist_QCDregion = getData_hist(dataFile_QCDregion, histname, normalized, channel, f"DATAin_QCDregion_{year}").Clone()

    # histosToPlot = [
    #     [sum_MC_QCD_hist_QCDregion, ROOT.kRed, "QCD in QCD region"],
    #     [data_hist_QCDregion, ROOT.kBlue, "Single Muon Data in QCD region"],
    #     [sum_MC_QCD_hist_WJetsregion, ROOT.kGreen, "QCD in WJets region"],
    # ]
    # make_plot(histosToPlot, histname, f"plots_{year}.png")

    # makeFit(nonQCD_mc_regRegion, dataFile_regRegion, QCDfile_regRegion, dataFile_QCDregion, histname, lumi)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process arguments.')
    parser.add_argument('--histname', type=str, help='Histogram name')
    parser.add_argument('--year', type=str, help='Year of analysis')
    parser.add_argument('--channel', type=str, help='Channel')
    parser.add_argument('--qcdRegion', type=str, help='QCD region files path')
    parser.add_argument('--regRegion', type=str, help='WJets region files path')
    parser.add_argument('--normalized', action='store_true', help='Normalize histograms')

    args = parser.parse_args()
    main(args.year, args.histname, args.normalized, args.channel, args.qcdRegion, args.regRegion)
