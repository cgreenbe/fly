#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ROOT
import stackhists
import CMS_lumi
import tdrstyle
import argparse

ROOT.TH1D.AddDirectory(False)


def get_prep_stack_hist(year, initial_tdrStyle, rlumi, verbose):
    """Prepare necessary variables with given year."""
    lumi_values = {
        "16pre": "19.5 fb^{-1}",
        "16post": "16.8 fb^{-1}",
        "17": "41.5 fb^{-1}",
        "18": "59.7 fb^{-1}",
        "run2": "138 fb^{-1}"
    }

    lumi_fb = {
        "16pre": 19.5,
        "16post": 16.8,
        "17": 41.48,
        "18": 59.7,
        "run2": 137.65
    }

    runs = []
    s = None

    if year in lumi_values:
        CMS_lumi.lumi_13TeV = lumi_values[year]
        s = stackhists.StackHists(ROOT, initial_tdrStyle, lumi_fb[year], verbose)
        runs = [year]

    if year == "run2":
        rlumi.update({k: v / lumi_fb["run2"] for k, v in lumi_fb.items() if k != "run2"})
        runs = list(lumi_values.keys())[:-1]

    if year != "run2":
        print(f"Year {year}")
    else:
        print("run2")

    return s, CMS_lumi.lumi_13TeV, rlumi, runs


def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="Your script description here")
    parser.add_argument('--year', '-Y', type=str, required=True, help='Year parameter')
    parser.add_argument('--channel', '-C', type=str, required=True, help='Channel parameter')
    parser.add_argument('--zonecode', '-Z', type=str, required=True, help='Zone code parameter')
    parser.add_argument('--inputfiles', '-I', type=str, required=True, help='Input files parameter')
    parser.add_argument('--region', '-Re', type=str, required=True, help='Region parameter')
    parser.add_argument('--qcddd', '-Q', action='store_true', help='Enable QCD data-driven')
    parser.add_argument('--ratio', '-Ra', action='store_true', help='Enable ratio plot')
    parser.add_argument('--significance', '-S', action='store_true', help='Enable significance plot')
    parser.add_argument('--underflow', '-Uf', action='store_true', help='Enable underflow')
    parser.add_argument('--preFit', '-Pf', action='store_true', help='Plot QCD prefitted histograms')
    parser.add_argument('--extraOutput', '-Eo', type=str, default='', help='Add extra output text if needed')
    parser.add_argument('--verbose', '-v', action='store_true', help='Enable verbose mode')

    return parser.parse_args()

def main():
    args = parse_arguments()

    # Additional setup based on arguments
    if args.extraOutput:
        CMS_lumi.outputFileName += f'_{args.extraOutput}_'

    initial_tdrStyle = ROOT.TStyle("tdrStyle", "Style for P-TDR")
    tdrstyle.setTDRStyle(ROOT, initial_tdrStyle)

    rlumi = {"16pre": 1., "16post": 1., "17": 1., "18": 1.}
    s, CMS_lumi.lumi_13TeV, rlumi, runs = get_prep_stack_hist(args.year, initial_tdrStyle, rlumi, args.verbose)

    # Initialize variables based on arguments
    year = args.year
    channel = args.channel
    zonecode = args.zonecode if 'nocut' in args.zonecode else 'cut' + args.zonecode
    inputfiles = args.inputfiles
    region = args.region
    qcddd = args.qcddd
    ratio = args.ratio
    significance = args.significance
    underflow = args.underflow
    preFit = args.preFit
    extraOutput = args.extraOutput
    verbose = args.verbose

    # Set up CMS lumi and style options
    CMS_lumi.extraText = ''
    CMS_lumi.cmsText = ''
    s.setupStyle(alpha=1)

    # Define histogram color coding
    signalColor = 0
    alt_wzjetsColor = 1
    STColor = 4
    ttbarColor = 3
    TTXColor = 5
    wzjetsColor = 10
    dibosonColor = 14
    drellYanColor = 13
    QCDColor =  8
    DataColor = 999

    # Scale factors
    monteCarlo_SF = 1
    wjets_SF = 1
    dataDriven_SF = 1

    # Fit specific part of the script
    if qcddd: 
        s.stackOrder = ["QCD_{DataDriven}", "W/Z-Jets", "t#bar{t}"]
        s.preFit = preFit
    else: 
        s.stackOrder = ["QCD_{MC}", "W/Z-Jets", "t#bar{t}"]

    s.sum_of_weights_hist_name = 'genweight_sumOf_nocut'
    s.histName_forFit = 'Wboson_transversMass_cut000'
    s.iso_cut_rev_iso_mu = '>0.45'
    s.iso_cut_iso_mu = '<0.15'
    QCD_folder_name = 'QCDregion_0p45/'
    QCD_folder_pwd = inputfiles + QCD_folder_name
    s.QCDregion_folder = QCD_folder_name

    # Loop through runs and set up channels
    for run in runs:
        if run == "16pre":
            print("Run2016_pre")
        
        elif run == "16post":
            print("Run2016_post")
        
        elif run == "17":
            print("Run2017")
            s.addChannel(inputfiles + 'PROC_ST_t-channel_top.root', 't-channel', signalColor, isMC=True, xsec=136.02*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_t-channel_antitop.root', 't-channel', signalColor, isMC=True, xsec=80.95*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

            s.addChannel(inputfiles + 'PROC_ST_s-channel.root', 's-channel + tW', STColor, isMC=True, xsec=3.68*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_tW_top.root', 's-channel + tW', STColor, isMC=True, xsec=35.85*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_tW_antitop.root', 's-channel + tW', STColor, isMC=True, xsec=35.85*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

            s.addChannel(inputfiles + 'PROC_TTToSemiLeptonic.root', 't#bar{t}', ttbarColor, isMC=True, xsec=364.351*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTTo2L2Nu.root', 't#bar{t}', ttbarColor, isMC=True, xsec=87.31*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            
            s.addChannel(inputfiles + 'PROC_TTWJetsToLNu.root', 'tt + X', TTXColor, isMC=True, xsec=0.2045*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTWJetsToQQ.root', 'tt + X', TTXColor, isMC=True, xsec=0.4062*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTZToLLNuNu.root', 'tt + X', TTXColor, isMC=True, xsec=0.2529*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTZToQQ.root', 'tt + X', TTXColor, isMC=True, xsec=0.5297*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '') 
            
            s.addChannel(inputfiles + 'PROC_WJetsToLNu.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=61334.9*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            
            s.addChannel(inputfiles + 'PROC_WW.root', 'Diboson', dibosonColor, isMC=True, xsec=118.7*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WZ.root', 'Diboson', dibosonColor, isMC=True, xsec=47.13*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ZZ.root', 'Diboson', dibosonColor, isMC=True, xsec=16.523*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
           
            s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-10to50.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=18610*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-50.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=6025.2*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            
            if 'mu' in channel:
                if not qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-15To20_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=3336011*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-20To30_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=3987854.9*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-30To50_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1705381*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-50To80_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=395178*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-80To120_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=106889.4*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-120To170_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=23773.61*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-170To300_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=8292.9*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-300To470_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=797.4*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-470To600_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=56.6*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-600To800_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=25.1*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-800To1000_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=4.7*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-1000_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1.6*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                
                if qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_dataDriven_Muon.root', 'QCD_{DataDriven}', QCDColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')
                
                
                s.addChannel(inputfiles + 'DATA_SingleMuon.root', 'Data', DataColor, isMC=False)
                

            if 'el' in channel:
                if not qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-20to30_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=4948840*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-30to50_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=6324800*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-50to80_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1806336*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-80to120_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=380538*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-120to170_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=66634.3*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-170to300_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=20859*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-300toInf_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1350*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                if qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_dataDriven_Muon.root', 'QCD_{DataDriven}', QCDColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')

                s.addChannel(inputfiles + 'DATA_SingleElectron.root', 'Data', DataColor, isMC=False)

        elif run == "18":
            print("Run2018")
            s.addChannel(inputfiles + 'PROC_ST_t-channel_top.root', 't-channel', signalColor, isMC=True, xsec=136.02*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_t-channel_antitop.root', 't-channel', signalColor, isMC=True, xsec=80.95*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

            s.addChannel(inputfiles + 'PROC_ST_s-channel.root', 's-channel + tW', STColor, isMC=True, xsec=3.68*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_tW_top.root', 's-channel + tW', STColor, isMC=True, xsec=35.85*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_tW_antitop.root', 's-channel + tW', STColor, isMC=True, xsec=35.85*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

            s.addChannel(inputfiles + 'PROC_TTToSemiLeptonic.root', 't#bar{t}', ttbarColor, isMC=True, xsec=364.351*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTTo2L2Nu.root', 't#bar{t}', ttbarColor, isMC=True, xsec=87.31*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            
            s.addChannel(inputfiles + 'PROC_TTWJetsToLNu.root', 'tt + X', TTXColor, isMC=True, xsec=0.2045*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTWJetsToQQ.root', 'tt + X', TTXColor, isMC=True, xsec=0.4062*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTZToLLNuNu.root', 'tt + X', TTXColor, isMC=True, xsec=0.2529*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTZToQQ.root', 'tt + X', TTXColor, isMC=True, xsec=0.5297*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '') 
            
            # s.addChannel(inputfiles + 'PROC_WJetsToLNu.root', 'W-Jets', wzjetsColor, isMC=True, xsec=61334.9*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WJetsToLNu_0J.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=49240*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WJetsToLNu_1J.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=8236*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WJetsToLNu_2J.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=2736*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            
            s.addChannel(inputfiles + 'PROC_WW.root', 'Diboson', dibosonColor, isMC=True, xsec=118.7*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WZ.root', 'Diboson', dibosonColor, isMC=True, xsec=47.13*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ZZ.root', 'Diboson', dibosonColor, isMC=True, xsec=16.523*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
           
            s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-10to50.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=18610*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-50.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=6025.2*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            
            if 'mu' in channel:
                if not qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-15To20_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=3336011*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-20To30_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=3987854.9*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-30To50_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1705381*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-50To80_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=395178*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-80To120_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=106889.4*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-120To170_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=23773.61*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-170To300_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=8292.9*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-300To470_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=797.4*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-470To600_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=56.6*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-600To800_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=25.1*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-800To1000_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=4.7*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-1000_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1.6*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                
                if qcddd:
                    s.addChannel(QCD_folder_pwd + 'PROC_QCD_dataDriven_Muon.root', 'QCD_{DataDriven}', QCDColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')
                    # Give the list of QCD files that will be used for QCD Fit in QCD and REG regions
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-15To20_MuEnriched.root', xsec=3336011*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-20To30_MuEnriched.root', xsec=3987854.9*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-30To50_MuEnriched.root', xsec=1705381*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-50To80_MuEnriched.root', xsec=395178*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-80To120_MuEnriched.root', xsec=106889.4*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-120To170_MuEnriched.root', xsec=23773.61*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-170To300_MuEnriched.root', xsec=8292.9*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-300To470_MuEnriched.root', xsec=797.4*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-470To600_MuEnriched.root', xsec=56.6*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-600To800_MuEnriched.root', xsec=25.1*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-800To1000_MuEnriched.root', xsec=4.7*10**3)
                    s.add_mcQCD_channel_forFit(inputfiles + 'PROC_QCD_Pt-1000_MuEnriched.root', xsec=1.6*10**3)

                s.addChannel(inputfiles + 'DATA_SingleMuon.root', 'Data', DataColor, isMC=False)
                

            if 'el' in channel:
                s.addChannel(inputfiles + 'PROC_QCD_Pt-20to30_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=4948840*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-30to50_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=6324800*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-50to80_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=1806336*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-80to120_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=380538*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-120to170_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=66634.3*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-170to300_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=20859*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-300toInf_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=1350*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

                s.addChannel(inputfiles + 'DATA_SingleElectron.root', 'Data', DataColor, isMC=False)

    
    from histogram_adder import (
    add_mu_histograms_to_stack,
    add_el_histograms_to_stack,
    add_elmu_histograms_to_stack
    )

    if 'mu' in channel:
        add_mu_histograms_to_stack(s, zonecode)
    elif 'el' in channel:
        add_el_histograms_to_stack(s, zonecode)

    add_elmu_histograms_to_stack(s, zonecode)

    # Drawing histograms and final output
    subplot = "R" if ratio else "S" if significance else ""
    s.draw(subplot, inputfiles, region, channel, qcddd)


if __name__ == '__main__':
    main()































# Histograms Part
# if 'mu' in channel and not 'nocut' in zonecode:
    # s.addHistogram('iso_mu_leading_pt_' + zonecode, 'tight iso muon p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
    # s.addHistogram('iso_mu_leading_eta_' + zonecode, 'tight iso muon eta', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
    # s.addHistogram('iso_mu_leading_phi_' + zonecode, 'tight iso muon phi', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
    
    # s.addHistogram('N_iso_mu_' + zonecode, 'Number of iso tight muons', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', is_logx=False, extratext = '')



# if 'el' in channel and not 'nocut' in zonecode:
#     s.addHistogram('iso_el_leading_pt_' + zonecode, 'isolated electron p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
#     s.addHistogram('iso_el_leading_eta_' + zonecode, 'isolated electron eta', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
#     s.addHistogram('iso_el_leading_phi_' + zonecode, 'isolated electron phi', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
    # s.addHistogram('log10_rev_iso_el_rel_iso_collection_' + zonecode, 'electron Rel Iso', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')


############ MET ############
# s.addHistogram('MET_PF_pt_' + zonecode, 'PF MET p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
# s.addHistogram('MET_PF_phi_' + zonecode, 'PF MET p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
# s.addHistogram('MET_pt_corr_' + zonecode, 'PF MET p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
# s.addHistogram('MET_phi_corr_' + zonecode, 'PF MET phi', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
# s.addHistogram('MET_pt_phimodcorr_' + zonecode, 'PF MET p_{T} [GeV] phimodcorr', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
# s.addHistogram('MET_phi_phimodcorr_' + zonecode, 'PF MET phi phimodcorr', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')



# s.addHistogram('MET_Puppi_pt_' + zonecode, 'Puppi MET p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')

############ Bjets ############
# s.addHistogram('tightBJet_leading_eta_' + zonecode, 'Leading tight b jet eta', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
# s.addHistogram('tightBJet_leading_pt_' + zonecode, 'Leading tight b jet pT [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')


# ############ W boson ############
# s.addHistogram('Wboson_transversMass_' + zonecode, 'm_{T,W} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', extratext='')


# ############ Isolation ############
# s.addHistogram('log10_nonIso_tight_mu_relIso_col_' + zonecode, 'log_{10}(I^{dR=0.4}_{rel})', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', is_logx=True, extratext='', underflow_bin=True)
# s.addHistogram('nonIso_tight_mu_relIso_col_' + zonecode, 'I^{dR=0.4}_{rel}', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', extratext='')
# s.addHistogram('log10_iso_mu_relIso_dR0p4_collection_' + zonecode, 'I^{dR=0.4}_{rel} iso mu', draw_mode=stackhists.STACKED, draw_option='hist', extratext='', is_logx=True, underflow_bin=True)

# s.addHistogram("log10_nonIso_mu_relIso_col_" + zonecode, "muon Iso_{Rel}^{dR=0.4}", 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')

############ other ############
# s.addHistogram('NTightBJets_' + zonecode, 'Number of tight b jets', draw_mode=stackhists.STACKED, draw_option='hist', extratext='')