#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ROOT
import stackhists
import CMS_lumi
import tdrstyle
import click
import sys
from pathlib import Path
import argparse
from QCDstudy import makeFit, hist_add, make_plot, getData_hist

ROOT.TH1D.AddDirectory(False)

# sys.path.append("..") # Add parent directory to system path
# import hists_names as histName # Import module from parent directory
# import printColors as color


def get_prep_stack_hist(year, initial_tdrStyle, rlumi, verbose):
    """Prepare necessary variables with given year

    Args:
        year: given year
        initial_tdrStyle: initialized tdrStyle instance
        rlumi list
    Return:
        s, CMS_lumi.lumi_13TeV, rlumi, runs
    """
    runs = []
    s = None
    if year == "16pre":
        CMS_lumi.lumi_13TeV = "19.5 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 19.5, verbose)
        runs = ["16pre"]
    
    if year == "16post":
        CMS_lumi.lumi_13TeV = "16.8 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 16.8, verbose)
        runs = ["16post"]
    
    elif year == "17":
        CMS_lumi.lumi_13TeV = "41.5 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 41.48, verbose)
        runs = ["17"]
    
    elif year == "18":
        CMS_lumi.lumi_13TeV = "59.7 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 59.7, verbose)
        runs = ["18"]
    
    elif year == "run2":
        CMS_lumi.lumi_13TeV = "138 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 137.65, verbose)
        rlumi["16pre"] = 19.5 / 137.65
        rlumi["16post"] = 16.8 / 137.65
        rlumi["17"] = 41.48 / 137.65
        # rlumi["18"] = 59.83/137.65
        rlumi["18"] = 150.0 / 137.65
        runs = ["16pre", "16post", "17", "18"]

    # DATA
    if year == "run2":
        print("run2")
    else:
        print("no run2")

    return s, CMS_lumi.lumi_13TeV, rlumi, runs



def main():
    parser = argparse.ArgumentParser(description="Your script description here")
    parser.add_argument('--ratio', '-Ra', action='store_true', default=False, help='Enable ratio plot')
    parser.add_argument('--significance', '-S', action='store_true', default=False, help='Enable significance plot')
    parser.add_argument('--logstyle', '-L', action='store_true', default=False, help='Use log scale for y-axis')
    parser.add_argument('--verbose', '-v', action='store_true', default=False, help='Enable verbose mode')
    parser.add_argument('--year', '-Y', type=str, default="", help='Year parameter')
    parser.add_argument('--channel', '-C', type=str, default='', help='Channel parameter')
    parser.add_argument('--zonecode','-Z', type=str, default='', help='Zone code parameter')
    parser.add_argument('--inputfiles','-I', type=str, default='', help='Input files parameter')
    parser.add_argument('--region','-Re', type=str, default='', help='Region parameter')
    parser.add_argument('--qcddd', '-Q', action='store_true', default=False, help='Enable QCD data-driven')
    parser.add_argument('--underflow','-Uf', action='store_true', default=False, help='Enable underflow')
    parser.add_argument('--preFit', '-Pf', action='store_true', default=False, help='Plot QCD prefitted histograms')
    parser.add_argument('--extraOutput','-Eo', type=str, default='', help='Add extra output text if needed')

    args = parser.parse_args()

    if not args.year or not args.channel or not args.zonecode or not args.inputfiles or not args.region:
        parser.error("Please provide values for year, channel, zonecode, inputfiles, and region.")

    year = args.year
    channel = args.channel
    zonecode = args.zonecode
    inputfiles = args.inputfiles
    region = args.region
    qcddd = args.qcddd
    ratio = args.ratio
    significance = args.significance
    underflow = args.underflow
    preFit = args.preFit
    extraOutput = args.extraOutput

    if extraOutput!='': CMS_lumi.outputFileName += '_' + extraOutput + '_'

    verbose = args.verbose
    # Subplot ( Ratio plot : option -R, --ratio (default) / significance plot :  option -S, --significance )
    initial_tdrStyle = ROOT.TStyle("tdrStyle", "Style for P-TDR")
    tdrstyle.setTDRStyle(ROOT, initial_tdrStyle)

    sys = "_norm"
    # Lumi ratio dictionary for integrated Run2
    rlumi = {"16pre": 1., "16post": 1., "17": 1., "18": 1.}
    s, CMS_lumi.lumi_13TeV, rlumi, runs = get_prep_stack_hist(year, initial_tdrStyle, rlumi, verbose)

    s.setupStyle(alpha=1)
    CMS_lumi.extraText = ''
    CMS_lumi.cmsText = ''

    signalColor = 0
    alt_wzjetsColor = 1
    STColor = 4
    ttbarColor = 3
    TTXColor = 5
    wzjetsColor = 10
    dibosonColor = 14
    drellYanColor = 13
    QCDColor =  8
    DataColor = 999

    if not 'nocut' in zonecode:
        zonecode = 'cut' + zonecode
    
    monteCarlo_SF_num = 1
    monteCarlo_SF_denom = 1

    monteCarlo_SF = monteCarlo_SF_num/monteCarlo_SF_denom

    wjets_SF = 1
    dataDriven_SF = 1

    ###### Fit Specific part of the script ######
    if qcddd:

        if '18' in year: lumii=59.7
        if '17' in year: lumii=41.48
        if '16post' in year: lumii=16.8
        if '16pre' in year: lumii=19.5

        histoForQCDfit = "Wboson_transversMass_cut000"

        pathToFiles = Path(inputfiles)
        list_of_all_inputFiles = list(pathToFiles.glob('*.root'))

        if 'mu' in channel:
            dataFile = pathToFiles / 'DATA_SingleMuon.root'
            qcd_dataDriven_file = pathToFiles / 'PROC_QCD_dataDriven_Muon.root'
            nonQCD_inputFiles = [file for file in list_of_all_inputFiles if "MuEnriched" not in file.name and "QCD" not in file.name and "Run" not in file.name and "DATA" not in file.name]
            qcd_inputFiles = [file for file in list_of_all_inputFiles if "MuEnriched" in file.name and "QCD" in file.name]
        
        #>>>>>>>>>>> The Following block is only when one wishes to plot mcQCD in QCD region################################
        pathTo_mcQCD_inQCDregion = Path(inputfiles + '/QCDregion')
        list_mcQCD_inQCDregion_files = list(pathTo_mcQCD_inQCDregion.glob('*.root'))
        if 'mu' in channel:
            qcd_QCDregion_inputFiles = [file for file in list_mcQCD_inQCDregion_files if "MuEnriched" in file.name and "QCD" in file.name]
        if 'el' in channel:
            qcd_QCDregion_inputFiles = [file for file in list_mcQCD_inQCDregion_files if "EmEnriched" in file.name and "QCD" in file.name]

        mcQCD_inQCDregion_hist = hist_add(qcd_QCDregion_inputFiles, histoForQCDfit, True, channel, region, lumii).Clone()
        #<<<<<<<<<<<#######################################################################################################

        qcd_data_driven_hist = getData_hist(qcd_dataDriven_file, histoForQCDfit, True, channel, f"DATAin_QCDregion_{year}").Clone()
        qcd_mc_hist = hist_add(qcd_inputFiles, histoForQCDfit, True, channel, region, lumii).Clone()

        histosToPlot = [
        [qcd_data_driven_hist, ROOT.kRed, "Data Driven QCD"],
        [qcd_mc_hist, ROOT.kBlue, "QCD in WJets Region"],
        [mcQCD_inQCDregion_hist, ROOT.kGreen, "QCD in QCD region"],
        ]

        make_plot(histosToPlot, histoForQCDfit, f"{inputfiles}comparaisonQCPlot_{year}.png")


        monteCarlo_SF, wjets_SF, QCDdataDriven_SF, QCDdataDriven_normalization = makeFit(nonQCD_inputFiles, dataFile, qcd_inputFiles, qcd_dataDriven_file, histoForQCDfit, lumii)

        # >>>>>>>>>>>>>>> Create a simple table to keep generated SF ####################
        table_text = f"""
        +---------------------+----------------------+
        | Variable            | Value                |
        +---------------------+----------------------+
        | all_mc_sf           | {monteCarlo_SF:.2f}  |
        | wjets_sf            | {wjets_SF:.2f}  |
        | QCD_dataDriven_sf   | {dataDriven_SF:.2f}  |
        +---------------------+----------------------+
        """

        # File path to save the text file
        file_path = inputfiles + 'scaleFactors.txt'

        # Save the text to a file
        with open(file_path, "w") as f:
            f.write(table_text)

        # file_path
        # <<<<<<<<<<<<<<< ###############################################################

        if preFit:
            
            dataDriven_SF = QCDdataDriven_normalization
            monteCarlo_SF = 1
            wjets_SF = 1
            CMS_lumi.extraText += 'prefit'
            CMS_lumi.outputFileName += '_prefit'
            
        else:
            dataDriven_SF = QCDdataDriven_normalization*QCDdataDriven_SF

        print(f'QCDdataDriven_normalization = {QCDdataDriven_normalization}')
        print(f'QCDdataDriven_SF = {QCDdataDriven_SF}')
        print(f'dataDriven_SF = {dataDriven_SF}')
        print(f'monteCarlo_SF = {monteCarlo_SF}')
        print(f'wjets_SF = {wjets_SF}')
    

    if monteCarlo_SF != 1 and monteCarlo_SF_denom != 1: 
        CMS_lumi.extraText += f' MC Scale:{monteCarlo_SF_num}/{monteCarlo_SF_denom}'


    for run in runs:
        if run == "16pre":
            print("Run2016_pre")
        
        elif run == "16post":
            print("Run2016_post")
        
        elif run == "17":
            print("Run2017")
            s.addChannel(inputfiles + 'PROC_ST_t-channel_top.root', 't-channel', signalColor, isMC=True, xsec=136.02*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_t-channel_antitop.root', 't-channel', signalColor, isMC=True, xsec=80.95*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

            s.addChannel(inputfiles + 'PROC_ST_s-channel.root', 's-channel + tW', STColor, isMC=True, xsec=3.68*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_tW_top.root', 's-channel + tW', STColor, isMC=True, xsec=35.85*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_tW_antitop.root', 's-channel + tW', STColor, isMC=True, xsec=35.85*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

            s.addChannel(inputfiles + 'PROC_TTToSemiLeptonic.root', 't#bar{t}', ttbarColor, isMC=True, xsec=364.351*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTTo2L2Nu.root', 't#bar{t}', ttbarColor, isMC=True, xsec=87.31*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            
            s.addChannel(inputfiles + 'PROC_TTWJetsToLNu.root', 'tt + X', TTXColor, isMC=True, xsec=0.2045*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTWJetsToQQ.root', 'tt + X', TTXColor, isMC=True, xsec=0.4062*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTZToLLNuNu.root', 'tt + X', TTXColor, isMC=True, xsec=0.2529*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTZToQQ.root', 'tt + X', TTXColor, isMC=True, xsec=0.5297*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '') 
            
            s.addChannel(inputfiles + 'PROC_WJetsToLNu.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=61334.9*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            
            s.addChannel(inputfiles + 'PROC_WW.root', 'Diboson', dibosonColor, isMC=True, xsec=118.7*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WZ.root', 'Diboson', dibosonColor, isMC=True, xsec=47.13*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ZZ.root', 'Diboson', dibosonColor, isMC=True, xsec=16.523*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
           
            s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-10to50.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=18610*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-50.root', 'W/Z-Jets', wzjetsColor, isMC=True, xsec=6025.2*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            
            if 'mu' in channel:
                if not qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-15To20_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=3336011*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-20To30_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=3987854.9*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-30To50_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1705381*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-50To80_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=395178*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-80To120_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=106889.4*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-120To170_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=23773.61*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-170To300_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=8292.9*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-300To470_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=797.4*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-470To600_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=56.6*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-600To800_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=25.1*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-800To1000_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=4.7*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-1000_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1.6*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                
                if qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_dataDriven_Muon.root', 'QCD_{DataDriven}', QCDColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')
                
                
                s.addChannel(inputfiles + 'DATA_SingleMuon.root', 'Data', DataColor, isMC=False)
                

            if 'el' in channel:
                if not qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-20to30_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=4948840*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-30to50_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=6324800*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-50to80_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1806336*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-80to120_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=380538*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-120to170_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=66634.3*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-170to300_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=20859*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-300toInf_EMEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1350*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                if qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_dataDriven_Muon.root', 'QCD_{DataDriven}', QCDColor, isMC=True, xsec=1, scale_factor=dataDriven_SF, counter_histogram_root = '')

                s.addChannel(inputfiles + 'DATA_SingleElectron.root', 'Data', DataColor, isMC=False)


        
        elif run == "18":
            print("Run2018")
            s.addChannel(inputfiles + 'PROC_ST_t-channel_top.root', 't-channel', signalColor, isMC=True, xsec=136.02*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_t-channel_antitop.root', 't-channel', signalColor, isMC=True, xsec=80.95*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

            s.addChannel(inputfiles + 'PROC_ST_s-channel.root', 's-channel + tW', STColor, isMC=True, xsec=3.68*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_tW_top.root', 's-channel + tW', STColor, isMC=True, xsec=35.85*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ST_tW_antitop.root', 's-channel + tW', STColor, isMC=True, xsec=35.85*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

            s.addChannel(inputfiles + 'PROC_TTToSemiLeptonic.root', 't#bar{t}', ttbarColor, isMC=True, xsec=364.351*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTTo2L2Nu.root', 't#bar{t}', ttbarColor, isMC=True, xsec=87.31*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            
            s.addChannel(inputfiles + 'PROC_TTWJetsToLNu.root', 'tt + X', TTXColor, isMC=True, xsec=0.2045*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTWJetsToQQ.root', 'tt + X', TTXColor, isMC=True, xsec=0.4062*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTZToLLNuNu.root', 'tt + X', TTXColor, isMC=True, xsec=0.2529*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_TTZToQQ.root', 'tt + X', TTXColor, isMC=True, xsec=0.5297*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '') 
            
            s.addChannel(inputfiles + 'PROC_WJetsToLNu.root', 'W-Jets', wzjetsColor, isMC=True, xsec=61334.9*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WJetsToLNu_0J.root', 'W-Jets_xJ', alt_wzjetsColor, isMC=True, xsec=49240*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WJetsToLNu_1J.root', 'W-Jets_xJ', alt_wzjetsColor, isMC=True, xsec=8236*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WJetsToLNu_2J.root', 'W-Jets_xJ', alt_wzjetsColor, isMC=True, xsec=2736*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
            
            s.addChannel(inputfiles + 'PROC_WW.root', 'Diboson', dibosonColor, isMC=True, xsec=118.7*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_WZ.root', 'Diboson', dibosonColor, isMC=True, xsec=47.13*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_ZZ.root', 'Diboson', dibosonColor, isMC=True, xsec=16.523*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
           
            s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-10to50.root', 'DY', drellYanColor, isMC=True, xsec=18610*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-50.root', 'DY', drellYanColor, isMC=True, xsec=6025.2*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
            
            if 'mu' in channel:
                if not qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-15To20_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=3336011*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-20To30_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=3987854.9*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-30To50_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1705381*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-50To80_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=395178*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-80To120_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=106889.4*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-120To170_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=23773.61*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-170To300_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=8292.9*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-300To470_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=797.4*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-470To600_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=56.6*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-600To800_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=25.1*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-800To1000_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=4.7*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                    s.addChannel(inputfiles + 'PROC_QCD_Pt-1000_MuEnriched.root', 'QCD_{MC}', QCDColor, isMC=True, xsec=1.6*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                
                if qcddd:
                    s.addChannel(inputfiles + 'PROC_QCD_dataDriven_Muon.root', 'QCD_{DataDriven}', QCDColor, isMC=True, xsec=10**3, scale_factor=dataDriven_SF, counter_histogram_root = '')

                s.addChannel(inputfiles + 'DATA_SingleMuon.root', 'Data', DataColor, isMC=False)
                

            if 'el' in channel:
                s.addChannel(inputfiles + 'PROC_QCD_Pt-20to30_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=4948840*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-30to50_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=6324800*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-50to80_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=1806336*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-80to120_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=380538*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-120to170_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=66634.3*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-170to300_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=20859*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')
                s.addChannel(inputfiles + 'PROC_QCD_Pt-300toInf_EMEnriched.root', 'QCD', QCDColor, isMC=True, xsec=1350*10**3, scale_factor=monteCarlo_SF, counter_histogram_root = '')

                s.addChannel(inputfiles + 'DATA_SingleElectron.root', 'Data', DataColor, isMC=False)

    ###############################################################
    ##                       Histograms Part    
    ##                   Options: NORMALIZED / STACKED
    ###############################################################


    # if 'mu' in channel and not 'nocut' in zonecode:
        # s.addHistogram('iso_mu_leading_pt_' + zonecode, 'tight muon p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
        # s.addHistogram('iso_mu_leading_eta_' + zonecode, 'tight muon eta', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
        # s.addHistogram('iso_mu_leading_phi_' + zonecode, 'tight muon phi', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
        
    #     # s.addHistogram('log10_relativeIso04_nonIso_iso_mus_' + zonecode, 'tight muon I_{rel} (dR = 0.4) (log)', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', is_logx=True, extratext = '')
    #     # s.addHistogram('log10_relativeIso03_nonIso_iso_mus_' + zonecode, 'tight muon I_{rel} (dR = 0.3) (log)', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', is_logx=True, extratext = '')
    #     # s.addHistogram('log10_relativeIso04_nonIso_looseMuons_' + zonecode, 'loose muon I_{rel} (dR = 0.4) (log)', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', is_logx=True, extratext = '')
    #     s.addHistogram('NlooseMuons_' + zonecode, 'Number of loose muons', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', is_logx=False, extratext = '')



    if 'el' in channel and not 'nocut' in zonecode:
        s.addHistogram('iso_el_leading_pt_' + zonecode, 'isolated electron p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
        s.addHistogram('iso_el_leading_eta_' + zonecode, 'isolated electron eta', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
        s.addHistogram('iso_el_leading_phi_' + zonecode, 'isolated electron phi', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
        # s.addHistogram('log10_rev_iso_el_rel_iso_collection_' + zonecode, 'electron Rel Iso', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')


    ############ MET ############
    # s.addHistogram('MET_PF_pt_' + zonecode, 'PF MET p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
    # s.addHistogram('MET_Puppi_pt_' + zonecode, 'Puppi MET p_{T} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')

    ############ Bjets ############
    # s.addHistogram('tightBJet_leading_eta_' + zonecode, 'Leading tight b jet eta', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')
    # s.addHistogram('tightBJet_leading_pt_' + zonecode, 'Leading tight b jet pT [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')


    ############ W boson ############
    s.addHistogram('Wboson_transversMass_' + zonecode, 'm_{T,W} [GeV]', 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist', extratext='')

    # s.addHistogram("log10_nonIso_mu_relIso_col_" + zonecode, "muon Iso_{Rel}^{dR=0.4}", 'Events/bin', draw_mode=stackhists.STACKED, draw_option='hist')

    ############ other ############
    # s.addHistogram('NTightBJets_' + zonecode, 'Number of tight b jets', draw_mode=stackhists.STACKED, draw_option='hist', extratext='')


    subplot = ""
    if ratio:
        subplot = "R"
    elif significance:
        subplot = "S"
    s.draw(subplot, inputfiles, region, channel, qcddd, underflow)


if __name__ == '__main__':
    main()