import stackhists


def add_mu_histograms_to_stack(s, zonecode):
    # Define histograms specific to the muon channel
    muon_histograms = [

        {"name": 'iso_mu_leading_pt_' + zonecode, "title": 'iso_mu_pt [GeV]', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},


    ]

    for hist in muon_histograms:
        s.addHistogram(hist["name"], hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], is_logx=hist.get("is_logx", False), underflow_bin=hist.get("underflow_bin", False), extratext=hist.get("extratext", ''))

def add_el_histograms_to_stack(s, zonecode):
    # Define histograms specific to the electron channel
    electron_histograms = [

        {"name": 'iso_el_leading_pt_' + zonecode, "title": 'Isolated Electron p_{T} [GeV]', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},


    ]

    for hist in electron_histograms:
        s.addHistogram(hist["name"], hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"])


def add_elmu_histograms_to_stack(s, zonecode):
    # Define histograms common to both muon and electron channels
    common_histograms = [
        
        # MET
        {"name": 'MET_pt_phimodcorr_' + zonecode, "title": 'PF MET p_{T} [GeV] phimodcorr', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},
        {"name": 'MET_phi_phimodcorr_' + zonecode, "title": 'PF MET phi phimodcorr', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist'},

        # W boson
        {"name": 'Wboson_transversMass_' + zonecode, "title": 'm_{T,W} [GeV]', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist', "extratext": ''},

        #Isolation
        {"name": 'nonIso_tight_mu_relIso_col_' + zonecode, "title": 'non_isolated_tight_muon_relative_isolation', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED, "draw_option": 'hist', "extratext": ''},
        {"name": 'log10_iso_mu_relIso_dR0p4_collection_' + zonecode, "title": 'log10_iso_mu_rel_iso', "y_label": 'Events/bin', "draw_mode": stackhists.STACKED,  "draw_option": 'hist', "extratext": ''}

    ]

    for hist in common_histograms:
        s.addHistogram(hist["name"], hist["title"], hist["y_label"], draw_mode=hist["draw_mode"], draw_option=hist["draw_option"], extratext=hist.get("extratext", ''))


