#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ROOT
import stackhists
import CMS_lumi
import tdrstyle
import argparse

ROOT.TH1D.AddDirectory(False)


def get_prep_stack_hist(year, initial_tdrStyle, rlumi, verbose):
    """Prepare necessary variables with given year."""
    lumi_values = {
        "16pre": "19.5 fb^{-1}",
        "16post": "16.8 fb^{-1}",
        "17": "41.5 fb^{-1}",
        "18": "59.7 fb^{-1}",
        "run2": "138 fb^{-1}"
    }

    lumi_fb = {
        "16pre": 19.5,
        "16post": 16.8,
        "17": 41.48,
        "18": 59.7,
        "run2": 137.65
    }

    runs = []
    s = None

    if year in lumi_values:
        CMS_lumi.lumi_13TeV = lumi_values[year]
        s = stackhists.StackHists(ROOT, initial_tdrStyle, lumi_fb[year], verbose)
        runs = [year]

    if year == "run2":
        rlumi.update({k: v / lumi_fb["run2"] for k, v in lumi_fb.items() if k != "run2"})
        runs = list(lumi_values.keys())[:-1]

    if year != "run2":
        print(f"Year {year}")
    else:
        print("run2")

    return s, CMS_lumi.lumi_13TeV, rlumi, runs


def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="Your script description here")
    parser.add_argument('--year', '-Y', type=str, required=True, help='Year parameter')
    parser.add_argument('--channel', '-C', type=str, required=True, help='Channel parameter')
    parser.add_argument('--zonecode', '-Z', type=str, required=True, help='Zone code parameter')
    parser.add_argument('--inputfiles', '-I', type=str, required=True, help='Input files parameter')
    parser.add_argument('--region', '-Re', type=str, required=True, help='Region parameter')
    parser.add_argument('--qcddd', '-Q', action='store_true', help='Enable QCD data-driven')
    parser.add_argument('--ratio', '-Ra', action='store_true', help='Enable ratio plot')
    parser.add_argument('--significance', '-S', action='store_true', help='Enable significance plot')
    parser.add_argument('--underflow', '-Uf', action='store_true', help='Enable underflow')
    parser.add_argument('--preFit', '-Pf', action='store_true', help='Plot QCD prefitted histograms')
    parser.add_argument('--extraOutput', '-Eo', type=str, default='', help='Add extra output text if needed')
    parser.add_argument('--verbose', '-v', action='store_true', help='Enable verbose mode')

    return parser.parse_args()

def main():
    args = parse_arguments()

    # Additional setup based on arguments
    if args.extraOutput:
        CMS_lumi.outputFileName += f'_{args.extraOutput}_'

    initial_tdrStyle = ROOT.TStyle("tdrStyle", "Style for P-TDR")
    tdrstyle.setTDRStyle(ROOT, initial_tdrStyle)

    rlumi = {"16pre": 1., "16post": 1., "17": 1., "18": 1.}
    s, CMS_lumi.lumi_13TeV, rlumi, runs = get_prep_stack_hist(args.year, initial_tdrStyle, rlumi, args.verbose)

    # Initialize variables based on arguments
    year = args.year
    channel = args.channel
    zonecode = args.zonecode if 'nocut' in args.zonecode else 'cut' + args.zonecode
    inputfiles = args.inputfiles
    region = args.region
    qcddd = args.qcddd
    ratio = args.ratio
    significance = args.significance
    underflow = args.underflow
    preFit = args.preFit
    extraOutput = args.extraOutput
    verbose = args.verbose

    s.sum_of_weights_hist_name = 'genweight_sumOf_nocut'

    s.addChannel(inputfiles + 'PROC_ST_t-channel_top.root', 'signal', 0, isMC=True, xsec=136.02*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_ST_t-channel_antitop.root', 'signal', 0, isMC=True, xsec=80.95*10**3, scale_factor=1, counter_histogram_root = '')

    s.addChannel(inputfiles + 'PROC_ST_s-channel.root', 'singletop', 0, isMC=True, xsec=3.68*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_ST_tW_top.root', 'singletop', 0, isMC=True, xsec=35.85*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_ST_tW_antitop.root', 'singletop', 0, isMC=True, xsec=35.85*10**3, scale_factor=1, counter_histogram_root = '')

    s.addChannel(inputfiles + 'PROC_TTToSemiLeptonic.root', 'ttbar', 0, isMC=True, xsec=364.351*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_TTTo2L2Nu.root', 'ttbar', 0, isMC=True, xsec=87.31*10**3, scale_factor=1, counter_histogram_root = '')
    
    s.addChannel(inputfiles + 'PROC_TTWJetsToLNu.root', 'ttbar', 0, isMC=True, xsec=0.2045*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_TTWJetsToQQ.root', 'ttbar', 0, isMC=True, xsec=0.4062*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_TTZToLLNuNu.root', 'ttbar', 0, isMC=True, xsec=0.2529*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_TTZToQQ.root', 'ttbar', 0, isMC=True, xsec=0.5297*10**3, scale_factor=1, counter_histogram_root = '') 
    
    # s.addChannel(inputfiles + 'PROC_WJetsToLNu.root', 'W-Jets', wzjetsColor, isMC=True, xsec=61334.9*10**3, scale_factor=wjets_SF, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_WJetsToLNu_0J.root', 'wjets', 0, isMC=True, xsec=49240*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_WJetsToLNu_1J.root', 'wjets', 0, isMC=True, xsec=8236*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_WJetsToLNu_2J.root', 'wjets', 0, isMC=True, xsec=2736*10**3, scale_factor=1, counter_histogram_root = '')
    
    s.addChannel(inputfiles + 'PROC_WW.root', 'diboson', 0, isMC=True, xsec=118.7*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_WZ.root', 'diboson', 0, isMC=True, xsec=47.13*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_ZZ.root', 'diboson', 0, isMC=True, xsec=16.523*10**3, scale_factor=1, counter_histogram_root = '')
    
    s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-10to50.root', 'drellyan', 0, isMC=True, xsec=18610*10**3, scale_factor=1, counter_histogram_root = '')
    s.addChannel(inputfiles + 'PROC_DYJetsToLL_M-50.root', 'drellyan', 0, isMC=True, xsec=6025.2*10**3, scale_factor=1, counter_histogram_root = '')
    
    if 'mu' in channel:
        s.addChannel(inputfiles + 'PROC_QCD_Pt-15To20_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=3336011*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-20To30_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=3987854.9*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-30To50_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=1705381*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-50To80_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=395178*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-80To120_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=106889.4*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-120To170_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=23773.61*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-170To300_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=8292.9*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-300To470_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=797.4*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-470To600_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=56.6*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-600To800_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=25.1*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-800To1000_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=4.7*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-1000_MuEnriched.root', 'qcd_mu', 0, isMC=True, xsec=1.6*10**3, scale_factor=1, counter_histogram_root = '')
        
        

    if 'el' in channel:
        s.addChannel(inputfiles + 'PROC_QCD_Pt-20to30_EMEnriched.root', 'qcd_el', 0, isMC=True, xsec=4948840*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-30to50_EMEnriched.root', 'qcd_el', 0, isMC=True, xsec=6324800*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-50to80_EMEnriched.root', 'qcd_el', 0, isMC=True, xsec=1806336*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-80to120_EMEnriched.root', 'qcd_el', 0, isMC=True, xsec=380538*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-120to170_EMEnriched.root', 'qcd_el', 0, isMC=True, xsec=66634.3*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-170to300_EMEnriched.root', 'qcd_el', 0, isMC=True, xsec=20859*10**3, scale_factor=1, counter_histogram_root = '')
        s.addChannel(inputfiles + 'PROC_QCD_Pt-300toInf_EMEnriched.root', 'qcd_el', 0, isMC=True, xsec=1350*10**3, scale_factor=1, counter_histogram_root = '')

    weights = [

        "evWeight_wobtagSF",
        "pugenWeight",
        "muon_SF_central",
        "ele_SF_central",
        "totbtagSF",
        "btag_SF_bcflav_central",
        "btag_SF_lflav_central",
        "evWeight",
        "genWeight",
        "prefiring_SF_central",
        "syst_elec_recoUp",
        "syst_elec_recoDown",
        "syst_elec_idUp",
        "syst_elec_idDown",
        "muon_hltUp",
        "muon_hltDown",
        "syst_muon_hltUp",
        "syst_muon_hltDown",
        "stat_muon_hlt_2018Up",
        "stat_muon_hlt_2018Down",
        "muon_recoUp",
        "muon_recoDown",
        "syst_muon_recoUp",
        "syst_muon_recoDown",
        "stat_muon_reco_2018Up",
        "stat_muon_reco_2018Down",
        "muon_idUp",
        "muon_idDown",
        "syst_muon_idUp",
        "syst_muon_idDown",
        "stat_muon_id_2018Up",
        "stat_muon_id_2018Down",
        "muon_isoUp",
        "muon_isoDown",
        "syst_muon_isoUp",
        "syst_muon_isoDown",
        "stat_muon_iso_2018Up",
        "stat_muon_iso_2018Down",
        "syst_puUp",
        "syst_puDown",
        "syst_b_correlatedUp",
        "syst_b_correlatedDown",
        "syst_b_uncorrelated_2018Up",
        "syst_b_uncorrelated_2018Down",
        "syst_l_correlatedUp",
        "syst_l_correlatedDown",
        "syst_l_uncorrelated_2018Up",
        "syst_l_uncorrelated_2018Down",
        "syst_prefiringUp",
        "syst_prefiringDown"
    ]

    s.create_combine_root_file(inputfiles, weights, zonecode, region)

if __name__ == '__main__':
    main()
