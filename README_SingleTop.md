#FLY framework specific files description for Christopher's environment

##submitBatchJob.sh:

This is a bash script for processing multiple ROOT files in parallel using Slurm workload manager.

Here is a brief explanation of what the script does:

    1. The script reads the input file name from the first command line argument.
    2. The script loops over the lines in the input file, which contains a list of ROOT files to be processed.
    3. For each line in the input file, the script extracts the name of the output file and checks if it has the ".root" extension. If it does, the script submits a Slurm job to process the input file using the processonefile.py script, with the output file name and some other parameters.
    4. The script prints some debug information and checks if the maximum number of parallel jobs has been reached. If it has, the script sets a flag to stop submitting more jobs.

This script is designed to process multiple ROOT files in parallel using Slurm and the processonefile.py script.



##executeJobs.py

The following code is used to execute the script called "submitBatchJobs.sh". It provides input arguments for the SLURM system and specifies the file pathways to access the necessary data and monte carlo files. Specifically, it retrieves text files located in the "fly/DSinfo/'year'/'mc_data'/'dataset'" directory and provides them to "submitBatchJobs.sh", along with the Slurm parameters.

Command line to execute the script:

python3 executeJobs.py -Y (16pre/16post/17/18) -N ('all' = analyze all root files per dataset or give it an int) -O (give a name to the output directory)

The output will be stored inside "fly/results"


##mergeFiles.py

This code is for merging root files using the hadd command.

The code then loops through all files in the input folder and checks the number of input files and output files for each root file to ensure they match. If they don't match, an error message is printed and the code exits.

command line to execute code:

python3 mergeFiles.py -Y (16pre/16post/17/18) -I inputFolder -O outputfolder -R region code to be merged


##src/SingleTopAnalyzer.cpp

This is the main analyzer script. Here we define new variables to plot. Cuts and object selection are applied here. 
It is written using ROOT's library RDataFrame. 
Refer to the main README file for more details