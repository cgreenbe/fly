import subprocess

def create_file_with_root_paths(dataset_name, outputFile):
    # Construct the command to be executed
    command = f"rucio list-files cms:{dataset_name} --csv"
    
    # Execute the command and capture the output
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    
    if result.returncode != 0:
        # If the command execution failed, return the error
        return f"Error executing rucio command: {result.stderr}"

    # Extract root file paths
    root_paths = [line.split(',')[0] for line in result.stdout.splitlines() if ',' in line]

    # Define the output file name
    output_file = outputFile + ".txt"

    # Write the paths to the file
    with open(output_file, "w") as file:
        for path in root_paths:
            path = path.replace("cms:","/eos/lyoeos.in2p3.fr/grid/cms")
            file.write(path + '\n')
    
    return f"File '{output_file}' created with root paths."

# Replace the following line with the actual dataset name
dataset = "/WJetsToLNu_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1/NANOAODSIM"
outputFile = "../17/mc/PROC_WJetsToLNu_0J"
print(create_file_with_root_paths(dataset,outputFile))