from pathlib import Path

# Define the base directory path
base_directory = "/eos/lyoeos.in2p3.fr/grid/cms/store/mc/RunIISummer20UL16NanoAODAPVv9"

target_dir = '/gridgroup/cms/greenberg/v2_analyzer/CMSSW_12_3_0/src/v3_FLY/v3_fly/DSinfo/16/mc_post/'

baseDirectory = Path(base_directory)

datasets = [item for item in baseDirectory.iterdir() if item.is_dir and 
            'Tprime' not in item.name and 
            'HWmin' not in item.name and 
            'HWpl' not in item.name and 
            'tt' not in item.name and
            'tZq' not in item.name and
            'WZTo3LNu' not in item.name and 
            'HZJ' not in item.name
            ]

rootFiles = {}

for dataset in datasets:
    # print(dataset.name)
    rootFiles[dataset.name] = [rootFile for rootFile in dataset.rglob("*.root")]
    # break

# for key, value in rootFiles.items():
#     print(f"Dataset: {key}")
#     for file in value:
#         print(file)

for key, value in rootFiles.items():
    dataset_file_path = Path(target_dir) / (key + ".txt")
    with open(dataset_file_path, 'w') as txt_file:
        for file in value:
            txt_file.write(f"{file}\n")