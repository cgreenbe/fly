import os

# Define the source and destination directories
src_dir = "/gridgroup/cms/greenberg/v2_analyzer/CMSSW_12_3_0/src/v3_FLY/v3_fly/DSinfo/17/mc"
dst_dir = "/gridgroup/cms/greenberg/v2_analyzer/CMSSW_12_3_0/src/v3_FLY/v3_fly/DSinfo/16/mc_post"

# List all files in the source directory
src_files = os.listdir(src_dir)
dst_files = os.listdir(dst_dir)

# for file_16 in dst_files:
#     for file_17 in src_files:
#         if file_17 in file_16:
#             print(f'source file name: {file_17}')
#             print(f'target file name: {file_16}')


# Iterate through the files in the source directory
for src_file in src_files:
    # Check if the file is a .txt file
    if src_file.endswith(".txt"):
        # Extract the base name (e.g., "PROC_DYJetsToLL_M-10to50.txt" -> "DYJetsToLL_M-10to50.txt")
        src_base_name = os.path.basename(src_file)
        # Find the corresponding file in the destination directory
        matching_dst_file = next((f for f in dst_files if f.endswith(".txt") and f == src_base_name), None)
        if matching_dst_file:
            # Build the full source and destination file paths
            src_file_path = os.path.join(src_dir, src_file)
            dst_file_path = os.path.join(dst_dir, matching_dst_file)

            # Rename the source file to match the destination file name
            # os.rename(src_file_path, dst_file_path)
            print(f'src_file_path: {src_file_path}')
            print(f'dst_file_path: {dst_file_path}')

print("File names in 2016 repo have been renamed to match the 2017 repo.")