/*
 * SingleTopAnalyzer.h
 *
 *  Created on: May 6, 2022
 *      Author: suyong
 *		Developper: cdozen
 */

#ifndef SINGLETOPANALYZER_H_
#define SINGLETOPANALYZER_H_

#include "NanoAODAnalyzerrdframe.h"

class SingleTopAnalyzer: public NanoAODAnalyzerrdframe
{
	public:
		SingleTopAnalyzer(TTree *t, std::string outfilename, std::string process);
		void defineCuts();		//define a series of cuts from defined variables only. you must implement this in your subclassed analysis 
		void defineMoreVars(); 	//define higher-level variables from basic ones, you must implement this in your subclassed analysis code
		void bookHists(std::string process); 		//book histograms, you must implement this in your subclassed analysis code

		void setTree(TTree *t, std::string outfilename, std::string process);
		void setupObjects(std::string process);
		void setupAnalysis(std::string process);
		// object selectors
		void selectElectrons();
		void selectMuons();
		void selectChannel();
		void selectJets();

		void reconstructWboson();

		void calculateEvWeight();
		void selectMET();

		void removeOverlaps();
		void defineRegion();
		void reconstructTop();
		



		bool debug = true;
		bool _jsonOK;
		string _outfilename;


		TFile *_outrootfile;
		vector<string> _outrootfilenames;

};



#endif /* SingleTopAnalyzer_H_ */
