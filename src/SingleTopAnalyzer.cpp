#include "Math/GenVector/VectorUtil.h"
#include "SingleTopAnalyzer.h"
#include "utility.h"
#include <fstream>
#include "correction.h"
#include <TLorentzVector.h>

using correction::CorrectionSet;

SingleTopAnalyzer::SingleTopAnalyzer(TTree *t, std::string outfilename, std::string process)
:NanoAODAnalyzerrdframe(t, outfilename)
{
    
    // Begin_HLT_selection
    HLT2016Names_muons= {
        
        "HLT_IsoMu24",
    };

	HLT2017Names_muons= {

        "HLT_IsoMu27",
        // "HLT_Mu27",
        // "HLT_Mu20", // Use Only this one for the next QCD region analysis cause we don't know the relative prescaling between this two non iso HLT paths

    };

	HLT2018Names_muons= {

        "HLT_IsoMu24",
        // "HLT_Mu20",
    };





    HLT2016Names_elec= {
        
        "HLT_Ele32_eta2p1_WPTight_Gsf",
    };

	HLT2017Names_elec= {

        "HLT_Ele30_eta2p1_WPTight_Gsf_CentralPFJet35_EleCleaned",
        "HLT_Ele35_WPTight_Gsf",
    };

	HLT2018Names_elec= {

        "HLT_Ele30_eta2p1_WPTight_Gsf_CentralPFJet35_EleCleaned",
        "HLT_Ele32_WPTight_Gsf",
    };
    // End_HLT_selection


}

// Define your cuts here
void SingleTopAnalyzer::defineCuts() // 
{
  if (debug){
    std::cout<<std::endl;
    std::cout<< "================================//=================================" << std::endl;
    std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
    std::cout<< "================================//=================================" << std::endl;
  }
  auto Nentry = _rlm.Count();
  // This is how you can express a range of the first 100 entries
  // _rlm = _rlm.Range(0, 1000000);
  // auto Nentry_100 = _rlm.Count();
  std::cout<< "-------------------------------------------------------------------" << std::endl;
  cout << "Usage of ranges:\n"
       << " - All entries: " << *Nentry << endl;
  //<< " - Entries from 0 to 100: " << *Nentry_100 << endl;
  std::cout<< "-------------------------------------------------------------------" << std::endl;
  
  
  //---------------------------------Region Definitions-------------------------------------//
  //                                                                                        //
  //                             Signal     (2j1t)       region == 0.0                      //
  //                             alt_WJets  (2j0t1m)     region == 1.0                      //
  //                             WJets      (2j0t0m1l)   region == 2.0                      //
  //                             WJets      (2j0t)       region == 3.0                      //
  //                             TTBar      (3j2t)       region == 4.0                      //
  //                                                                                        //
  //                                                                                        //
  //                                                                                        //
  //                             batg       (at least    region_btag == 1.0                 //
  //                                         2 good jets)                                   //
  //                                                                                        //
  ////////////////////////////////////////////////////////////////////////////////////////////
  if(!_isData){
    addCuts("muonChannel==1 && ((genWeight < 400) && (genWeight > -400))", "0"); // Muon 
    // addCuts("muonChannel==1", "0"); // 
    // addCuts("N_nonIso_tight_mu==1", "0"); // 

    // addCuts("electronChannel==1 && ((genWeight < 400) && (genWeight > -400))", "0"); // Electron Channel
    // addCuts("(QCDmuonChannel==1)", "0"); // QCD Muon Channel
    // addCuts("(QCDelectronChannel==1) && ((genWeight < 400) && (genWeight > -400))", "0"); // QCD Electron Channel

    // addCuts("NgoodMuons==1 && ((genWeight < 400) && (genWeight > -400))", "0");
    // addCuts("NgoodElectron==1 && ((genWeight < 400) && (genWeight > -400))", "0");
  }
  else{
    addCuts("muonChannel==1", "0"); // Muon Channel
    // addCuts("N_nonIso_tight_mu==1", "0"); // 

    // addCuts("electronChannel==1", "0"); // Electron Channel
    // addCuts("(QCDmuonChannel==1)", "0"); // QCD Muon Channel
    // addCuts("(QCDelectronChannel==1)", "0"); // QCD Electron Channel

    // addCuts("NgoodMuons==1", "0");
    // addCuts("NgoodElectron==1", "0");

  }
        addCuts(setHLT_muons(),"00");
        // addCuts(setHLT_elec(),"00");

            // addCuts("region_btag==1", "000");
            addCuts("region == 1.0", "000");
            // addCuts("region == 4.0", "001");

    
  
  // Comments:
  /* 
     the lepton channel is defined in the function selectChannel()
     the region is defined in the function defineRegion()
     the HLT paths are defined in the constructor
  */
  
}


//=================================Define regions=================================================//
void SingleTopAnalyzer::defineRegion()
{
    if (debug){
    std::cout<<std::endl;
    std::cout<< "================================//=================================" << std::endl;
    std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
    std::cout<< "================================//=================================" << std::endl;
    }

    _rlm = _rlm.Define("region_2j1t", "NgoodJets == 2 && NTightBJets >= 1 ? 1 : 0");
    _rlm = _rlm.Define("region_2j0t1m", "NgoodJets == 2 && NTightBJets == 0 && NMediumBJets>=1 ? 1 : 0");
    _rlm = _rlm.Define("region_2j0t0m1l", "NgoodJets == 2 && NTightBJets == 0 && NMediumBJets==0 && NLooseBJets==1 ? 1 : 0");
    _rlm = _rlm.Define("region_2j0t", "NgoodJets == 2 && NTightBJets == 0 ? 1 : 0");
    _rlm = _rlm.Define("region_3j2t", "NgoodJets >= 3 && NTightBJets >= 2 ? 1 : 0");

    _rlm = _rlm.Define("region_btag", "NgoodJets >= 2 ? 1 : 0");

    _rlm = _rlm.Define("region", "region_2j1t == 1 ? 0.0 : region_2j0t1m == 1 ? 1.0 : region_2j0t0m1l == 1 ? 2.0 : region_2j0t == 1 ? 3.0 : region_3j2t == 1 ? 4.0 : -1.0");

}


//===============================Select the channel=============================================//
//: Define if we are in electron or muon channel
//=============================================================================================//
void SingleTopAnalyzer::selectChannel()
{
    if (debug){
    std::cout<<std::endl;
    std::cout<< "================================//=================================" << std::endl;
    std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
    std::cout<< "================================//=================================" << std::endl;
    }


    _rlm = _rlm.Define("muonChannel", "(N_iso_mu==1) && (N_iso_loose_mu==1) && (N_veto_el==0) ? 1 : 0");

    _rlm = _rlm.Define("QCDmuonChannel", "(N_rev_iso_mu==1) && (N_nonIso_loose_mu==1) && (N_veto_el==0) ? 1 : 0");

    _rlm = _rlm.Define("electronChannel", "(N_iso_el==1) && (N_veto_el==1) && (N_iso_loose_mu==0) ? 1 : 0");
   
    _rlm = _rlm.Define("QCDelectronChannel", "(N_rev_iso_el==1) && (N_iso_loose_mu==0) && (N_veto_el==0) ? 1 : 0");

    


    /*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
    // Defining a numb TLorentz four-vector
    _rlm = _rlm.Define("numb","-999.99");
    _rlm = _rlm.Define("numbLorentzVector", ::generate_TLorentzVector, {"numb", "numb", "numb", "numb"});

    _rlm = _rlm.Define("lepton_TL4vec", "electronChannel==1 ? iso_el_TL4vec : muonChannel==1 ? iso_mu_TL4vec : QCDelectronChannel==1 ? rev_iso_el_TL4vec : QCDmuonChannel==1 ? rev_iso_mu_TL4vec : numbLorentzVector")
               .Define("lep_phi", "electronChannel==1 ? iso_el_leading_phi : muonChannel==1 ? iso_mu_leading_phi : QCDelectronChannel==1 ? rev_iso_el_leading_phi : QCDmuonChannel==1 ? rev_iso_mu_leading_phi : -999.9");

}



//===============================Find Good Electrons===========================================//
//: Define Good Electrons in rdata frame
//=============================================================================================//
void SingleTopAnalyzer::selectElectrons()
{
    if (debug){
    std::cout<<std::endl;
    std::cout<< "================================//=================================" << std::endl;
    std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
    std::cout<< "================================//=================================" << std::endl;
    }
    cout << "select good electrons" << endl;


    // 2016
    if(_year==2016)
    {
        _rlm = _rlm.Define("all_iso_el", "Electron_cutBased==4 && Electron_pt>35 && abs(Electron_eta+Electron_deltaEtaSC)<2.1");
        _rlm = _rlm.Define("all_rev_iso_el", "Electron_cutBased>=0 && Electron_pfRelIso03_all<1 && Electron_pt>35 && abs(Electron_eta+Electron_deltaEtaSC)<2.1");
        _rlm = _rlm.Define("all_veto_el", "Electron_cutBased>=1 && Electron_pt>15 && abs(Electron_eta+Electron_deltaEtaSC)<2.5");
    }

    // 2017 and 2018
    if(_year==2017 || _year==2018)
    {
        _rlm = _rlm.Define("all_iso_el", "Electron_cutBased==4 && Electron_pt>32 && abs(Electron_eta+Electron_deltaEtaSC)<2.1");
        // _rlm = _rlm.Define("all_rev_iso_el", "Electron_cutBased>=0 && Electron_pfRelIso03_all<1 && Electron_pt>32 && abs(Electron_eta+Electron_deltaEtaSC)<2.1");
        _rlm = _rlm.Define("all_rev_iso_el", "Electron_cutBased>=0 && Electron_pt>32 && abs(Electron_eta+Electron_deltaEtaSC)<2.1");
        _rlm = _rlm.Define("all_veto_el", "Electron_cutBased>=1 && Electron_pt>15 && abs(Electron_eta+Electron_deltaEtaSC)<2.5");
    }

    _rlm = _rlm.Define("iso_el", "all_iso_el && ((abs(Electron_eta+Electron_deltaEtaSC) <= 1.479 && abs(Electron_dxy) < 0.05 && abs(Electron_dz) < 0.1) || (abs(Electron_eta+Electron_deltaEtaSC) > 1.479 && abs(Electron_dxy) < 0.1 && abs(Electron_dz) < 0.2))");
    _rlm = _rlm.Define("rev_iso_el", "all_rev_iso_el && ((abs(Electron_eta+Electron_deltaEtaSC) <= 1.479 && abs(Electron_dxy) < 0.05 && abs(Electron_dz) < 0.1) || (abs(Electron_eta+Electron_deltaEtaSC) > 1.479 && abs(Electron_dxy) < 0.1 && abs(Electron_dz) < 0.2))");
    _rlm = _rlm.Define("veto_el", "all_veto_el && ((abs(Electron_eta+Electron_deltaEtaSC) <= 1.479 && abs(Electron_dxy) < 0.05 && abs(Electron_dz) < 0.1) || (abs(Electron_eta+Electron_deltaEtaSC) > 1.479 && abs(Electron_dxy) < 0.1 && abs(Electron_dz) < 0.2))");


    /*--------------- Isolated Electron ID ---------------*/

    _rlm = _rlm.Define("goodElectrons", "Electron_cutBased==4 && Electron_pt>30")
               .Define("goodElectrons_pt_col", "Electron_pt[goodElectrons]")
               .Define("NgoodElectron", "int(goodElectrons_pt_col.size())");

    /*--------------- Isolated Electron ID ---------------*/

    _rlm = _rlm.Define("iso_el_pt_collection", "Electron_pt[iso_el]")
                .Define("iso_el_leading_pt", "int(iso_el_pt_collection.size())>0 ? iso_el_pt_collection[0] : -999.9")

                .Define("iso_el_eta_collection", "Electron_eta[iso_el]")
                .Define("iso_el_absEta_collection","abs(iso_el_eta_collection)")
                .Define("iso_el_leading_eta", "int(iso_el_eta_collection.size())>0 ? iso_el_eta_collection[0] : -999.9")

                .Define("iso_el_phi_collection", "Electron_phi[iso_el]")
                .Define("iso_el_leading_phi", "int(iso_el_phi_collection.size())>0 ? iso_el_phi_collection[0] : -999.9")

                .Define("iso_el_mass_collection", "Electron_mass[iso_el]")
                .Define("iso_el_leading_mass", "int(iso_el_mass_collection.size())>0 ? iso_el_mass_collection[0] : -999.9")

                .Define("iso_el_charge_collection", "Electron_charge[iso_el]")
                .Define("iso_el_leading_charge", "int(iso_el_charge_collection.size())>0 ? iso_el_charge_collection[0] : -999.9")

               .Define("iso_el_rel_iso_collection","Electron_pfRelIso03_all[iso_el]")
               .Define("log10_iso_el_rel_iso_collection", "log10(iso_el_rel_iso_collection)")
               .Define("iso_el_leading_rel_iso","int(iso_el_rel_iso_collection.size())>0 ? iso_el_rel_iso_collection[0] : -999.9")

               .Define("N_iso_el", "int(iso_el_pt_collection.size())");


    _rlm = _rlm.Define("iso_el_4vec", ::generate_single_4vec, {"iso_el_leading_pt", "iso_el_leading_eta", "iso_el_leading_phi", "iso_el_leading_mass"});
    _rlm = _rlm.Define("iso_el_TL4vec", ::generate_TLorentzVector, {"iso_el_leading_pt", "iso_el_leading_eta", "iso_el_leading_phi", "iso_el_leading_mass"});


    /*--------------- veto Electron ID ---------------*/

    _rlm = _rlm.Define("veto_el_pt_collection", "Electron_pt[veto_el]")
               .Define("N_veto_el", "int(veto_el_pt_collection.size())");


    /*-----------------revert isolated Electron----------*/

    _rlm = _rlm.Define("rev_iso_el_pt_collection", "Electron_pt[rev_iso_el]")
               .Define("rev_iso_el_leading_pt", "int(rev_iso_el_pt_collection.size())>0 ? rev_iso_el_pt_collection[0] : -999.9")

               .Define("rev_iso_el_eta_collection", "Electron_eta[rev_iso_el]")
               .Define("rev_iso_el_leading_eta", "int(rev_iso_el_eta_collection.size())>0 ? rev_iso_el_eta_collection[0] : -999.9")

               .Define("rev_iso_el_phi_collection", "Electron_phi[rev_iso_el]")
               .Define("rev_iso_el_leading_phi", "int(rev_iso_el_phi_collection.size())>0 ? rev_iso_el_phi_collection[0] : -999.9")

               .Define("rev_iso_el_mass_collection", "Electron_mass[rev_iso_el]")
               .Define("rev_iso_el_leading_mass", "int(rev_iso_el_mass_collection.size())>0 ? rev_iso_el_mass_collection[0] : -999.9")

               .Define("rev_iso_el_rel_iso_collection","Electron_pfRelIso03_all[rev_iso_el]")
               .Define("log10_rev_iso_el_rel_iso_collection", "log10(rev_iso_el_rel_iso_collection)")


               .Define("N_rev_iso_el", "int(rev_iso_el_pt_collection.size())");

    _rlm = _rlm.Define("rev_iso_el_TL4vec", ::generate_TLorentzVector, {"rev_iso_el_leading_pt", "rev_iso_el_leading_eta", "rev_iso_el_leading_phi", "rev_iso_el_leading_mass"});
    _rlm = _rlm.Define("rev_iso_el_4vec", ::generate_single_4vec, {"rev_iso_el_leading_pt", "rev_iso_el_leading_eta", "rev_iso_el_leading_phi", "rev_iso_el_leading_mass"});



    // Some lines to remember how to print stuff in output file:
    // auto display1 = _rlm.Display("iso_el_leading_pt");
    // std::cout<<"Testing Display() on electrons"<<std::endl;
    // display1->Print();
    // typeOfDisp = display1->Print();
    // std::cout<<"type of display = " << typeid(display1->Print()).name()<<std::endl;

    // for (const auto &el: _rlm.Take<double>("iso_el_leading_pt"))
    // { 
    //     if(el>0) std::cout << "Element: " << el << "\n";
    // }

    // auto ress = _rlm.Take<double>("iso_el_leading_pt").GetValue();
    // std::cout<<"test: "<< ress <<std::endl;

}


//===============================Find Good Muons===============================================//
//: Define Good Muons in rdata frame
//=============================================================================================//
void SingleTopAnalyzer::selectMuons()
{

    if (debug){
        std::cout<<std::endl;
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
    cout << "select good muons" << endl;
    

    _rlm = _rlm.Define("iso_mu_iso_cut", "Muon_pfRelIso04_all<0.15"); // Isolated Muon Cut
    _rlm = _rlm.Define("rev_iso_mu_iso_cut", "Muon_pfRelIso04_all>0.45"); // Reversed Isolated Muon Cut

    _rlm = _rlm.Define("loose_iso_mu_iso_cut", "Muon_pfRelIso04_all<0.25");

    _rlm = _rlm.Define("goodMuons", "Muon_tightId==true && Muon_pt_corr>26");
    
    // 2016 and 2018
    if(_year==2016 || _year==2018)
    {
        _rlm = _rlm.Define("non_iso_tight_mu", "Muon_tightId==true && Muon_pt_corr>26.0 && abs(Muon_eta)<2.4");
    }

    // 2017
    if(_year==2017)
    {
        _rlm = _rlm.Define("non_iso_tight_mu", "Muon_tightId==true && Muon_pt_corr>30.0 && abs(Muon_eta)<2.4");
    }

    _rlm = _rlm.Define("iso_mu", "non_iso_tight_mu && iso_mu_iso_cut"); //
    _rlm = _rlm.Define("rev_iso_mu", "non_iso_tight_mu && rev_iso_mu_iso_cut");

    _rlm = _rlm.Define("non_iso_loose_mu", "Muon_looseId==true && Muon_pt_corr>10 && abs(Muon_eta)<2.4");
    _rlm = _rlm.Define("iso_loose_mu", "non_iso_loose_mu && loose_iso_mu_iso_cut");

    /*--------------- Good Muon ---------------*/
    _rlm = _rlm.Define("goodMuons_pt_col", "Muon_pt_corr[goodMuons]")
               .Define("NgoodMuons", "int(goodMuons_pt_col.size())");

    // --------------- Non Isolated Tight Muon -------------- //
    _rlm = _rlm.Define("nonIso_tight_mu_relIso_col", "Muon_pfRelIso04_all[non_iso_tight_mu]")
               .Define("log10_nonIso_tight_mu_relIso_col", "log10(nonIso_tight_mu_relIso_col)")
               .Define("N_nonIso_tight_mu", "int(log10_nonIso_tight_mu_relIso_col.size())");

    // --------------- Non Isolated loose Muon -------------- //
    _rlm = _rlm.Define("nonIso_loose_mu_relIso_col", "Muon_pfRelIso04_all[non_iso_loose_mu]")
               .Define("log10_nonIso_loose_mu_relIso_col", "log10(nonIso_loose_mu_relIso_col)")
               .Define("N_nonIso_loose_mu", "int(log10_nonIso_loose_mu_relIso_col.size())");

    /*--------------- Isolated Tight Muon ---------------*/
    _rlm = _rlm.Define("iso_mu_pt_collection", "Muon_pt_corr[iso_mu]") 
               .Define("iso_mu_leading_pt", "int(iso_mu_pt_collection.size())>0 ? iso_mu_pt_collection[0] : -999.9")

            //    .Define("N_iso_mu_with_pT_gt10", ::pTcounter, {"iso_mu_pt_collection"})

               .Define("iso_mu_eta_collection", "Muon_eta[iso_mu]")
               .Define("iso_mu_absEta_collection", "abs(iso_mu_eta_collection)")
               .Define("iso_mu_leading_eta", "int(iso_mu_eta_collection.size())>0 ? iso_mu_eta_collection[0] : -999.9")

               .Define("iso_mu_phi_collection", "Muon_phi[iso_mu]")
               .Define("iso_mu_leading_phi", "int(iso_mu_phi_collection.size())>0 ? iso_mu_phi_collection[0] : -999.9")

               .Define("iso_mu_mass_collection", "Muon_mass[iso_mu]")
               .Define("iso_mu_leading_mass", "int(iso_mu_mass_collection.size())>0 ? iso_mu_mass_collection[0] : -999.9")

               .Define("iso_mu_charge_collection", "Muon_charge[iso_mu]")
               .Define("iso_mu_leading_charge", "int(iso_mu_charge_collection.size())>0 ? iso_mu_charge_collection[0] : -999.9")

               .Define("iso_mu_relIso_dR0p4_collection", "Muon_pfRelIso04_all[iso_mu]")
               .Define("iso_mu_relIso_dR0p3_collection", "Muon_pfRelIso03_all[iso_mu]")

               .Define("log10_iso_mu_relIso_dR0p4_collection", "log10(iso_mu_relIso_dR0p4_collection)")
               .Define("log10_iso_mu_relIso_dR0p3_collection", "log10(iso_mu_relIso_dR0p3_collection)")

               .Define("N_iso_mu", "int(iso_mu_pt_collection.size())");
    
    _rlm = _rlm.Define("iso_mu_4vec", ::generate_single_4vec, {"iso_mu_leading_pt", "iso_mu_leading_eta", "iso_mu_leading_phi", "iso_mu_leading_mass"});
    _rlm = _rlm.Define("iso_mu_TL4vec", ::generate_TLorentzVector, {"iso_mu_leading_pt", "iso_mu_leading_eta", "iso_mu_leading_phi", "iso_mu_leading_mass"});
    _rlm = _rlm.Define("iso_mu_4vecs", ::generate_4vec, {"iso_mu_pt_collection", "iso_mu_eta_collection", "iso_mu_phi_collection", "iso_mu_mass_collection"});

    /*--------------- Reverted isolated Muons ID ---------------*/

    _rlm = _rlm.Define("rev_iso_mu_pt_collection", "Muon_pt_corr[rev_iso_mu]")
               .Define("rev_iso_mu_leading_pt", "int(rev_iso_mu_pt_collection.size())>0 ? rev_iso_mu_pt_collection[0] : -999.9")

               .Define("rev_iso_mu_eta_collection", "Muon_eta[rev_iso_mu]")
               .Define("rev_iso_mu_leading_eta", "int(rev_iso_mu_eta_collection.size())>0 ? rev_iso_mu_eta_collection[0] : -999.9")

               .Define("rev_iso_mu_phi_collection", "Muon_phi[rev_iso_mu]")
               .Define("rev_iso_mu_leading_phi", "int(rev_iso_mu_phi_collection.size())>0 ? rev_iso_mu_phi_collection[0] : -999.9")

               .Define("rev_iso_mu_mass_collection", "Muon_mass[rev_iso_mu]")
               .Define("rev_iso_mu_leading_mass", "int(rev_iso_mu_mass_collection.size())>0 ? rev_iso_mu_mass_collection[0] : -999.9")

               .Define("rev_iso_mu_charge_collection", "Muon_charge[rev_iso_mu]")
               .Define("rev_iso_mu_leading_charge", "int(rev_iso_mu_charge_collection.size())>0 ? rev_iso_mu_charge_collection[0] : -999.9")

               .Define("rev_iso_mu_relIso_dR0p4_collection", "Muon_pfRelIso04_all[rev_iso_mu]")
               .Define("rev_iso_mu_relIso_dR0p3_collection", "Muon_pfRelIso03_all[rev_iso_mu]")

               .Define("log10_rev_iso_mu_relIso_dR0p4_collection", "log10(rev_iso_mu_relIso_dR0p4_collection)")
               .Define("log10_rev_iso_mu_relIso_dR0p3_collection", "log10(rev_iso_mu_relIso_dR0p3_collection)")

               .Define("N_rev_iso_mu", "int(rev_iso_mu_pt_collection.size())");

    _rlm = _rlm.Define("rev_iso_mu_4vec", ::generate_single_4vec, {"rev_iso_mu_leading_pt", "rev_iso_mu_leading_eta", "rev_iso_mu_leading_phi", "rev_iso_mu_leading_mass"});
    _rlm = _rlm.Define("rev_iso_mu_TL4vec", ::generate_TLorentzVector, {"rev_iso_mu_leading_pt", "rev_iso_mu_leading_eta", "rev_iso_mu_leading_phi", "rev_iso_mu_leading_mass"});

    /*--------------- Isolated Loose Muons ---------------*/

    _rlm = _rlm.Define("iso_loose_mu_pT_collection", "Muon_pt_corr[iso_loose_mu]")
               .Define("N_iso_loose_mu", "int(iso_loose_mu_pT_collection.size())");



}


//=============================Reconstruct W boson=============================================//
//: Reconstruct the W boson
//=============================================================================================//
void SingleTopAnalyzer::reconstructWboson()
{
    if (debug){
    std::cout<<std::endl;
    std::cout<< "================================//=================================" << std::endl;
    std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
    std::cout<< "================================//=================================" << std::endl;
    }

    /*--------------------- Reconstruct neutrino ---------------------*/

    std::cout<<"Reconstructing neutrino from MET"<<std::endl;
    _rlm = _rlm.Define("nu_pt", "MET_pt_phimodcorr")
               .Define("nu_phi", "MET_phi_phimodcorr")
               .Define("nu_phi_double", "static_cast<double>(nu_phi)")

               .Define("nu_px", "nu_pt*cos(nu_phi)")
               .Define("nu_py", "nu_pt*sin(nu_phi)");

    _rlm = _rlm.Define("lambda_reco", ::calculateLambda, {"lepton_TL4vec", "nu_pt", "nu_phi"});

    _rlm = _rlm.Define("delta_reco", ::calculateDelta, {"lepton_TL4vec", "nu_pt", "lambda_reco"})
               .Define("isRealSolution", "delta_reco > 0 ? 1 : -1");

    _rlm = _rlm.Define("nu_pz", ::calculate_nu_z, {"lepton_TL4vec", "lambda_reco", "delta_reco", "nu_pt", "nu_phi"});

    _rlm = _rlm.Define("nu_energy", ::calculate_nu_energy, {"nu_pt", "nu_phi", "nu_pz"});

    _rlm = _rlm.Define("nu_TL4vec", ::get_neutrino_TL4vec, {"nu_pt", "nu_phi", "nu_pz", "nu_energy"});


    // for (const auto &delta_reco: _rlm.Take<float>("delta_reco"))
    // { 
    //     std::cout << "delta_reco: " << delta_reco << "\n";
    // }

    /*--------------------- Reconstruct W boson ---------------------*/

    // _rlm = _rlm.Define("Wboson_4vec", ::reconstructWboson_TL4vec, {"lepton_TL4vec", "nu_TL4vec"});

    // _rlm = _rlm.Define("Wboson_transversMass", "Wboson_4vec.Mt()");

    // _rlm = _rlm.Define("Wboson_transversMass", "sqrt(pow(lepton_TL4vec.Pt()+nu_pt,2)-pow(nu_pt*cos(nu_phi)+lepton_TL4vec.Px(),2) - pow(nu_pt*sin(nu_phi)+lepton_TL4vec.Py(),2))");
    _rlm = _rlm.Define("delta_phi_lep_nu", ::calculate_deltaPhi_scalars, {"lep_phi", "nu_phi_double"})
    .Define("Wboson_transversMass", "sqrt(2*lepton_TL4vec.Pt()*nu_pt*(1-cos(delta_phi_lep_nu)))");


}



//=================================Select Jets=================================================//
//check the twiki page :    https://twiki.cern.ch/twiki/bin/view/CMS/JetID
//to find jetId working points for the purpose of  your analysis.
    //jetId==2 means: pass tight ID, fail tightLepVeto
    //jetId==6 means: pass tight ID and tightLepVeto ID.
//=============================================================================================//
void SingleTopAnalyzer::selectJets()
{

    if (debug){
        std::cout<<std::endl;
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
    cout << "select good jets" << endl;

    /////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// All Good Dirty Jets /////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////

    // 2016
    if(_year==2016)
    {
        _rlm = _rlm.Define("allJetsIsolated", JetID(7)); // JetID(7) = High chance that it is a Jet
        _rlm = _rlm.Define("dirtyGoodJets", "allJetsIsolated && abs(Jet_eta) < 4.7 && Jet_pt > 40"); 
    }
       
    // 2017 & 2018
    if(_year==2017 || _year==2018)
    {
        _rlm = _rlm.Define("allJetsIsolated", JetID(6)); // JetID(6) = High chance that it is a Jet
        _rlm = _rlm.Define("dirtyGoodJets_low_eta", "allJetsIsolated && abs(Jet_eta) < 2.4 && Jet_pt > 40")
                .Define("dirtyGoodJets_high_eta", "allJetsIsolated && abs(Jet_eta) >= 2.4 && abs(Jet_eta) < 4.7 && Jet_pt > 60")
                .Define("dirtyGoodJets", "dirtyGoodJets_low_eta || dirtyGoodJets_high_eta");
    }

    _rlm = _rlm.Define("dirtyGoodJets_pt_collection", "Jet_pt[dirtyGoodJets]")

               .Define("dirtyGoodJets_eta_collection", "Jet_eta[dirtyGoodJets]")

               .Define("dirtyGoodJets_phi_collection", "Jet_phi[dirtyGoodJets]")

               .Define("dirtyGoodJets_mass_collection", "Jet_mass[dirtyGoodJets]")

               .Define("dirtyGoodJets_DeepJetBtagger","Jet_btagDeepFlavB[dirtyGoodJets]")

               .Define("NdirtyGoodJets", "int(dirtyGoodJets_pt_collection.size())");


    _rlm = _rlm.Define("dirtyGoodJets_4vecs", ::generate_4vec, {"dirtyGoodJets_pt_collection", "dirtyGoodJets_eta_collection", "dirtyGoodJets_phi_collection", "dirtyGoodJets_mass_collection"});

    // Stuff only available in MC before removal of overlaps
    if(!_isData)
    {
        _rlm = _rlm.Define("dirtyGoodJets_hadFlav", "Jet_hadronFlavour[dirtyGoodJets]");
    }

    removeOverlaps(); // Creates the selector "checkOverlap" to remove overlaping jets with the leptons

    /////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// All Good Cleaned Jets ///////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////


    _rlm = _rlm.Define("goodJets_pt_collection", "dirtyGoodJets_pt_collection[checkOverlap]")

               .Define("goodJets_eta_collection", "dirtyGoodJets_eta_collection[checkOverlap]")

               .Define("goodJets_phi_collection", "dirtyGoodJets_phi_collection[checkOverlap]")

               .Define("goodJets_mass_collection", "dirtyGoodJets_mass_collection[checkOverlap]")

               .Define("goodJets_DeepJetBtagger","dirtyGoodJets_DeepJetBtagger[checkOverlap]")
               .Define("NgoodJets", "int(goodJets_pt_collection.size())");
    
    _rlm = _rlm.Define("goodJets_4vecs", ::generate_4vec, {"goodJets_pt_collection", "goodJets_eta_collection", "goodJets_phi_collection", "goodJets_mass_collection"});
               
    _rlm = _rlm.Define("deltaR_elec_jets", ::calculateDeltaR_group, {"goodJets_4vecs","iso_el_4vec"})
               .Define("deltaR_muon_jets", ::calculateDeltaR_group, {"goodJets_4vecs","iso_mu_4vec"});
    _rlm = _rlm.Define("deltaR_elec_jets_beforeCut", ::calculateDeltaR_group, {"dirtyGoodJets_4vecs","iso_el_4vec"})
               .Define("deltaR_muon_jets_beforeCut", ::calculateDeltaR_group, {"dirtyGoodJets_4vecs","iso_mu_4vec"});

    // Stuff only available in MC after removal of overlaps
    if(!_isData)
    {
        _rlm = _rlm.Define("goodJets_hadFlav", "dirtyGoodJets_hadFlav[checkOverlap]");
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////         Checking Flavour and BTag Jets          //////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////


    if(_year==2016)
    {   
        _rlm = _rlm.Define("tight_bJets_selection", "goodJets_DeepJetBtagger >= 0.7107 && abs(goodJets_eta_collection) < 2.4"); // post = 7565 ; pre = 6502
        _rlm = _rlm.Define("medium_bJets_selection", "goodJets_DeepJetBtagger >= 0.3142 && abs(goodJets_eta_collection) < 2.4");
        _rlm = _rlm.Define("loose_bJets_selection", "goodJets_DeepJetBtagger >= 0.0646 && abs(goodJets_eta_collection) < 2.4");
    }

    if(_year==2017)
    {
        _rlm = _rlm.Define("tight_bJets_selection", "goodJets_DeepJetBtagger >= 0.7476 && abs(goodJets_eta_collection) < 2.5");
        _rlm = _rlm.Define("medium_bJets_selection", "goodJets_DeepJetBtagger >= 0.3040 && abs(goodJets_eta_collection) < 2.5");
        _rlm = _rlm.Define("loose_bJets_selection", "goodJets_DeepJetBtagger >= 0.0532 && abs(goodJets_eta_collection) < 2.5");

    }

    if(_year==2018)
    {
        _rlm = _rlm.Define("tight_bJets_selection", "goodJets_DeepJetBtagger >= 0.7100 && abs(goodJets_eta_collection) < 2.5");
        _rlm = _rlm.Define("medium_bJets_selection", "goodJets_DeepJetBtagger >= 0.2783 && abs(goodJets_eta_collection) < 2.5");
        _rlm = _rlm.Define("loose_bJets_selection", "goodJets_DeepJetBtagger >= 0.0490 && abs(goodJets_eta_collection) < 2.5");        
    }

    // Divide int bc flavour and light flavour jets only for MC and get pT and eta values for 2D histgrams for efficiency computation
    if(!_isData)
    {
        // bc flavour 
        _rlm = _rlm.Define("all_bcFlav_selection", "goodJets_hadFlav!=0");
        _rlm = _rlm.Define("all_bcFlav_pT_col", "goodJets_pt_collection[all_bcFlav_selection]")
                   .Define("all_bcFlav_eta_col", "goodJets_eta_collection[all_bcFlav_selection]")
                   .Define("abs_all_bcFlav_eta_col", "abs(all_bcFlav_eta_col)")
                   .Define("N_bcFlav_jets", "int(all_bcFlav_pT_col.size())");

        _rlm = _rlm.Define("medID_bTagPass_bcFlav_selection", "medium_bJets_selection && all_bcFlav_selection"); 
        _rlm = _rlm.Define("medID_bTagPass_bcFlav_pT_col", "goodJets_pt_collection[medID_bTagPass_bcFlav_selection]")
                   .Define("medID_bTagPass_bcFlav_eta_col", "goodJets_eta_collection[medID_bTagPass_bcFlav_selection]")
                   .Define("abs_medID_bTagPass_bcFlav_eta_col", "abs(medID_bTagPass_bcFlav_eta_col)")
                   .Define("N_medID_bTagPass_bcFlav_jets", "int(medID_bTagPass_bcFlav_pT_col.size())");

        _rlm = _rlm.Define("tightID_bTagPass_bcFlav_selection", "tight_bJets_selection && all_bcFlav_selection"); 
        _rlm = _rlm.Define("tightID_bTagPass_bcFlav_pT_col", "goodJets_pt_collection[tightID_bTagPass_bcFlav_selection]")
                   .Define("tightID_bTagPass_bcFlav_eta_col", "goodJets_eta_collection[tightID_bTagPass_bcFlav_selection]")
                   .Define("abs_tightID_bTagPass_bcFlav_eta_col", "abs(tightID_bTagPass_bcFlav_eta_col)")
                   .Define("N_tightID_bTagPass_bcFlav_jets", "int(tightID_bTagPass_bcFlav_pT_col.size())");



        // light flavour
        _rlm = _rlm.Define("all_lFlav_selection", "goodJets_hadFlav==0");
        _rlm = _rlm.Define("all_lFlav_pT_col", "goodJets_pt_collection[all_lFlav_selection]")
                   .Define("all_lFlav_eta_col", "goodJets_eta_collection[all_lFlav_selection]")
                   .Define("abs_all_lFlav_eta_col", "abs(all_lFlav_eta_col)")
                   .Define("N_lFlav_jets", "int(all_lFlav_pT_col.size())");

        _rlm = _rlm.Define("medID_bTagPass_lFlav_selection", "medium_bJets_selection && all_lFlav_selection"); 
        _rlm = _rlm.Define("medID_bTagPass_lFlav_pT_col", "goodJets_pt_collection[medID_bTagPass_lFlav_selection]")
                   .Define("medID_bTagPass_lFlav_eta_col", "goodJets_eta_collection[medID_bTagPass_lFlav_selection]")
                   .Define("abs_medID_bTagPass_lFlav_eta_col", "abs(medID_bTagPass_lFlav_eta_col)")
                   .Define("N_medID_bTagPass_lFlav_jets", "int(medID_bTagPass_lFlav_pT_col.size())");

        _rlm = _rlm.Define("tightID_bTagPass_lFlav_selection", "tight_bJets_selection && all_lFlav_selection"); 
        _rlm = _rlm.Define("tightID_bTagPass_lFlav_pT_col", "goodJets_pt_collection[tightID_bTagPass_lFlav_selection]")
                   .Define("tightID_bTagPass_lFlav_eta_col", "goodJets_eta_collection[tightID_bTagPass_lFlav_selection]")
                   .Define("abs_tightID_bTagPass_lFlav_eta_col", "abs(tightID_bTagPass_lFlav_eta_col)")
                   .Define("N_tightID_bTagPass_lFlav_jets", "int(tightID_bTagPass_lFlav_pT_col.size())");

    }

    // Tight tagged BJets
    _rlm = _rlm.Define("TightBJets", "tight_bJets_selection");

    _rlm = _rlm.Define("TightBJets_pt_collection", "goodJets_pt_collection[TightBJets]")
               .Define("TightBJet_leading_pt", "int(TightBJets_pt_collection.size()) > 0 ? TightBJets_pt_collection[0] : -999.9")
               .Define("TightBJet_subleading_pt", "NgoodJets >= 3 && int(TightBJets_pt_collection.size()) >= 2 ? TightBJets_pt_collection[1] : -999.9")

               .Define("TightBJets_eta_collection", "goodJets_eta_collection[TightBJets]")
               .Define("TightBJet_leading_eta","int(TightBJets_eta_collection.size())>0 ? TightBJets_eta_collection[0] : -999.9")
               .Define("TightBJet_subleading_eta", "NgoodJets >= 3 && int(TightBJets_eta_collection.size()) >= 2 ? TightBJets_eta_collection[1] : -999.9")
               .Define("TightBJets_absEta_collection", "abs(TightBJets_eta_collection)")
               
               .Define("TightBJets_phi_collection", "goodJets_phi_collection[TightBJets]")
               .Define("TightBJet_leading_phi","int(TightBJets_phi_collection.size())>0 ? TightBJets_phi_collection[0] : -999.9")
               .Define("TightBJet_subleading_phi", "NgoodJets >= 3 && int(TightBJets_phi_collection.size()) >= 2 ? TightBJets_phi_collection[1] : -999.9")

               .Define("TightBJets_mass_collection", "goodJets_mass_collection[TightBJets]")
               .Define("TightBJet_leading_mass","int(TightBJets_mass_collection.size())>0 ? TightBJets_mass_collection[0] : -999.9")
               .Define("TightBJet_subleading_mass", "NgoodJets >= 3 && int(TightBJets_mass_collection.size()) >= 2 ? TightBJets_mass_collection[1] : -999.9")

               .Define("TightBJets_DeepJetBtagger_collection", "goodJets_DeepJetBtagger[TightBJets]")

               .Define("NTightBJets", "int(TightBJets_pt_collection.size())")
               .Define("TightBJets_4vecs", ::generate_4vec, {"TightBJets_pt_collection","TightBJets_eta_collection","TightBJets_phi_collection","TightBJets_mass_collection"})
               //.Define("TightBJets_4vecs", "goodJets_4vecs[TightBJets]")
               .Define("TightBJets_TL4vec", ::generate_TLorentzVector, {"TightBJet_leading_pt","TightBJet_leading_eta","TightBJet_leading_phi","TightBJet_leading_mass"});


    _rlm = _rlm.Define("MediumBJets", "medium_bJets_selection")
               .Define("MediumBJets_pt_collection", "goodJets_pt_collection[MediumBJets]")
               .Define("MediumBJets_eta_collection", "goodJets_eta_collection[MediumBJets]")
               .Define("NMediumBJets", "int(MediumBJets_pt_collection.size())");

    
    _rlm = _rlm.Define("LooseBJets", "loose_bJets_selection")
               .Define("LooseBJets_pt_collection", "goodJets_pt_collection[LooseBJets]")
               .Define("LooseBJets_eta_collection", "goodJets_eta_collection[LooseBJets]")
               .Define("NLooseBJets", "int(LooseBJets_pt_collection.size())");




    /////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// Spectator Jets///////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////

    _rlm = _rlm.Define("specJet_2j1t", "NgoodJets == 2 && NTightBJets == 1 ? goodJets_DeepJetBtagger <= 0.0490 : goodJets_DeepJetBtagger > 0.7476")

    
               .Define("specJet_2j1t_pt_collection", "goodJets_pt_collection[specJet_2j1t]")
               .Define("specJet_2j1t_pt", "int(specJet_2j1t_pt_collection.size()) == 1 ? specJet_2j1t_pt_collection[0] : specJet_2j1t_pt_collection[1] ")

               .Define("specJet_2j1t_eta_collection", "goodJets_eta_collection[specJet_2j1t]")
               .Define("specJet_2j1t_eta", "int(specJet_2j1t_eta_collection.size()) == 1 ? specJet_2j1t_eta_collection[0] : specJet_2j1t_eta_collection[1] ")

               .Define("specJet_2j1t_phi_collection", "goodJets_phi_collection[specJet_2j1t]")
               .Define("specJet_2j1t_phi", "int(specJet_2j1t_phi_collection.size()) == 1 ? specJet_2j1t_phi_collection[0] : specJet_2j1t_phi_collection[1] ")

               .Define("specJet_2j1t_mass_collection", "goodJets_mass_collection[specJet_2j1t]")
               .Define("specJet_2j1t_mass", "int(specJet_2j1t_mass_collection.size()) == 1 ? specJet_2j1t_mass_collection[0] : specJet_2j1t_mass_collection[1] ")

               .Define("NspecJets_2j1t", "int(specJet_2j1t_pt_collection.size())")
               .Define("SpecJet_is_Btight_2j1t", "NgoodJets == 2 && NTightBJets == 2 ? 1 : 0");

            //    _rlm = _rlm.Define("specJet_2j1t_4vects", ::generate_4vec, {"specJet_2j1t_pt_collection","specJet_2j1t_eta_collection","specJet_2j1t_phi_collection","specJet_2j1t_mass_collection"});

    
    _rlm = _rlm.Define("specJet_2j0t", "NgoodJets == 2 && NTightBJets == 0 && goodJets_DeepJetBtagger <= 0.7476")
    
               .Define("specJet_2j0t_pt_collection", "goodJets_pt_collection[specJet_2j0t]")
               .Define("specJet_2j0t_leading_pt", "int(specJet_2j0t_pt_collection.size()) > 0 ? specJet_2j0t_pt_collection[0] : -999.9 ")
               .Define("specJet_2j0t_subleading_pt", "int(specJet_2j0t_pt_collection.size()) > 1 ? specJet_2j0t_pt_collection[1] : -999.9 ")


               .Define("specJet_2j0t_eta_collection", "goodJets_eta_collection[specJet_2j0t]")
               .Define("specJet_2j0t_leading_eta", "int(specJet_2j0t_eta_collection.size()) > 0 ? specJet_2j0t_eta_collection[0] : -999.9 ")
               .Define("specJet_2j0t_subleading_eta", "int(specJet_2j0t_eta_collection.size()) > 1 ? specJet_2j0t_eta_collection[1] : -999.9 ")

               .Define("specJet_2j0t_phi_collection", "goodJets_phi_collection[specJet_2j0t]")
               .Define("specJet_2j0t_leading_phi", "int(specJet_2j0t_phi_collection.size()) > 0 ? specJet_2j0t_phi_collection[0] : -999.9 ")
               .Define("specJet_2j0t_subleading_phi", "int(specJet_2j0t_phi_collection.size()) > 1 ? specJet_2j0t_phi_collection[1] : -999.9 ")

               .Define("specJet_2j0t_mass_collection", "goodJets_mass_collection[specJet_2j0t]")
               .Define("specJet_2j0t_leading_mass", "int(specJet_2j0t_mass_collection.size()) > 0 ? specJet_2j0t_mass_collection[0] : -999.9 ")
               .Define("specJet_2j0t_subleading_mass", "int(specJet_2j0t_mass_collection.size()) > 1 ? specJet_2j0t_mass_collection[1] : -999.9 ")

               .Define("NspecJets_2j0t", "int(specJet_2j0t_pt_collection.size())");

            //    _rlm = _rlm.Define("specJet_2j0t_4vects", ::generate_4vec, {"specJet_2j0t_pt_collection","specJet_2j0t_eta_collection","specJet_2j0t_phi_collection","specJet_2j0t_mass_collection"});


    _rlm = _rlm.Define("specJet_3j2t", "NgoodJets >= 3 && NTightBJets == 2 ? goodJets_DeepJetBtagger <= 0.7476 : goodJets_DeepJetBtagger > 0.7476")

    
               .Define("specJet_3j2t_pt_collection", "goodJets_pt_collection[specJet_3j2t]")
               .Define("specJet_3j2t_pt", "NgoodJets >= 3 && NTightBJets == 2 ? specJet_3j2t_pt_collection[0] : specJet_3j2t_pt_collection[2] ")

               .Define("specJet_3j2t_eta_collection", "goodJets_eta_collection[specJet_3j2t]")
               .Define("specJet_3j2t_eta", "NgoodJets >= 3 && NTightBJets == 2 ? specJet_3j2t_eta_collection[0] : specJet_3j2t_eta_collection[2] ")

               .Define("specJet_3j2t_phi_collection", "goodJets_phi_collection[specJet_3j2t]")
               .Define("specJet_3j2t_phi", "NgoodJets >= 3 && NTightBJets == 2 ? specJet_3j2t_phi_collection[0] : specJet_3j2t_phi_collection[2] ")

               .Define("specJet_3j2t_mass_collection", "goodJets_mass_collection[specJet_3j2t]")
               .Define("specJet_3j2t_mass", "NgoodJets >= 3 && NTightBJets == 2 ? specJet_3j2t_mass_collection[0] : specJet_3j2t_mass_collection[2] ")

               .Define("NspecJet_3j2t", "int(specJet_3j2t_pt_collection.size())")
               .Define("SpecJet_is_Btight_3j2t", "NgoodJets >= 3 && NTightBJets >= 3 ? 1 : 0");

            //    _rlm = _rlm.Define("specJet_3j2t_4vects", ::generate_4vec, {"specJet_3j2t_pt_collection","specJet_3j2t_eta_collection","specJet_3j2t_phi_collection","specJet_3j2t_mass_collection"});


}



//=================================Remove overlaps between jets and leptons=================================================//
void SingleTopAnalyzer::removeOverlaps()
{
    cout << "checking overlapss between jets and leptons" << endl;

	// lambda function for checking overlapped jets with electrons
	
	auto checkoverlap = [](FourVectorVec &jets, FourVector &lep)
		{
			doubles mindrlepton;
			for (auto ajet: jets)
			{
                auto mindr = 6.0;
				// for (auto alepton: leps)
				// {
				// 	auto dr = ROOT::Math::VectorUtil::DeltaR(ajet, alepton);
                //     if (dr < mindr) mindr = dr;
                // }
                auto dr = ROOT::Math::VectorUtil::DeltaR(ajet, lep);
                if (dr < mindr) mindr = dr;
                int out = mindr > 0.4 ? 1 : 0;
                mindrlepton.emplace_back(out);

			}
            return mindrlepton;
	    };

    
	_rlm = _rlm.Define("numb2","-999.99");
	_rlm = _rlm.Define("numb4Vector", ::generate_single_4vec, {"numb2", "numb2", "numb2", "numb2"});

    _rlm = _rlm.Define("lepton_forOverlapCheck", "electronChannel==1 ? iso_el_4vec : muonChannel==1 ? iso_mu_4vec : QCDmuonChannel==1 ? rev_iso_mu_4vec : QCDelectronChannel==1 ? rev_iso_el_4vec : numb4Vector");

    _rlm = _rlm.Define("checkOverlap", checkoverlap, {"dirtyGoodJets_4vecs","lepton_forOverlapCheck"});
    
}


//=============================Reconstruct single top quark=============================================//
//: Reconstruct the Single top quark
//=============================================================================================//
void SingleTopAnalyzer::reconstructTop()
{
    if (debug){
    std::cout<<std::endl;
    std::cout<< "================================//=================================" << std::endl;
    std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
    std::cout<< "================================//=================================" << std::endl;
    }

    _rlm = _rlm.Define("bQuark_forReco", "region == 0.0 ? TightBJets_TL4vec : numbLorentzVector")
               .Define("topQuark_TL4vec", "Wboson_4vec + bQuark_forReco");

    _rlm = _rlm.Define("top_mass", "topQuark_TL4vec.M()")
               .Define("top_pt", "topQuark_TL4vec.Pt()");

}




//=============================define variables and histograms to plot==================================================//
void SingleTopAnalyzer::defineMoreVars()
{
    if (debug){
        std::cout<<std::endl;
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }

    std::string year = std::to_string(_year);

    //================================Store variables in tree=======================================//
    // define variables that you want to store
    //==============================================================================================//


    if(!_isData)
    {
        addVartoStore("evWeight_wobtagSF");
        addVartoStore("pugenWeight");
        addVartoStore("muon_SF_central");
        addVartoStore("ele_SF_central");
    


        addVartoStore("totbtagSF");
        addVartoStore("btag_SF_bcflav_central");
        addVartoStore("btag_SF_lflav_central");
    


        addVartoStore("evWeight");
        addVartoStore("genWeight");
        addVartoStore("prefiring_SF_central");

        addVartoStore("totbtagSF");
        addVartoStore("evWeight_wobtagSF");
        addVartoStore("syst_elec_recoUp");
        addVartoStore("syst_elec_recoDown");
        addVartoStore("syst_elec_idUp");
        addVartoStore("syst_elec_idDown");
        addVartoStore("muon_hltUp");
        addVartoStore("muon_hltDown");
        addVartoStore("syst_muon_hltUp");
        addVartoStore("syst_muon_hltDown");
        addVartoStore(Form("stat_muon_hlt_%sUp",year.c_str()));
        addVartoStore(Form("stat_muon_hlt_%sDown",year.c_str()));
        addVartoStore("muon_recoUp");
        addVartoStore("muon_recoDown");
        addVartoStore("syst_muon_recoUp");
        addVartoStore("syst_muon_recoDown");
        addVartoStore(Form("stat_muon_reco_%sUp",year.c_str()));
        addVartoStore(Form("stat_muon_reco_%sDown",year.c_str()));
        addVartoStore("muon_idUp");
        addVartoStore("muon_idDown");
        addVartoStore("syst_muon_idUp");
        addVartoStore("syst_muon_idDown");
        addVartoStore(Form("stat_muon_id_%sUp",year.c_str()));
        addVartoStore(Form("stat_muon_id_%sDown",year.c_str()));
        addVartoStore("muon_isoUp");
        addVartoStore("muon_isoDown");
        addVartoStore("syst_muon_isoUp");
        addVartoStore("syst_muon_isoDown");
        addVartoStore(Form("stat_muon_iso_%sUp",year.c_str()));
        addVartoStore(Form("stat_muon_iso_%sDown",year.c_str()));
        addVartoStore("syst_puUp");
        addVartoStore("syst_puDown");
        addVartoStore("syst_b_correlatedUp");
        addVartoStore("syst_b_correlatedDown");
        addVartoStore(Form("syst_b_uncorrelated_%sUp",year.c_str()));
        addVartoStore(Form("syst_b_uncorrelated_%sDown",year.c_str()));
        addVartoStore("syst_l_correlatedUp");
        addVartoStore("syst_l_correlatedDown");
        addVartoStore(Form("syst_l_uncorrelated_%sUp",year.c_str()));
        addVartoStore(Form("syst_l_uncorrelated_%sDown",year.c_str()));
        addVartoStore("syst_prefiringUp");
        addVartoStore("syst_prefiringDown");

    }



}


void SingleTopAnalyzer::bookHists(std::string process)
{
    //=================================structure of histograms==============================================//
    //add1DHist( {"hnevents", "hist_title; x_axis title; y_axis title", 2, -0.5, 1.5}, "one", "evWeight", "");
    //add1DHist( {"hgoodelectron1_pt", "good electron1_pt; #electron p_{T}; Entries / after ", 18, -2.7, 2.7}, "good_electron1pt", "evWeight", "0");
    //======================================================================================================//
    
    if (debug){
        std::cout<<std::endl;
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
    std::string year = std::to_string(_year);
    std::double_t region = 1.0;

    if(!_isData)
    {
        std::string weights[34] = {"pugenWeight", "evWeight_wobtagSF", "syst_elec_recoUp", "syst_elec_recoDown", "syst_elec_idUp", "syst_elec_idDown", "syst_muon_hltUp", "syst_muon_hltDown", Form("stat_muon_hlt_%sUp",year.c_str()), Form("stat_muon_hlt_%sDown",year.c_str()), "syst_muon_recoUp", "syst_muon_recoDown", Form("stat_muon_reco_%sUp",year.c_str()), Form("stat_muon_reco_%sDown",year.c_str()), "syst_muon_idUp", "syst_muon_idDown", Form("stat_muon_id_%sUp",year.c_str()), Form("stat_muon_id_%sDown",year.c_str()), "syst_muon_isoUp", "syst_muon_isoDown", Form("stat_muon_iso_%sUp",year.c_str()), Form("stat_muon_iso_%sDown",year.c_str()), "syst_puUp", "syst_puDown", "syst_b_correlatedUp", "syst_b_correlatedDown", Form("syst_b_uncorrelated_%sUp",year.c_str()), Form("syst_b_uncorrelated_%sDown",year.c_str()), "syst_l_correlatedUp", "syst_l_correlatedDown", Form("syst_l_uncorrelated_%sUp",year.c_str()), Form("syst_l_uncorrelated_%sDown",year.c_str()), "syst_prefiringUp", "syst_prefiringDown"};
        for(int i = 0; i < 34; i++)
        {
            if(region==1.0)
            {
                std::string histName = process + "_Wboson_transversMass";
                // add1DHist( {Form("Wboson_transversMass_%s",weights[i].c_str()),"W boson Transvers Mass", 40, 0.0, 200}, "Wboson_transversMass",Form("%s",weights[i].c_str()),"");
                add1DHist( {Form("%s_%s",histName.c_str(),weights[i].c_str()),"W boson Transvers Mass;", 40, 0.0, 200}, "Wboson_transversMass",Form("%s",weights[i].c_str()),"");
            }
        }
        // add1DHist( {"Tprime_transverse_mass_First_pugen","Transverse mass of Tprime; Transverse mass of the Tprime (GeV);Events;", 20, 0.0, 1200.0}, "Tprime_transverse_mass_first","pugenWeight","0");
        // add1DHist( {"Tprime_transverse_mass_First_wobtag","Transverse mass of Tprime; Transverse mass of the Tprime (GeV);Events;", 20, 0.0, 1200.0}, "Tprime_transverse_mass_first","evWeight_wobtagSF","0");
    }

    
    /*--------------------- Electron ---------------------*/

        // add1DHist({"iso_el_leading_pt", "Isolated Electron Leading pT", 60, 30, 200}, "iso_el_leading_pt", "evWeight","");
        // add1DHist({"iso_el_leading_eta", "Isolated Electron Leading #eta", 30, -3, 3}, "iso_el_leading_eta", "evWeight","");
        // add1DHist({"iso_el_leading_phi", "Isolated Electron Leading #Phi", 34, -3.4, 3.4}, "iso_el_leading_phi", "evWeight","");
        // add1DHist({"iso_el_leading_mass", "Isolated Electron Leading mass", 100, 0.2, 0.3}, "iso_el_leading_mass", "evWeight","");
        // add1DHist({"iso_el_leading_rel_iso", "Relative Isolation", 30, 0, 30}, "iso_el_leading_rel_iso", "evWeight", "");
        // add1DHist({"log10_iso_el_rel_iso_collection", "Relative isolation", 40, -3, 1}, "log10_iso_el_rel_iso_collection", "evWeight", "");
        // add1DHist({"iso_el_leading_charge", "Isolated Electron Leading Charge", 6, -2, 2}, "iso_el_leading_charge", "evWeight", "");
        // add1DHist({"N_iso_el", "Number of Isolated Electrons", 10, 0, 10}, "N_iso_el", "one","");

        // add1DHist({"N_veto_el", "Number of VetoID Electrons", 10, 0, 10}, "N_veto_el", "one","");

        // add1DHist({"log10_rev_iso_el_rel_iso_collection", "Relative Isolation", 40, -3, 1}, "log10_rev_iso_el_rel_iso_collection", "evWeight", "");
        // add1DHist({"N_rev_iso_el", "Number of VetoID Electrons", 10, 0, 10}, "N_rev_iso_el", "one","");


        

    /*--------------------- Muon ---------------------*/

        // Leading isoMuon Properties
        add1DHist({"iso_mu_leading_pt", "Isolated Muon Leading pT", 100, 26, 200}, "iso_mu_leading_pt", "evWeight", "");
        add1DHist({"iso_mu_leading_eta", "Isolated Muon Leading #eta", 60, -3, 3}, "iso_mu_leading_eta", "evWeight", "");
        add1DHist({"iso_mu_leading_phi", "Isolated Muon Leading #Phi", 60, -3.4, 3.4}, "iso_mu_leading_phi", "evWeight", "");
        add1DHist({"iso_mu_leading_mass", "Isolated Muon Leading mass", 1000, 0, 1}, "iso_mu_leading_mass", "evWeight", "");
        add1DHist({"iso_mu_leading_charge", "Isolated Muon Leading Charge", 4, -2, 2}, "iso_mu_leading_charge", "evWeight", "");

        add1DHist({"N_iso_mu", "Number of Isolated Muons", 10, 0, 10}, "N_iso_mu", "one", "");
        add1DHist({"log10_iso_mu_relIso_dR0p4_collection", "Isolated Muon relative isolation (dR=0.4)", 100, -3, 1}, "log10_iso_mu_relIso_dR0p4_collection", "evWeight", ""); 
        add1DHist({"iso_mu_relIso_dR0p4_collection", "Isolated Muon relative isolation (dR=0.4)", 100, 0.001, 10}, "iso_mu_relIso_dR0p4_collection", "evWeight", ""); 

        // Leading Rev Iso Muon Properties
        add1DHist({"rev_iso_mu_leading_pt", "Rev Isolated Muon Leading pT", 100, 26, 200}, "rev_iso_mu_leading_pt", "evWeight", "");
        add1DHist({"rev_iso_mu_leading_eta", "Rev Isolated Muon Leading #eta", 60, -3, 3}, "rev_iso_mu_leading_eta", "evWeight", "");
        add1DHist({"rev_iso_mu_leading_phi", "Rev Isolated Muon Leading #Phi", 60, -3.4, 3.4}, "rev_iso_mu_leading_phi", "evWeight", "");
        add1DHist({"rev_iso_mu_leading_mass", "Rev Isolated Muon Leading mass", 1000, 0, 1}, "rev_iso_mu_leading_mass", "evWeight", "");
        add1DHist({"rev_iso_mu_leading_charge", "Rev Isolated Muon Leading Charge", 4, -2, 2}, "rev_iso_mu_leading_charge", "evWeight", "");

        add1DHist({"N_rev_iso_mu", "Number of Reverted Isolated Muons", 10, 0, 10}, "N_rev_iso_mu", "one", "");
        add1DHist({"log10_rev_iso_mu_relIso_dR0p4_collection", "Rev Isolated Muon relative isolation (dR=0.4)", 100, -3, 1}, "log10_rev_iso_mu_relIso_dR0p4_collection", "evWeight", ""); 
        add1DHist({"rev_iso_mu_relIso_dR0p4_collection", "Rev Isolated Muon relative isolation (dR=0.4)", 100, 0.001, 10}, "rev_iso_mu_relIso_dR0p4_collection", "evWeight", ""); 

        // Relative Isolation (dR=0.4), Log Scale
        add1DHist({"log10_nonIso_tight_mu_relIso_col", "Non Isolated Muon relative isolation (dR=0.4) Log Scale", 100, -3, 1}, "log10_nonIso_tight_mu_relIso_col", "evWeight", ""); 
        add1DHist({"nonIso_tight_mu_relIso_col", "Non Isolated Muon relative isolation (dR=0.4) Linear Scale", 100, 0.001, 10}, "nonIso_tight_mu_relIso_col", "evWeight", ""); 


 // Add more functions here if you have additional histogram groups

    /*--------------------- MET ---------------------*/

        add1DHist({"MET_PF_pt","PF MET pT", 100, 0, 500}, "MET_pt", "evWeight","");
        add1DHist({"MET_PF_phi","PF MET #Phi", 34, -3.4, 3.4}, "MET_phi", "evWeight","");

        add1DHist({"MET_pt_corr","PF MET pT Corrected", 100, 0, 500}, "MET_pt_corr", "evWeight","");
        add1DHist({"MET_phi_corr","PF MET #Phi Corrected", 34, -3.4, 3.4}, "MET_phi_corr", "evWeight","");

        add1DHist({"MET_pt_phimodcorr","PF MET pT Modulation Corrected", 100, 0, 500}, "MET_pt_phimodcorr", "evWeight","");
        add1DHist({"MET_phi_phimodcorr","PF MET #Phi Modulation Corrected", 34, -3.4, 3.4}, "MET_phi_phimodcorr", "evWeight","");

        // add1DHist({"MET_Puppi_pt","Puppi MET pT", 80, 0, 220}, "PuppiMET_pt", "evWeight","");
        // add1DHist({"MET_Puppi_phi","Puppi MET #Phi", 34, -3.4, 3.4}, "PuppiMET_phi", "evWeight","");

        // add1DHist({"MET_DeepResolutionTune_pt","DeepMETResolution pT", 80, 0, 220}, "DeepMETResolutionTune_pt", "evWeight","");
        // add1DHist({"MET_DeepResolutionTune_phi","DeepMETResolution #Phi", 34, -3.4, 3.4}, "DeepMETResolutionTune_phi", "evWeight","");
        
        // add1DHist({"MET_DeepResponseTune_pt","DeepMETResponse pT", 80, 0, 220}, "DeepMETResponseTune_pt", "evWeight","");
        // add1DHist({"MET_DeepResponseTune_phi","DeepMETResponse #Phi", 34, -3.4, 3.4}, "DeepMETResponseTune_phi", "evWeight","");



    /*--------------------- Jets ---------------------*/

            // if(!_isData)
        // {
        //     add2DHist({"Pt_Eta_all_bcFlav_jets", "Pt * Eta of all bc flavour jets; Pt of all bc Flavour jets (GeV); Eta of all bc Flavour Jets", 50, 0, 1000, 4, 0, 2.4}, "all_bcFlav_pT_col", "abs_all_bcFlav_eta_col", "evWeight_wobtagSF", "");
        //     add2DHist({"Pt_Eta_medID_bTagPass_bcFlav_jets", "Pt * Eta of mediumID B-Tag Pass bc flavour jets; Pt of mediumID B-Tag Pass bc Flavour jets (GeV); Eta of mediumID B-Tag Pass bc Flavour Jets", 50, 0, 1000, 4, 0, 2.4}, "medID_bTagPass_bcFlav_pT_col", "abs_medID_bTagPass_bcFlav_eta_col", "evWeight_wobtagSF", "");
        //     add2DHist({"Pt_Eta_all_lFlav_jets", "Pt * Eta of all light flavour jets; Pt of all light Flavour jets (GeV); Eta of all light Flavour Jets", 50, 0, 1000, 4, 0, 2.4}, "all_lFlav_pT_col", "abs_all_lFlav_eta_col", "evWeight_wobtagSF", "");
        //     add2DHist({"Pt_Eta_medID_bTagPass_lFlav_jets", "Pt * Eta of mediumID B-Tag Pass light flavour jets; Pt of mediumID B-Tag Pass light Flavour jets (GeV); Eta of mediumID B-Tag Pass light Flavour Jets", 50, 0, 1000, 4, 0, 2.4}, "medID_bTagPass_lFlav_pT_col", "abs_medID_bTagPass_lFlav_eta_col", "evWeight_wobtagSF", "");

        //     add2DHist({"Pt_Eta_tightID_bTagPass_bcFlav_jets", "Pt * Eta of tightID B-Tag Pass bc flavour jets; Pt of tightID B-Tag Pass bc Flavour jets (GeV); Eta of tightID B-Tag Pass bc Flavour Jets", 50, 0, 1000, 4, 0, 2.4}, "tightID_bTagPass_bcFlav_pT_col", "abs_tightID_bTagPass_bcFlav_eta_col", "evWeight_wobtagSF", "");
        //     add2DHist({"Pt_Eta_tightID_bTagPass_lFlav_jets", "Pt * Eta of tightID B-Tag Pass light flavour jets; Pt of tightID B-Tag Pass light Flavour jets (GeV); Eta of tightID B-Tag Pass light Flavour Jets", 50, 0, 1000, 4, 0, 2.4}, "tightID_bTagPass_lFlav_pT_col", "abs_tightID_bTagPass_lFlav_eta_col", "evWeight_wobtagSF", "");

        // }

    
        /*--------------------- All good jets ---------------------*/

        add1DHist({"NgoodJets", "Number of Good Jets", 20, 0, 20}, "NgoodJets", "one","");



        /*--------------------- Spec Jet ---------------------*/

        //2j1t

        // add1DHist({"specJet_2j1t_pt", "Spectator Jet pT 2j1t", 80, 40, 220}, "specJet_2j1t_pt", "evWeight","");

        // add1DHist({"specJet_2j1t_eta", "Spectator Jet #eta 2j1t", 30, -3, 3}, "specJet_2j1t_eta", "evWeight","");

        // add1DHist({"specJet_2j1t_phi", "Spectator Jet phi 2j1t", 34, -3.4, 3.4}, "specJet_2j1t_phi", "evWeight","");

        // add1DHist({"specJet_2j1t_mass", "Spectator Jet mass 2j1t", 50, 0, 100}, "specJet_2j1t_mass", "evWeight","");

        // add1DHist({"NspecJets_2j1t", "Number of 2j1t Jets", 20, 0, 20}, "NspecJets_2j1t", "one","");
        // add1DHist({"SpecJet_is_Btight_2j1t", "SpecJet is a tight B jet", 6, 0, 5}, "SpecJet_is_Btight_2j1t", "one","");


        // add1DHist({"specJet_2j1t_pt", "Spectator Jet pT 2j1t", 80, 40, 220}, "specJet_2j1t_pt", "evWeight","");

        // add1DHist({"specJet_2j1t_eta", "Spectator Jet #eta 2j1t", 30, -3, 3}, "specJet_2j1t_eta", "evWeight","");

        // add1DHist({"specJet_2j1t_phi", "Spectator Jet phi 2j1t", 34, -3.4, 3.4}, "specJet_2j1t_phi", "evWeight","");

        // add1DHist({"specJet_2j1t_mass", "Spectator Jet mass 2j1t", 50, 0, 100}, "specJet_2j1t_mass", "evWeight","");

        // add1DHist({"NspecJets_2j1t", "Number of 2j1t Jets", 20, 0, 20}, "NspecJets_2j1t", "one","");
        // add1DHist({"SpecJet_is_Btight_2j1t", "SpecJet is a tight B jet", 6, 0, 5}, "SpecJet_is_Btight_2j1t", "one","");



        // 2jOt1m

        // add1DHist({"specJet_2j0t1m_pt", "Spectator Jet pT 2j0t1m", 80, 40, 220}, "specJet_2j0t1m_pt", "evWeight","");

        // add1DHist({"specJet_2j0t1m_eta", "Spectator Jet #eta 2j0t1m", 30, -3, 3}, "specJet_2j0t1m_eta", "evWeight","");

        // add1DHist({"specJet_2j0t1m_phi", "Spectator Jet phi 2j0t1m", 34, -3.4, 3.4}, "specJet_2j0t1m_phi", "evWeight","");

        // add1DHist({"specJet_2j0t1m_mass", "Spectator Jet mass 2j0t1m", 50, 0, 100}, "specJet_2j0t1m_mass", "evWeight","");

        // add1DHist({"NspecJets_2j0t1m", "Number of 2j0t1m Jets", 20, 0, 20}, "NspecJets_2j0t1m", "one","");


        // add1DHist({"specJet_2j0t1m_pt", "Spectator Jet pT 2j0t1m", 80, 40, 220}, "specJet_2j0t1m_pt", "evWeight","");

        // add1DHist({"specJet_2j0t1m_eta", "Spectator Jet #eta 2j0t1m", 30, -3, 3}, "specJet_2j0t1m_eta", "evWeight","");

        // add1DHist({"specJet_2j0t1m_phi", "Spectator Jet phi 2j0t1m", 34, -3.4, 3.4}, "specJet_2j0t1m_phi", "evWeight","");

        // add1DHist({"specJet_2j0t1m_mass", "Spectator Jet mass 2j0t1m", 50, 0, 100}, "specJet_2j0t1m_mass", "evWeight","");

        // add1DHist({"NspecJets_2j0t1m", "Number of 2j0t1m Jets", 20, 0, 20}, "NspecJets_2j0t1m", "one","");




        // 2j0t


        // add1DHist({"specJet_2j0t_leading_pt", "Leading pT Jet 2j0t", 80, 40, 220}, "specJet_2j0t_leading_pt", "evWeight","");
        // add1DHist({"specJet_2j0t_subleading_pt", "Subleading pT Jet 2j0t", 80, 40, 220}, "specJet_2j0t_subleading_pt", "evWeight","");

        // add1DHist({"specJet_2j0t_leading_eta", "Leading #eta Jet 2j0t", 30, -3, 3}, "specJet_2j0t_leading_eta", "evWeight","");
        // add1DHist({"specJet_2j0t_subleading_eta", "Subleading #eta Jet 2j0t", 30, -3, 3}, "specJet_2j0t_subleading_eta", "evWeight","");

        // add1DHist({"specJet_2j0t_leading_phi", "Leading phi Jet 2j0t", 34, -3.4, 3.4}, "specJet_2j0t_leading_phi", "evWeight","");
        // add1DHist({"specJet_2j0t_subleading_phi", "Subeading phi Jet 2j0t", 34, -3.4, 3.4}, "specJet_2j0t_subleading_phi", "evWeight","");

        // add1DHist({"specJet_2j0t_leading_mass", "Leading mass Jet 2j0t", 50, 0, 100}, "specJet_2j0t_leading_mass", "evWeight","");
        // add1DHist({"specJet_2j0t_subleading_mass", "Subleading mass Jet 2j0t", 50, 0, 100}, "specJet_2j0t_subleading_mass", "evWeight","");

        // add1DHist({"NspecJets_2j0t", "Number of 2j0t Jets", 20, 0, 20}, "NspecJets_2j0t", "one","");


        // add1DHist({"specJet_2j0t_leading_pt", "Leading pT Jet 2j0t", 80, 40, 220}, "specJet_2j0t_leading_pt", "evWeight","");
        // add1DHist({"specJet_2j0t_subleading_pt", "Subleading pT Jet 2j0t", 80, 40, 220}, "specJet_2j0t_subleading_pt", "evWeight","");

        // add1DHist({"specJet_2j0t_leading_eta", "Leading #eta Jet 2j0t", 30, -3, 3}, "specJet_2j0t_leading_eta", "evWeight","");
        // add1DHist({"specJet_2j0t_subleading_eta", "Subleading #eta Jet 2j0t", 30, -3, 3}, "specJet_2j0t_subleading_eta", "evWeight","");

        // add1DHist({"specJet_2j0t_leading_phi", "Leading phi Jet 2j0t", 34, -3.4, 3.4}, "specJet_2j0t_leading_phi", "evWeight","");
        // add1DHist({"specJet_2j0t_subleading_phi", "Subeading phi Jet 2j0t", 34, -3.4, 3.4}, "specJet_2j0t_subleading_phi", "evWeight","");

        // add1DHist({"specJet_2j0t_leading_mass", "Leading mass Jet 2j0t", 50, 0, 100}, "specJet_2j0t_leading_mass", "evWeight","");
        // add1DHist({"specJet_2j0t_subleading_mass", "Subleading mass Jet 2j0t", 50, 0, 100}, "specJet_2j0t_subleading_mass", "evWeight","");

        // add1DHist({"NspecJets_2j0t", "Number of 2j0t Jets", 20, 0, 20}, "NspecJets_2j0t", "one","");


        // 3j2t
        
        // add1DHist({"tightBJet_subleading_pt", "Subleading tight b-Jet pT", 80, 40, 220}, "TightBJet_subleading_pt", "evWeight","");

        // add1DHist({"tightBJet_subleading_eta", "Subleading tight b-Jet eta", 30, -3, 3}, "TightBJet_subleading_eta", "evWeight","");

        // add1DHist({"tightBJet_subleading_phi", "Subleading tight b-Jet phi", 34, -3.4, 3.4}, "TightBJet_subleading_phi", "evWeight","");

        // add1DHist({"tightBJet_subleading_mass", "Subleading tight b-Jet mass", 60, 0, 30}, "TightBJet_subleading_mass", "evWeight","");

        // add1DHist({"tightBJet_subleading_pt", "Subleading tight b-Jet pT", 80, 40, 220}, "TightBJet_subleading_pt", "evWeight","");

        // add1DHist({"tightBJet_subleading_eta", "Subleading tight b-Jet eta", 30, -3, 3}, "TightBJet_subleading_eta", "evWeight","");

        // add1DHist({"tightBJet_subleading_phi", "Subleading tight b-Jet phi", 34, -3.4, 3.4}, "TightBJet_subleading_phi", "evWeight","");

        // add1DHist({"tightBJet_subleading_mass", "Subleading tight b-Jet mass", 60, 0, 30}, "TightBJet_subleading_mass", "evWeight","");


        // add1DHist({"specJet_3j2t_pt", "Spectator Jet pT 3j2t", 80, 40, 220}, "specJet_3j2t_pt", "evWeight","");

        // add1DHist({"specJet_3j2t_eta", "Spectator Jet #eta 3j2t", 30, -3, 3}, "specJet_3j2t_eta", "evWeight","");

        // add1DHist({"specJet_3j2t_phi", "Spectator Jet phi 3j2t", 34, -3.4, 3.4}, "specJet_3j2t_phi", "evWeight","");

        // add1DHist({"specJet_3j2t_mass", "Spectator Jet mass 3j2t", 50, 0, 100}, "specJet_3j2t_mass", "evWeight","");

        // add1DHist({"NspecJet_3j2t", "Number of 3j2t Jets", 20, 0, 20}, "NspecJet_3j2t", "one","");
        // add1DHist({"SpecJet_is_Btight_3j2t", "SpecJet is a tight B jet", 6, 0, 5}, "SpecJet_is_Btight_3j2t", "one","");


        // add1DHist({"specJet_3j2t_pt", "Spectator Jet pT 3j2t", 80, 40, 220}, "specJet_3j2t_pt", "evWeight","");

        // add1DHist({"specJet_3j2t_eta", "Spectator Jet #eta 3j2t", 30, -3, 3}, "specJet_3j2t_eta", "evWeight","");

        // add1DHist({"specJet_3j2t_phi", "Spectator Jet phi 3j2t", 34, -3.4, 3.4}, "specJet_3j2t_phi", "evWeight","");

        // add1DHist({"specJet_3j2t_mass", "Spectator Jet mass 3j2t", 50, 0, 100}, "specJet_3j2t_mass", "evWeight","");

        // add1DHist({"NspecJet_3j2t", "Number of 3j2t Jets", 20, 0, 20}, "NspecJet_3j2t", "one","");
        // add1DHist({"SpecJet_is_Btight_3j2t", "SpecJet is a tight B jet", 6, 0, 5}, "SpecJet_is_Btight_3j2t", "one","");


        
        /*--------------------- BJets ---------------------*/

        /*--------------------- tight ---------------------*/
        // add1DHist({"tightBJet_leading_pt", "Leading tight b-Jet pT", 100, 40, 500}, "TightBJet_leading_pt", "evWeight","");

        // add1DHist({"tightBJet_leading_eta", "Leading tight b-Jet eta", 30, -3, 3}, "TightBJet_leading_eta", "evWeight","");

        // add1DHist({"tightBJet_leading_phi", "Leading tight b-Jet phi", 34, -3.4, 3.4}, "TightBJet_leading_phi", "evWeight","");

        // add1DHist({"tightBJet_leading_mass", "Leading tight b-Jet mass", 60, 0, 30}, "TightBJet_leading_mass", "evWeight","");

        // add1DHist({"NTightBJets", "Number of tight tagged b-Jets", 20, 0, 20}, "NTightBJets", "one","");


    /*--------------------- Weights and event info ---------------------*/

        if(isDefined("genWeight"))
        {
            add1DHist({"genWeight", "genWeight", 1001, -500000, 500000}, "genWeight", "one", ""); 
            add1DHist({"genweight_sumOf", "Sum of genweights", 1, 1, 2}, "one", "genWeight", "");
            // add1DHist({"genWeight_maxValue", "Max Value for genWeight", 1, 1, 2}, "one", "genWeight_maxValue", "");
        }

        // if(isDefined("genWeight_normalized"))
        // {
        //     add1DHist({"genWeight_normalized", "Normaized genWeights", 5, -2, 2}, "genWeight_normalized", "one", ""); 
        //     add1DHist({"genWeight_normalized_sumOf", "Sum of Normalized genweights", 1, 1, 2}, "one", "genWeight_normalized", "");
        // }
        
        // if(isDefined("LHEWeight_originalXWGTUP"))
        // {
        //     add1DHist({"LHEweight", "LHEweight", 1001, -500, 500}, "LHEWeight_originalXWGTUP", "one", ""); 
        //     add1DHist({"sumOf_LHEweight", "Sum of LHEweights", 1, 1, 2}, "one", "LHEWeight_originalXWGTUP", "");
        // }

        // if(isDefined("Pileup_nTrueInt"))
        // {
        //     add1DHist({"Pileup_nTrueInt", "Pileup_nTrueInt", 101, 0, 100}, "Pileup_nTrueInt", "one", "");
        // }

        add1DHist({"nevents", "Number of Events", 1, 1, 2}, "one", "one", "");

        add1DHist({"evWeight", "evWeight", 1001, -50000, 50000}, "evWeight", "one", "");
        add1DHist({"evWeight_sumOf", "Sum of event weights", 1,1,2}, "one", "evWeight", "");


    /*--------------------- Regions ---------------------*/

        add1DHist({"region", "region", 10, 0, 10}, "region", "one", "");
        // add1DHist({"region_2j1t", "region_2j1t", 4, 0, 4}, "region_2j1t", "one", "");
        // add1DHist({"region_2j0t", "region_2j0t", 4, 0, 4}, "region_2j0t", "one", "");
        // add1DHist({"region_2j0t1m", "region_2j0t1m", 4, 0, 4}, "region_2j0t1m", "one", "");
        // add1DHist({"region_3j2t", "region_3j2t", 4, 0, 4}, "region_3j2t", "one", "");


    /*--------------------- DeltaR between jets and leptons---------------------*/

        // add1DHist({"deltaR_elec_jets", "Delta R (tight electron/goodJets)", 40, 0, 2}, "deltaR_elec_jets", "one", "");
        // add1DHist({"deltaR_muon_jets", "Delta R (tight muon/goodJets)", 40, 0, 2}, "deltaR_muon_jets", "one", "");

        // add1DHist({"deltaR_elec_jets_beforeCut", "Delta R (tight electron/goodJets) before cut", 40, 0, 2}, "deltaR_elec_jets_beforeCut", "one", "");
        // add1DHist({"deltaR_muon_jets_beforeCut", "Delta R (tight muon/goodJets) before cut", 40, 0, 2}, "deltaR_muon_jets_beforeCut", "one", "");



    /*--------------------- Wreco and top reco ---------------------*/

        add1DHist({"Wboson_transversMass", "W boson Transvers Mass", 40, 0, 200}, "Wboson_transversMass", "evWeight", "");
        if(isDefined("evWeight_wobtagSF"))
        {
            add1DHist({"Wboson_transversMass_wobtagSF", "W boson Transvers Mass", 40, 0, 200}, "Wboson_transversMass", "evWeight_wobtagSF", "");
        }
        
}






//=============================setup output tree==================================================//

void SingleTopAnalyzer::setTree(TTree *t, std::string outfilename, std::string process)
{
	if (debug){
        std::cout<<std::endl;
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }

	_rd = ROOT::RDataFrame(*t);
	_rlm = RNode(_rd);
	_outfilename = outfilename;
	_hist1dinfovector.clear();
	_th1dhistos.clear();
	_varstostore.clear();
	//_hist1dinfovector.clear();
	_selections.clear();

	this->setupAnalysis(process);
}

void SingleTopAnalyzer::calculateEvWeight()
{
  //Scale Factors for BTag ID	
  int _case = 1;
  std::vector<std::string> Jets_vars_names = {"goodJets_hadFlav", "goodJets_eta_collection",  "goodJets_pt_collection", "goodJets_DeepJetBtagger"};  
  if(_case !=1){
    Jets_vars_names.emplace_back("goodJets_DeepJetBtagger");
  }
  std::string output_btag_column_name = "btag_SF_";
  _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.2783, "M", output_btag_column_name); // Automatize this 
 
  //Scale Factors for Muon HLT, RECO, ID and ISO
  std::vector<std::string> Muon_vars_names = {"iso_mu_eta_collection", "iso_mu_pt_collection"};
  std::string output_mu_column_name = "muon_SF_";
  _rlm = calculateMuSF(_rlm, Muon_vars_names, output_mu_column_name);

  //Scale Factors for Electron RECO and ID
  std::vector<std::string> Electron_vars_names = {"iso_el_eta_collection", "iso_el_pt_collection"};
  std::string output_ele_column_name = "ele_SF_";
  _rlm = calculateEleSF(_rlm, Electron_vars_names, output_ele_column_name);

  //Prefiring Weight for 2016 and 2017
  _rlm = applyPrefiringWeight(_rlm);
  //Total event Weight:


    std::string year = std::to_string(_year);    

    _rlm = _rlm.Define("evWeight_wobtagSF", "pugenWeight * prefiring_SF_central * muon_SF_central * ele_SF_central")
               .Define("totbtagSF", "btag_SF_bcflav_central * btag_SF_lflav_central")
               .Define("evWeight", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_elec_recoUp", "(N_iso_el != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_reco_sfup * ele_SF_id_sf:evWeight")
               .Define("syst_elec_recoDown", "(N_iso_el != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_reco_sfdown * ele_SF_id_sf:evWeight")
               .Define("syst_elec_idUp", "(N_iso_el != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_reco_sf * ele_SF_id_sfup:evWeight")
               .Define("syst_elec_idDown", "(N_iso_el != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_reco_sf * ele_SF_id_sfdown:evWeight")
               .Define("muon_hltUp", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_systup * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_hltDown", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_systdown * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_hltUp", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf + muon_SF_hlt_syst) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_hltDown", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf - muon_SF_hlt_syst) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define(Form("stat_muon_hlt_%sUp",year.c_str()), "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf + muon_SF_hlt_stat) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define(Form("stat_muon_hlt_%sDown",year.c_str()), "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf - muon_SF_hlt_stat) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_recoUp", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_systup * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_recoDown", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_systdown * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_recoUp", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf + muon_SF_reco_syst) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_recoDown", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf - muon_SF_reco_syst) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define(Form("stat_muon_reco_%sUp",year.c_str()), "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf + muon_SF_reco_stat) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define(Form("stat_muon_reco_%sDown",year.c_str()), "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf - muon_SF_reco_stat) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_idUp", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_systup * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_idDown", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_systdown * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_idUp", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf + muon_SF_id_syst) * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_idDown", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf - muon_SF_id_syst) * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define(Form("stat_muon_id_%sUp",year.c_str()), "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf + muon_SF_id_stat) * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define(Form("stat_muon_id_%sDown",year.c_str()), "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf - muon_SF_id_stat) * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_isoUp", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_systup * ele_SF_central:evWeight")
               .Define("muon_isoDown", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_systdown * ele_SF_central:evWeight")
               .Define("syst_muon_isoUp", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf + muon_SF_iso_syst) * ele_SF_central:evWeight")
               .Define("syst_muon_isoDown", "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf - muon_SF_iso_syst) * ele_SF_central:evWeight")
               .Define(Form("stat_muon_iso_%sUp",year.c_str()), "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf + muon_SF_iso_stat) * ele_SF_central:evWeight")
               .Define(Form("stat_muon_iso_%sDown",year.c_str()), "(N_iso_mu != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf - muon_SF_iso_stat) * ele_SF_central:evWeight")
               .Define("syst_puUp", "genWeight * puWeight_plus * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_puDown", "genWeight * puWeight_minus * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_b_correlatedUp", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_up_correlated * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_b_correlatedDown", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_down_correlated * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define(Form("syst_b_uncorrelated_%sUp",year.c_str()), "pugenWeight * prefiring_SF_central * btag_SF_bcflav_up_uncorrelated * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define(Form("syst_b_uncorrelated_%sDown",year.c_str()), "pugenWeight * prefiring_SF_central * btag_SF_bcflav_down_uncorrelated * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_l_correlatedUp", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_up_correlated * muon_SF_central * ele_SF_central")
               .Define("syst_l_correlatedDown", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_down_correlated * muon_SF_central * ele_SF_central")
               .Define(Form("syst_l_uncorrelated_%sUp",year.c_str()), "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_up_uncorrelated * muon_SF_central * ele_SF_central")
               .Define(Form("syst_l_uncorrelated_%sDown",year.c_str()), "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_down_uncorrelated * muon_SF_central * ele_SF_central")
               .Define("syst_prefiringUp", "pugenWeight * prefiring_SF_up * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_prefiringDown", "pugenWeight * prefiring_SF_down * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central");
}


//================================Selected Object Definitions====================================//
void SingleTopAnalyzer::setupObjects(std::string process)
{

  //Apply Muon pt correction
  // Object selection will be defined in sequence.
  // Selected objects will be stored in new vectors.
  selectElectrons();
  selectMuons();
  selectChannel();
  selectJets();
  defineRegion();
  reconstructWboson();
  // reconstructTop();
  if(!_isData){
    this->calculateEvWeight(); // PU, genweight and BTV and Mu and Ele
  }

}


void SingleTopAnalyzer::setupAnalysis(std::string process)
{
	if (debug){
        std::cout<<std::endl;
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
	
 	cout<<"year===="<< _year<< "==runtype=== " <<  _runtype <<endl;
	_rlm = _rlm.Define("one", "1.0");
	
	if (_isData && !isDefined("evWeight"))
	  {
	    _rlm = _rlm.Define("evWeight", [](){
		return 1.0;
	      }, {} );
	  }
	/*
	if (_isData && !isDefined("genWeight"))
	  {
	    _rlm = _rlm.Define("genWeight", [](){
		return 1.0;
	      }, {} );
	  }
	*/
	if(!_isData ) // Only use genWeight
	  {
	    //_rlm = _rlm.Define("evWeight", "genWeight");
	    
	    //std::cout<<"Using evWeight = genWeight"<<std::endl;
	    
	    auto sumgenweight = _rd.Sum("genWeight");
	    string sumofgenweight = Form("%f",*sumgenweight);
	    _rlm = _rlm.Define("genEventSumw",sumofgenweight.c_str());
	    std::cout<<"Sum of genWeights = "<<sumofgenweight.c_str()<<std::endl;
	  }
	
	
	defineCuts();
	defineMoreVars();
	bookHists(process);
	setupCuts_and_Hists();
	setupTree();
}

