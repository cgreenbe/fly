import argparse
import ROOT
import math

# Set up argument parser
parser = argparse.ArgumentParser(description='Get value of first non-empty bin in a TH1D histogram')
parser.add_argument('filename', type=str, help='name of ROOT file')
parser.add_argument('histname', type=str, help='name of histogram')
args = parser.parse_args()

# Open ROOT file
f = ROOT.TFile.Open(args.filename)

# Get TH1D object
h = f.Get(args.histname)

# Find first non-empty bin
bin_num = 1
while h.GetBinContent(bin_num) == 0 and bin_num <= h.GetNbinsX() + 1:
    bin_num += 1

# Get value of first non-empty bin
value = h.GetBinContent(bin_num)
bin_x_value = math.exp(h.GetBinCenter(bin_num))

# Print value
print("Value of first non-empty bin:", value)
print("Value of bin_x_value:", bin_x_value)
