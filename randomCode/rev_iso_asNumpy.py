import ROOT
from ROOT import *
import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description="")
parser.add_argument('--input', '-I', type=str, required=True, help='Input files path')
parser.add_argument('-N', '--normalize', action='store_true', help='Normalize histograms')
args = parser.parse_args()

files = [
    "QCDregion_0p15/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p20/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p25/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p30/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p35/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p40/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p45/PROC_QCD_dataDriven_Muon.root",
]

hist_to_plot = "Wboson_transversMass_cut000"  # Example histogram name in ROOT file

# Define your colors array
colors = [
    ROOT.kAzure + 1,
    ROOT.kViolet + 1,
    ROOT.kPink + 4,
    ROOT.kOrange + 7,
    ROOT.kTeal + 3,
    ROOT.kYellow + 2,
    ROOT.kSpring - 7
]

plt.figure(figsize=(10, 8))

for i, file_path in enumerate(files):
    df = ROOT.RDataFrame(args.input + '/' + file_path)
    np_array = df.AsNumpy()
    
    # Assuming 'np_array' contains the histogram data as one of its elements
    # hist_data = np_array['your_branch_name']  # Replace with the actual branch name
    # hist, bin_edges = np.histogram(hist_data, bins=100)  # Define your binning

    # if args.normalize:
    #     hist = hist / np.sum(hist)

    # bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])
    # plt.plot(bin_centers, hist, drawstyle='steps-mid', color=colors[i % len(colors)], label="label")

# plt.xlabel("X-axis label")
# plt.ylabel("Normalized Events/Bin" if args.normalize else "Events/Bin")
# plt.title("Histogram Title")
# plt.legend()
# plt.show()

# # Save the plot
# filename = hist_to_plot + '_normalized.png' if args.normalize else hist_to_plot + '.png'
# plt.savefig(filename)
