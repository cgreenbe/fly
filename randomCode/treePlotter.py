import os
import ROOT

def make_histogram(tree, branch_name, bin_count, sample_name):
    # Check if the branch is valid
    if not tree.GetBranch(branch_name):
        print(f"Error: Branch '{branch_name}' not found in the tree.")
        return None

    # Get the TLeaf object associated with the branch
    branch = tree.GetBranch(branch_name)

    # Set bin limits based on the min and max values
    if "btag" in branch_name:
        bin_limits = (0.8, 2)
    elif "muon" in branch_name:
        bin_limits = (0.9, 1.02)
    elif "evWeight" in branch_name:
        min_value = 100.0
        max_value = 0.0

        for entry in range(tree.GetEntries()):
            tree.GetEntry(entry)
            branch_value = branch.GetLeaf(branch_name).GetValue()
            if min_value > branch_value: min_value = branch_value
            if max_value < branch_value: max_value = branch_value
        
        bin_limits = [min_value, max_value]

    # Create a histogram for the given branch with the sample name
    hist = ROOT.TH1F(f"{sample_name}_{branch_name}", f"{sample_name}_{branch_name}", bin_count, bin_limits[0], bin_limits[1])

    # Fill the histogram from the branch
    for entry in range(tree.GetEntries()):
        tree.GetEntry(entry)
        branch_value = branch.GetLeaf(branch_name).GetValue()
        hist.Fill(branch_value)

    return hist

def multiply_branches(tree, branch_name1, branch_name2, bin_count, sample_name):

    if not tree.GetBranch(branch_name1):
        print(f"Error: Branch '{branch_name1}' not found in the tree.")
        return None
    if not tree.GetBranch(branch_name2):
        print(f"Error: Branch '{branch_name2}' not found in the tree.")
        return None
    
    branch1 = tree.GetBranch(branch_name1)
    branch2 = tree.GetBranch(branch_name2)

    branch_final_value = []
    min_value = 100
    max_value = 0
    for entry in range(tree.GetEntries()):
        tree.GetEntry(entry)
        branch1_value = branch1.GetLeaf(branch_name1).GetValue()
        branch2_value = branch2.GetLeaf(branch_name2).GetValue()
        multiply_result = branch1_value*branch2_value
        branch_final_value.append(multiply_result)

        if min_value > multiply_result:
            min_value = multiply_result

        if max_value < multiply_result:
            max_value = multiply_result

    
    bin_limits = (min_value-0.1, max_value+0.1)
    hist = ROOT.TH1F(f"{sample_name}_muon_times_btag", f"{sample_name}_muon_times_btag", bin_count, bin_limits[0], bin_limits[1])

    for entry in branch_final_value:
        hist.Fill(entry)

    return hist 



def main(input_folder):
    # Set ROOT options and load necessary libraries
    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.SetStyle('Plain')
    ROOT.gStyle.SetOptStat(0)

    # Set bin count
    bin_count = 40

    # Get a list of .root files starting with "PROC" in the input folder
    file_list = [f for f in os.listdir(input_folder) if f.startswith("PROC") and f.endswith(".root")]

    # Create a ROOT file to save the histograms
    output_root_file = ROOT.TFile("output.root", "RECREATE")

    for file_name in file_list:
        # Extract the sample name from the input file name
        sample_name = file_name.replace(".root", "")

        # Open the ROOT file
        file = ROOT.TFile(os.path.join(input_folder, file_name))

        print(file_name)

        # Get the outputTree TTree
        output_tree = file.Get("outputTree")

        # Check if the TTree is valid
        if not output_tree:
            print(f"Error: TTree 'outputTree' not found in {file_name}")
            continue

        # Create histograms for the branches
        btag_hist = make_histogram(output_tree, "btag_SF_central", bin_count, sample_name)
        muon_hist = make_histogram(output_tree, "muon_SF_central", bin_count, sample_name)
        evWeight_hist = make_histogram(output_tree, "evWeight", bin_count, sample_name)

        # Check if histograms are valid
        if not btag_hist or not muon_hist:
            print(f"Error: Failed to create histograms for {file_name}")
            continue

        # Create a histogram for the product of btag and muon histograms
        product_hist = multiply_branches(output_tree, "btag_SF_central", "muon_SF_central", bin_count, sample_name)

        # Create a canvas and draw the histograms
        output_folder = input_folder
        # Set the canvas size (HD resolution)
        ROOT.gStyle.SetCanvasDefH(1000)  # Height in pixels
        ROOT.gStyle.SetCanvasDefW(1000)  # Width in pixels
        c = ROOT.TCanvas()
        btag_hist.Draw()
        c.SaveAs(os.path.join(output_folder, f"{sample_name}_btag.png"))

        c.Clear()
        muon_hist.Draw()
        c.SaveAs(os.path.join(output_folder, f"{sample_name}_muon.png"))

        c.Clear()
        product_hist.Draw()
        c.SaveAs(os.path.join(output_folder, f"{sample_name}_muon_times_btag.png"))

        c.Clear()
        evWeight_hist.Draw()
        c.SaveAs(os.path.join(output_folder, f"{sample_name}_evWeight.png"))

        # Close the ROOT file
        file.Close()

    # Close the output ROOT file
    output_root_file.Close()

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print("Usage: python script.py <input_folder>")
        sys.exit(1)
    input_folder = sys.argv[1]
    main(input_folder)
