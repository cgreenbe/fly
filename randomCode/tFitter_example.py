import ROOT

# Create a histogram with two components
h = ROOT.TH1F("h", "", 100, 0, 10)
h.FillRandom("gaus", 1000)
h.FillRandom("expo", 1000)

# Create a TFractionFitter object and add the two components
fit = ROOT.TFractionFitter()
fit.Add("gaus", 0.5)
fit.Add("expo", 0.5)

# Perform the fit and retrieve the results
fit.Fit()

# Print the fraction of each component
for i in range(fit.GetNumberFreeParameters()):
    fraction = ROOT.Double(0)
    error = ROOT.Double(0)
    fit.GetResult(i, fraction, error)
    print(f"Component {i} fraction = {fraction:.3f} +/- {error:.3f}")

# Plot the fit result
c = ROOT.TCanvas()
fit.GetPlot().Draw("same")
h.Draw("same")
c.SaveAs("fit.pdf")
