import ROOT
import argparse
import math

# Define command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("filename", help="name of ROOT file containing histogram")
parser.add_argument("histname", help="name of TH1D histogram to read")
args = parser.parse_args()

# Open ROOT file
f = ROOT.TFile.Open(args.filename)

# Get TH1D object
h = f.Get(args.histname)

# Loop over bins and print values
for i in range(0, h.GetNbinsX() + 1):
    x = h.GetBinCenter(i)
    y = h.GetBinContent(i)
    print("Bin %d: x-value = %f, y-value = %f" % (i, math.exp(x), y))
