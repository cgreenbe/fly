import ROOT
from ROOT import TCanvas, TLegend, TFile, gStyle
import argparse

ROOT.TH1D.AddDirectory(False)

parser = argparse.ArgumentParser(description="")
parser.add_argument('--input', '-I', type=str, required=True, help='Input files path')
parser.add_argument('-N', '--normalize', action='store_true', help='Normalize histograms')
args = parser.parse_args()

files = [
    "QCDregion_0p15/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p20/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p25/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p30/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p35/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p40/PROC_QCD_dataDriven_Muon.root",
    "QCDregion_0p45/PROC_QCD_dataDriven_Muon.root",
]

# Creating a canvas
c = TCanvas("c", "Data in QCD region", 1000, 800)

# Setting up a legend, moving it a bit down
legend = TLegend(0.7, 0.65, 0.9, 0.85)  # Adjust the y-coordinates for position
legend.SetBorderSize(0)
legend.SetTextSize(0.03)
legend.SetFillStyle(0)

# Set style
gStyle.SetOptStat(0)

# Improved set of colors for better visibility
colors = [
    ROOT.kAzure + 1,
    ROOT.kViolet + 1,
    ROOT.kPink + 4,
    ROOT.kOrange + 7,
    ROOT.kTeal + 3,
    ROOT.kYellow + 2,
    ROOT.kSpring - 7
]

hist_to_plot = "Wboson_transversMass_cut000"
# hist_to_plot = "rev_iso_mu_leading_phi_cut000"

output_suffix = "_normalized" if args.normalize else ""

for i, file in enumerate(files):
    root_file = TFile.Open(args.input + '/' + file)
    hist = root_file.Get(hist_to_plot)
    if not hist:
        print(f"Failed to retrieve histogram from {file}")
        root_file.Close()
        continue
    
    print(f"Integral of {file.split('/')[0].split('_')[-1]} = {hist.Integral()}")

    if args.normalize:
        hist.Scale(1.0 / hist.Integral())
    hist.SetLineColor(colors[i % len(colors)])
    hist.SetLineWidth(2)
    option = 'SAME' if i > 0 else ''
    if i == 0:
        hist.GetXaxis().SetTitle(hist_to_plot)
        y_axis_title = "Normalized Events/Bin" if args.normalize else "Events/Bin"
        hist.GetYaxis().SetTitle(y_axis_title)
        hist.Draw("HIST")
    else:
        hist.Draw("HIST SAME")
    legend.AddEntry(hist, "rel_iso>" + file.split('/')[0].split('_')[-1], "l")
    root_file.Close()

# Turn off the grid
c.SetGrid(0, 0)

# Draw the legend
legend.Draw()

# Update the canvas
c.Update()
c.Draw()

# Save the canvas without grid
filename = hist_to_plot + output_suffix + '.png'
c.Print(filename)