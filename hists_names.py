# Histograms

# Electrons
tightElectron_leading_pt = 'tightElectron_leading_pt'
tightElectron_leading_eta = 'tightElectron_leading_eta'
tightElectron_leading_phi = 'tightElectron_leading_phi'
tightElectron_leading_mass = 'tightElectron_leading_mass'
tightElectron_leading_charge = 'tightElectron_leading_charge'
NtightElectrons = 'NtightElectrons'
NvetoElectrons = 'NvetoElectrons'

# Muons
tightMuon_leading_pt = 'tightMuon_leading_pt'
tightMuon_leading_eta = 'tightMuon_leading_eta'
tightMuon_leading_phi = 'tightMuon_leading_phi'
tightMuon_leading_mass = 'tightMuon_leading_mass'
tightMuon_leading_charge = 'tightMuon_leading_charge'
NtightMuons = 'NtightMuons'
NlooseMuons = 'NlooseMuons'
anti_isoMuon_leading_pt = 'anti_isoMuon_leading_pt'
Nanti_isoMuons = 'Nanti_isoMuons'
anti_isoMuon_leading_eta = 'anti_isoMuon_leading_eta'
log10_relativeIso04_nonIso_tightMuons = 'log10_relativeIso04_nonIso_tightMuons'
log10_relativeIso03_nonIso_tightMuons = 'log10_relativeIso03_nonIso_tightMuons'
log10_relativeIso04_nonIso_looseMuons = 'log10_relativeIso04_nonIso_looseMuons'
log10_relativeIso03_nonIso_looseMuons = 'log10_relativeIso03_nonIso_looseMuons'

# MET
MET_PF_pt = 'MET_PF_pt'
MET_PF_phi = 'MET_PF_phi'


# Jets
# Bjets
tightBJet_leading_pt = 'tightBJet_leading_pt'
tightBJet_leading_eta = 'tightBJet_leading_eta'
tightBJet_leading_phi = 'tightBJet_leading_phi'
tightBJet_leading_mass = 'tightBJet_leading_mass'
NTightBJets = 'NTightBJets'


# Weights
genweight_sumOf = 'genweight_sumOf'
nevents = 'nevents'
evWeight = 'evWeight'
evWeight_sumOf = 'evWeight_sumOf'

# Region
region = 'region'

# Wboson
isRealSolution = 'isRealSolution'
Wboson_transversMass = 'Wboson_transversMass'