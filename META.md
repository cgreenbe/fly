# Intro

This is a file intended to keep a quick record of the analysis made and their characteristics.

Takes note of:
- Year
- File pwd
- HLTs used
- Region of analysis
- QCD enriched or not 
- Channel (el/mu/elmu)
- Number of files analyzed

## Before running an analysis

### Check:
**Analyzer**: 
- HLTs
- Channel
- Region
- Corrections (Muon / Elec / BTag)

**Executiner**:
- Samples


# log

### 2023-10-11 Muon Channel / 2j0t1m QCD region / all tasks / all datasets
Analysis pwd: "results/17/2023-10-11/muChannel_2j0t1m_QCD_nbTasksall/Run2017D_SingleMuon/"
Year: 17
HLTs used: 
        "HLT_Mu27",
        "HLT_Mu20",
Region: 2j0t1m
Channel: mu
is QCD?: Yes
Number of tasks: all
Corrections: All
Comments: 
Merged? : yes


### 2023-10-11 Muon Channel / 2j0t1m REG region / all tasks / all datasets
Analysis pwd: "results/17/2023-10-11/muChannel_2j0t1m_nbTasksall/Run2017D_SingleMuon/"
Year: 17
HLTs used: 
        "HLT_IsoMu27",

Region: 2j0t1m
Channel: muon
is QCD?: no
Number of tasks: all
Corrections: All
Comments:
Merged?: yes



### 2023-10-13
Analysis pwd: "results/17/2023-10-13/elChannel_2j0t1m_nbTasksall/Run2017E_SingleElectron/"
**Year:** 17
**HLTs used:** 
        "HLT_Ele30_eta2p1_WPTight_Gsf_CentralPFJet35_EleCleaned",
        "HLT_Ele35_WPTight_Gsf",
**Region:** 2j0t1m
**Channel:** el
**is QCD?:** No
**Number of tasks:** all
**Corrections:** All
**Comments:** 
**Merged?:** Yes


### 2023-10-20
Analysis pwd: "results/17/2023-10-20/elChannel_2j0t1m_QCD_nbTasksall/Run2017E_SingleElectron/"
**Year:** 17
**HLTs used:** 
**Region:** 
**Channel:**
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 


### 2023-10-23 2017 / Muon / 2j0t1m / Reg Region
Analysis pwd: "results/17/2023-10-23/muChannel_2j0t1m_nbTasksall/Run2017D_SingleMuon/"
**Year:** 17
**HLTs used:** 
        "HLT_IsoMu27",
**Region:** 2j0t1m
**Channel:** mu
**is QCD?:** no
**Number of tasks:** all
**Corrections:** all 
**Comments:** Here we are trying the new region selection NMediumBJets >= 1
**Merged?:** 




### 2023-10-23 / 2017 / Muon / 2j0t1m / QCD Region
Analysis pwd: "results/17/2023-10-23/muChannel_2j0t1m_QCD_nbTasksall/Run2017D_SingleMuon/"
**Year:** 17
**HLTs used:** 
        "HLT_Mu27",
        "HLT_Mu20",
**Region:** 2j0t1m
**Channel:** mu
**is QCD?:** Yes
**Number of tasks:** all
**Corrections:** all 
**Comments:** Same selection as the previous analysis but in QCD.
**Merged?:** 


### 2023-10-24  / 2018 / Muon / 2j0t1m / Reg Rgion
Analysis pwd: "results/18/2023-10-24/muchannel_2j0t1m_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:** 
// "HLT_IsoMu24",
**Region:** 2j0t1m
**Channel:** mu
**is QCD?:** no
**Number of tasks:** all
**Corrections:** all
**Comments:** NMediumBJets >= 1
**Merged?:** yes


### 2023-10-24 / 2018 / Muon / 2j0t1m / QCD Rgion
Analysis pwd: "results/18/2023-10-24/muchannel_2j0t1m_QCDregion_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:** non isolated
**Region:**  2j0t1m
**Channel:** mu
**is QCD?:** Yes
**Number of tasks:** all
**Corrections:** all
**Comments:** same as the previous analysis but for QCD
**Merged?:** yes




### 2023-10-27 / 2018 / Muon / 2j0t1m / Reg Region / Only DATA
Analysis pwd: "results/18/2023-10-27/muChannel_2j0t1m_regRegion_onlyDATA_test_nbTasksall/Run2018D_SingleMuon/"
**Year:** 18
**HLTs used:** "HLT_IsoMu24",
**Region:** 2j0t1m
**Channel:** mu
**is QCD?:** no
**Number of tasks:** all
**Corrections:** all
**Comments:** Trying Veto Muons to be Loose ID tagged
**Merged?:** 


### 2023-10-27
Analysis pwd: "results/18/2023-10-27/muchannel_2j0t1m_regRegion_onlyMC_nbTasksall/PROC_ST_t-channel_top/"
**Year:** 18
**HLTs used:** 
**Region:** 
**Channel:**
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 


### 2023-10-27
Analysis pwd: "results/17/2023-10-27/muchannel_2j0t1m_regRegion_nbTasksall/Run2017D_SingleMuon/"
**Year:** 17
**HLTs used:** 
**Region:** 
**Channel:**
**is QCD?:**
**Number of tasks:** all
**Corrections:** 
**Comments:** 
**Merged?:** 

