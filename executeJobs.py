import os
from datetime import date
import argparse
import re

def dir_checker(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)


parser = argparse.ArgumentParser()
parser.add_argument('-Y', '--year', type=str, help='Give the year to analyze [2016pre, 2016post, 2017, 2018]')
parser.add_argument('-N', '--nfiles', type=str, help='Number of files to analyze. Give \'all\' to analyze all files.')
parser.add_argument('-O', '--outputFolder', type=str, help='Output folder name', default='')
parser.add_argument('-C', '--channel', type=str, help='Channel (el/mu/elmu)', default='mu')

args = parser.parse_args()
year = args.year
tasks = args.nfiles
channel = args.channel
extraOutputFile = channel + 'Channel_' + args.outputFolder + '_nbTasks' + str(tasks)



pwdToMC16pre = 'DSinfo/16/mc_pre/'
pwdToMC16post = 'DSinfo/16/mc_post/'
pwdToMC17 = 'DSinfo/17/mc/'
pwdToMC18 = 'DSinfo/18/mc/'

monteCarloSamples = [
    'PROC_DYJetsToLL_M-50',
    'PROC_DYJetsToLL_M-10to50',
    'PROC_ST_s-channel',
    'PROC_ST_tW_antitop',
    'PROC_ST_tW_top',
    'PROC_TTTo2L2Nu',
    'PROC_TTWJetsToLNu',
    'PROC_TTWJetsToQQ',
    'PROC_TTZToLLNuNu',
    'PROC_TTZToQQ',
    'PROC_WW',
    'PROC_WZ',
    'PROC_ZZ',
    'PROC_TTToSemiLeptonic',
    'PROC_WJetsToLNu_0J',
    'PROC_WJetsToLNu_1J',
    'PROC_WJetsToLNu_2J',
    'PROC_ST_t-channel_antitop',
    'PROC_ST_t-channel_top'
]

if channel == 'el' or channel == 'elmu':
    monteCarloSamples.extend([
        'PROC_QCD_Pt-15to20_EMEnriched',
        'PROC_QCD_Pt-20to30_EMEnriched',
        'PROC_QCD_Pt-30to50_EMEnriched',
        'PROC_QCD_Pt-50to80_EMEnriched',
        'PROC_QCD_Pt-80to120_EMEnriched',
        'PROC_QCD_Pt-120to170_EMEnriched',
        'PROC_QCD_Pt-170to300_EMEnriched',
        'PROC_QCD_Pt-300toInf_EMEnriched'
    ])

if channel == 'mu' or channel == 'elmu':
    monteCarloSamples.extend([
        'PROC_QCD_Pt-15To20_MuEnriched',
        'PROC_QCD_Pt-20To30_MuEnriched',
        'PROC_QCD_Pt-30To50_MuEnriched',
        'PROC_QCD_Pt-50To80_MuEnriched',
        'PROC_QCD_Pt-80To120_MuEnriched',
        'PROC_QCD_Pt-120To170_MuEnriched',
        'PROC_QCD_Pt-170To300_MuEnriched',
        'PROC_QCD_Pt-300To470_MuEnriched',
        'PROC_QCD_Pt-470To600_MuEnriched',
        'PROC_QCD_Pt-600To800_MuEnriched',
        'PROC_QCD_Pt-800To1000_MuEnriched',
        'PROC_QCD_Pt-1000_MuEnriched',
    ])

pwdToDATA16 = 'DSinfo/16/data/'
pwdToDATA17 = 'DSinfo/17/data/'
pwdToDATA18 = 'DSinfo/18/data/'

dataSamplesFor2016 = []
dataSamplesFor2017 = []
dataSamplesFor2018 = []

if channel == 'el' or channel == 'elmu':

    dataSamplesFor2016.extend([

        'Run2016B_SingleElectron',
        'Run2016C_SingleElectron',
        'Run2016D_SingleElectron',
        'Run2016E_SingleElectron',
        'Run2016F_SingleElectron',
        'Run2016G_SingleElectron',
        'Run2016H_SingleElectron',
    ])


    dataSamplesFor2017.extend([

        'Run2017F_SingleElectron',
        'Run2017B_SingleElectron',
        'Run2017C_SingleElectron',
        'Run2017D_SingleElectron',
        'Run2017E_SingleElectron',
    ])

    dataSamplesFor2018.extend([

        'Run2018A_EGamma',
        'Run2018B_EGamma',
        'Run2018C_EGamma',
        'Run2018D_EGamma',
    ])

if channel == 'mu' or channel == 'elmu':

    dataSamplesFor2016.extend([

        'Run2016B_SingleMuon',
        'Run2016B_SingleMuon',
        'Run2016D_SingleMuon',
        'Run2016E_SingleMuon',
        'Run2016F_SingleMuon',
        'Run2016G_SingleMuon',
        'Run2016H_SingleMuon',
    ])


    dataSamplesFor2017.extend([

        'Run2017E_SingleMuon',
        'Run2017F_SingleMuon',
        'Run2017B_SingleMuon',
        'Run2017C_SingleMuon',
        'Run2017D_SingleMuon',
    ])

    dataSamplesFor2018.extend([

        'Run2018A_SingleMuon',
        'Run2018B_SingleMuon',
        'Run2018C_SingleMuon',
        'Run2018D_SingleMuon',
    ])

copyInstance = True

if year == '16pre':
    filesToAnalyze = (
        (monteCarloSamples, pwdToMC16pre),
        (dataSamplesFor2016, pwdToDATA16)
    )
    jobconfiganalysisFile = 'jobconfiganalysis_16pre'

if year == '16post':
    filesToAnalyze = (
        (monteCarloSamples, pwdToMC16post),
        (dataSamplesFor2016, pwdToDATA16),
    )
    jobconfiganalysisFile = 'jobconfiganalysis_16post'


if year == '17':
    filesToAnalyze = (
        (monteCarloSamples, pwdToMC17),
        (dataSamplesFor2017, pwdToDATA17),
    )
    jobconfiganalysisFile = 'jobconfiganalysis_17'


if year == '18':
    filesToAnalyze = (
        (monteCarloSamples, pwdToMC18),
        (dataSamplesFor2018, pwdToDATA18),
    )
    jobconfiganalysisFile = 'jobconfiganalysis_18'


n = 0
while n != 2:
    for sample in filesToAnalyze[n][0]:

        # print(f'sample = {sample}')

        process = "none"
        if "PROC_ST_t-channel" in sample:
            process = "signal"
        if "WJetsToLNu" in sample:
            process = "wjets"
        if "TTToSemiLeptonic" in sample or "TTTo2L2Nu" in sample or "TTWJets" in sample or "TTZTo" in sample:
            process = "ttbar"
        if sample == "PROC_WW" or sample == "PROC_ZZ" or sample == "PROC_WZ":
            process = "diboson"
        if sample == "PROC_ST_s-channel" or sample == "PROC_ST_tW_antitop" or sample == "PROC_ST_tW_top":
            process = "singletop"
        if "DYJetsToLL" in sample:
            process = "drellyan"
        if "QCD" in sample and "MuEnriched" in sample:
            process = "qcd_mu"
        if "QCD" in sample and "EmEnriched" in sample:
            process = "qcd_el"
        
        # print(f'process = {process}')


        if len(filesToAnalyze[n][0]) == 0: break

        analysisOutput = 'results/' + year + '/'
        dir_checker(analysisOutput)
        analysisOutput = 'results/' + year + '/' + str(date.today()) + '/'
        dir_checker(analysisOutput)
        analysisOutput = 'results/' + year + '/' + str(date.today()) + '/' + extraOutputFile + '/'
        dir_checker(analysisOutput)


        if copyInstance: os.system('cp src/SingleTopAnalyzer.cpp ' + analysisOutput + "Z_SingleTopAnalyzer_COPY_"+ str(date.today()) +"_.cpp")

        analysisOutput += sample + '/'
        dir_checker(analysisOutput)
        dir_checker(analysisOutput + 'slurmOutputs')

        sample += '.txt'

        with open(filesToAnalyze[n][1] + sample, 'r') as fToRead:
            nLines = str(len(fToRead.readlines()))
        
        if tasks == 'all': ntasks = nLines
        elif tasks != 'all' and (int(nLines) < int(tasks)): ntasks = nLines
        elif tasks != 'all' and (int(nLines) > int(tasks)): ntasks = tasks

        slurmConfigs = [

            ' --cpus-per-task=' + '1',
            ' --partition=' + 'normal',
            ' --ntasks=' + ntasks,
            ' --job-name=' + sample.replace('PROC_',year + '').replace('.txt',''),
            ' --output=' + analysisOutput + 'slurmOutputs/output.out',
            ' --error=' + analysisOutput + 'slurmOutputs/errors.err'
        ]

        args = [
            ' ' + filesToAnalyze[n][1] + sample, #inputFile
            ' ' + analysisOutput, #outputFiles
            ' ' + tasks,
            ' ' + jobconfiganalysisFile,
            ' ' + process
        ]

        os.system('sbatch' + ''.join(slurmConfigs) + ' submitBatchJob.sh' + ''.join(args))
        # print('sbatch' + ''.join(slurmConfigs) + ' submitBatchJob.sh' + ''.join(args))

    n += 1

