import argparse
import subprocess
from pathlib import Path
import concurrent.futures
import shutil

def merge_root_files(root_files_by_directory, output_directory):
    def merge_files(directory_name, root_files):
        merged_file = output_directory / f"{directory_name}.root"
        command = ["hadd", "-fk", str(merged_file)] + [str(file) for file in root_files]
        subprocess.run(command)

    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        for directory_name, root_files in root_files_by_directory.items():
            futures.append(executor.submit(merge_files, directory_name, root_files))
        concurrent.futures.wait(futures)

def merge_merged_root_files(output_directory, year):
    merged_files_muon = []
    merged_files_electron = []

    if '18' in str(year):
        merged_files_muon = list(output_directory.glob("*Run*Muon*.root"))
        merged_files_electron = list(output_directory.glob("*Run*Gamma*.root"))
    elif '17' in str(year) or '16' in str(year):
        merged_files_muon = list(output_directory.glob("*Run*Muon*.root"))
        merged_files_electron = list(output_directory.glob("*Run*Elec*.root"))
    
    if merged_files_muon:
        merged_data_muon_file = output_directory / "DATA_SingleMuon.root"
        command_muon = ["hadd", "-fk", str(merged_data_muon_file)] + [str(file) for file in merged_files_muon]
        subprocess.run(command_muon)

    if merged_files_electron:
        merged_data_electron_file = output_directory / "DATA_SingleElectron.root"
        command_electron = ["hadd", "-fk", str(merged_data_electron_file)] + [str(file) for file in merged_files_electron]
        subprocess.run(command_electron)

def populate_root_files_dictionary(directory_list, region):
    if region == '':
        typeOfFile = '*.root'
    else:
        typeOfFile = '*_' + region + '.root'
    root_files_by_directory = {}
    for directory in directory_list:
        directory_name = directory.name
        root_files = [file for file in directory.glob(str(typeOfFile))]
        root_files_by_directory[directory_name] = root_files
    return root_files_by_directory

def move_unmerged_files(directory_list, output_directory):
    unmerged_directory = output_directory / "unMergedFiles"
    unmerged_directory.mkdir(exist_ok=True)

    for directory in directory_list:
        unmerged_files = list(directory.glob('*.root'))
        if unmerged_files:  # If directory has unmerged root files
            new_location = unmerged_directory / directory.name
            shutil.move(str(directory), str(new_location))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Analyze data for a specific year, input folder, and region.")

    parser.add_argument("-Y", "--year", type=int, help="The year to be analyzed")
    parser.add_argument("-I", "--input_folder", type=str, help="The input folder")
    parser.add_argument("-R", "--region", type=str, help="The region to be analyzed")

    args = parser.parse_args()

    year = args.year
    input_folder = args.input_folder
    region = args.region

    motherFolder = Path(input_folder)
    output_directory = motherFolder

    PROC_folders = [directory for directory in motherFolder.iterdir() if directory.is_dir() and 'PROC' in directory.name]
    data_muons = [directory for directory in motherFolder.iterdir() if directory.is_dir() and 'Muon' in directory.name]
    if '18' in str(year):
        data_electrons = [directory for directory in motherFolder.iterdir() if directory.is_dir() and 'Gamma' in directory.name]
    else:
        data_electrons = [directory for directory in motherFolder.iterdir() if directory.is_dir() and 'Elec' in directory.name]

    root_files_by_proc = populate_root_files_dictionary(PROC_folders, region)
    root_files_by_data_muons = populate_root_files_dictionary(data_muons, region)
    root_files_by_data_electrons = populate_root_files_dictionary(data_electrons, region)

    merge_root_files(root_files_by_proc, output_directory)
    merge_root_files(root_files_by_data_muons, output_directory)
    merge_root_files(root_files_by_data_electrons, output_directory)

    merge_merged_root_files(output_directory, year)
    
    # Move unmerged folders to "unMergedFiles"
    move_unmerged_files(PROC_folders, output_directory)
    move_unmerged_files(data_muons, output_directory)
    move_unmerged_files(data_electrons, output_directory)
